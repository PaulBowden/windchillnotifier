package com.nanga.windchillNotifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mks.api.Command;
import com.mks.api.MultiValue;
import com.mks.api.Option;
import com.mks.api.response.APIException;
import com.mks.api.response.Response;
import com.mks.services.lib.Connection;
import com.mks.services.lib.MKSLibConnectionException;



/**
 * A convenience wrapper for API calls
 * based on the fine work of cstoss.
 * @author cwhite
 *
 */
public class ApiUtils {
    
    private final static Logger log = LoggerFactory.getLogger(Notifier.class.getName());

	/**
	 * View and Integrity Item
	 * @param conn
	 * @param ID
	 * @param fields
	 */
	public static void viewItem(NotifierConnection conn, String ID, String ... fields){
		Command cmd = new Command(Command.IM, "viewissue");
		cmd.addOption(new Option ("fields", makeList(fields)));	
		cmd.addSelection(ID);		
		conn.runCommand(cmd);
	}

	/**
	 * A convenience method to make a DELIM separated list from an array
	 * @param fields
	 * @param DELIM
	 * @return
	 */
	private static String makeList(String[] fields, String DELIM) {
		StringBuffer s = new StringBuffer("");
		for(String f : fields){
			s.append(f);
			s.append(DELIM);
		}
		return s.toString();
	}
	/**
	 * A convenience method to make a comma separated list from an array
	 * @param fields
	 * @return
	 */
	private static String makeList(String[] fields) {
		return makeList(fields, ",");
	}
	
	/**
	 * View all fields of a segment
	 * @param conn
	 * @param id
	 * @return
	 */
	public static Response viewSegment(Connection conn, String id){
		return viewSegment(conn, id, (String[])null);
	}
	/**
	 * View a specific set of fields of a segment
	 * @param conn
	 * @param id
	 * @param fields
	 * @return
	 */
	public static Response viewSegment(Connection conn, String id, String ... fields) {
		Command cmd = new Command(Command.IM, "viewsegment");
		cmd.addOption(new Option("substituteParams"));		
		if(fields != null)
			cmd.addOption(new Option ("fields", makeList(fields)));
		cmd.addSelection(id);
		
		try {
			return conn.runCommand(cmd);	
		} catch (MKSLibConnectionException e) {
			// Need to handle this better. The Matix cannot run without this command so something has to be done
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * View an item in the GUI
	 * @param conn
	 * @param id
	 */
	public static void viewItem(Connection conn, String id) {
		Command cmd = new Command(Command.IM, "viewissue");
		cmd.addOption(new Option ("gui"));
		cmd.addSelection(id);		
		try {
			conn.runCommand(cmd);
		} catch (MKSLibConnectionException e) {
			// This should never fail, unless there is a mistake in the ID passed in. Hopefully this is prevented elsewhere in the code.
			e.printStackTrace();
		}		
	}

		
	/**
	 * View a segment in the GUI.
	 * @param conn
	 * @param id
	 */
	public static void viewSegment(Connection conn, int id) {
		Command cmd = new Command(Command.IM, "viewsegment");
		cmd.addOption(new Option ("gui"));
		cmd.addSelection(""+id);		
		try {
			conn.runCommand(cmd);
		} catch (MKSLibConnectionException e) {
			// This should never fail, unless there is a mistake in the ID passed in. Hopefully this is prevented elsewhere in the code.
			e.printStackTrace();
		}
		
	} // view segment
	
//	public boolean editIssue(String impersonateUser, String issueID, Hashtable issueData) throws APIException
//    {
//        int result = -1;
//        logDebugMessage("Attempting to edit issue: " + issueID);
//        Command edit = new Command(Command.IM, "editissue");
//        logDebugMessage("Options:");
//        edit.addSelection(issueID);
//        
//        for( Iterator it = issueData.keySet().iterator(); it.hasNext(); )
//        {
//            String fieldName = it.next().toString();
//            logDebugMessage("\tField: " + fieldName);
//            String fieldValue = issueData.get(fieldName).toString(); 
//            MultiValue mv = new MultiValue("=");
//            mv.add(fieldName);
//            mv.add(fieldValue);
//            edit.addOption(new Option("field", mv));
//            logDebugMessage("\t  --field=" + fieldName + '=' + fieldValue);
//        }
//        
//        Response editOut = null;
//        try
//        {
//            logDebugMessage("Ready to execute edit issue command");
//            if( null == impersonateUser )
//            {
//                editOut = exe.runCMD(edit); 
//            }
//            else
//            {
//                editOut = exe.runCMDAs(impersonateUser, edit);
//            }
//            
//            /* Example of the response object
//            <Response app="ci" command="editissue" version="4.8.0.3865.0.0">
//              <App-Connection server="cdsouzaxp" port="7001" userID="dsouza"></App-Connection>
//              <WorkItems selectionType="IIssueSelection">
//                <WorkItem id="4094" modelType="im.Issue">
//                  <Result>
//                    <Message>Updated issue 4094</Message>
//                    <Field name="resultant">
//                      <Item id="4094" modelType="im.Issue">
//                      </Item>
//                    </Field>
//                  </Result>
//                </WorkItem>
//              </WorkItems>
//              <ExitCode>0</ExitCode>
//            </Response>
//            */
//            logDebugMessage("Reading response object for edit issue: " + issueID);
//            result = editOut.getExitCode();
//        }
//        catch(APIException ex)
//        {
//            throwAPIException(ex);
//        }
//        
//        if( result == 0 )
//        {
//            logDebugMessage("Successfully edited issue: " + issueID);
//            return true;
//        }
//        else
//        {
//            logDebugMessage("ERROR: Failed to edit issue: " + issueID);
//            return false;
//        }
//    }

    /**
     * Edits an issue with a single field/value update
     * @param issueID MKS Integrity issue id
     * @param fieldName Field to update
     * @param fieldValue Value to change
     * @return true on success; false on failure
     * @throws APIException
     */
    public static boolean editIssue(Connection conn, String issueID, String fieldName, String fieldValue) throws APIException  {
        int result = -1;
        log.debug("Attempting to edit issue: " + issueID);
        Command edit = new Command(Command.IM, "editissue");
        log.debug("Options:");
        edit.addSelection(issueID);
        log.debug("\tField: " + fieldName);
        MultiValue mv = new MultiValue("=");
        mv.add(fieldName);
        mv.add(fieldValue);
        edit.addOption(new Option("field", mv));
        log.debug("\t  --field=" + fieldName + '=' + fieldValue);

        Response editOut = null;
        try {
            log.debug("Ready to execute edit issue command");
            editOut = conn.runCommand(edit);
            log.debug("Reading response object for edit issue: " + issueID);
            result = editOut.getExitCode();
        }  catch(APIException ex) {
            // TODO: attempt to handle it here
                    throw ex;
        } catch (MKSLibConnectionException e) {
            // TODO Auto-generated catch block
            log.error("got an MKS LibException " + e.getMessage());
            
            e.printStackTrace();
        }
        
        if( result == 0 ) {
            log.info("Successfully edited issue: " + issueID);
            return true;
        }
        else
        {
            log.info("ERROR: Failed to edit issue: " + issueID);
            return false;
        }
    } // editIssue
}

