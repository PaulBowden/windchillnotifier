package com.nanga.windchillNotifier.ui;


import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

public class PasswordDialog extends JPanel {
  /**
     * 
     */
    private static final long serialVersionUID = 1L;
private final JPasswordField passwordField = new JPasswordField(12);
  private boolean gainedFocusBefore;

  /**
   * "Hook" method that causes the JPasswordField to request focus the first time this method is called.
   */
  void gainedFocus() {
    if (!gainedFocusBefore) {
      gainedFocusBefore = true;
      passwordField.requestFocusInWindow();
    }
  }

  public PasswordDialog(String message) {
    super(new FlowLayout());

    add(new JLabel(message));
    add(passwordField);
  }

  public char[] getPassword() {
      return passwordField.getPassword();
  }

 
}