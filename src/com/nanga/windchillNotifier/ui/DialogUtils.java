package com.nanga.windchillNotifier.ui;

import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

/**
 * Simple dialog utilities.
 * @author pbowden
 *
 */
public class DialogUtils 
{
    /**
     * Ask the user a question.
     * @param title
     * @param message
     * @return JOptionPane results
     */
	public static int ask(String title, String message)
    {
		Object[] options = { "OK", "CANCEL" };
		int sel = JOptionPane.showOptionDialog(null, message, title,
		            JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
		            null, options, options[0]);
        return sel;
    }
    
   
	/**
	 * Flag a non-recoverable error to the user.
	 * @param title
	 * @param message
	 */
    public static void error(String title, String message)
    {
    	Object[] options1 = { "OK" };
    	JOptionPane.showOptionDialog(null, message, title,
                JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE,
                null, options1, options1[0]);
    }
    

    public static String getPassword(String title, String message)
    {
        String password = null;
        
        PasswordDialog pPnl = new PasswordDialog(message);
        JOptionPane op = new JOptionPane(pPnl, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

        JDialog dlg = op.createDialog(title);
        
        //dlg.setResizable(true);
        dlg.setVisible(true);
        op.show();
        
        
        if (op.getValue() != null && op.getValue().equals(JOptionPane.OK_OPTION)) {
            password = new String(pPnl.getPassword());
        }
        
        return (password);
    }
    
    
             
    
}

