package com.nanga.windchillNotifier;


import com.mks.api.CmdRunner;
import com.mks.api.Command;
import com.mks.api.IntegrationPoint;
import com.mks.api.IntegrationPointFactory;
import com.mks.api.Option;
import com.mks.api.Session;
import com.mks.api.response.APIException;
import com.mks.api.response.Response;

/**
 * Largely unnecessary. I am using this as a wrapper for the MKSLib Connection because it currently does not support building a local
 * integration point. So instead of changing that library I wrapped it here.
 * @author cstoss
 *
 */
public class NotifierConnection {
    private CmdRunner cmdRunner=null;
    /**
     * API major version number the facade will use. 
     * im about will give a version number of the form Build: 4.15.0.7925
     * 4 is the major version, 10 is the minor.
     */
    public static final int API_MAJOR_VERSION=4;
    /**
     * API minor version number the facade will use. 
     * im about will give a version number of the form Build: 4.15.0.7925
     * 4 is the major version, 10 is the minor.
     */
    public static final int API_MINOR_VERSION=15;
	
	/**
	 * @return the cmdRunner
	 */
	public CmdRunner getCmdRunner() {
		return cmdRunner;
	}

	public NotifierConnection()
	{
		          
			IntegrationPointFactory ipf = IntegrationPointFactory.getInstance();
            try {
            	IntegrationPoint integrationPoint = ipf.createLocalIntegrationPoint(API_MAJOR_VERSION, API_MINOR_VERSION);
				integrationPoint.setAutoStartIntegrityClient(true);				
				Session session = integrationPoint.getCommonSession();
				//Setup connection
				cmdRunner = session.createCmdRunner();
			} catch (APIException e) {
				// If this fails we are in trouble...
				e.printStackTrace();
			}
		
            
            if( (null==EnvVariable.HOSTNAME.getVarValue()) ||
                (null==EnvVariable.PORT.getVarValue())      ||
                (null==EnvVariable.USER.getVarValue())){
                System.out.println("This plugin will only connect to Integrity if the following vars are set:\n"+ 
                                    "MKSSI_HOST\r\n" +
                                    "MKSSI_PORT\r\n" +
                                    "MKSSI_USER\r\n");
            }
			
			cmdRunner.setDefaultHostname(EnvVariable.HOSTNAME.getVarValue());
			cmdRunner.setDefaultPort(Integer.parseInt(EnvVariable.PORT.getVarValue()));
			cmdRunner.setDefaultUsername(EnvVariable.USER.getVarValue());
			

            
            Command cmd = new Command(Command.SI, "connect");
            cmd.addOption(new Option("hostname", EnvVariable.HOSTNAME.getVarValue()));
            cmd.addOption(new Option("port", EnvVariable.USER.getVarValue()));
			runCommand(cmd);
	
	}
	
	public synchronized Response runCommand(Command cmd)
	{
	    try
	    {
			Response resp = cmdRunner.execute(cmd);
			//if(logger.isTraceEnabled())
			//	dumpResponse(resp);
			return resp;
	    }catch(final APIException e)
	    {
	    	e.printStackTrace();
	    }
	    return null;
	}

}
