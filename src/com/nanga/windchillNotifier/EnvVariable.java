package com.nanga.windchillNotifier;

/**
 * The Environment variables used by Integrity. This isn't something that should change,  but wanted to separate these from the
 * Defs definitions because they are accessed differently (through the System Environment Variables vs the Properties file).
 * @author cstoss
 *
 */
public enum EnvVariable {
	HOSTNAME("MKSSI_HOST"),
	PORT("MKSSI_PORT"),
	USER("MKSSI_USER"),
	NUMOFITEM("MKSSI_NISSUE"),
	ITEMONEID("MKSSI_ISSUE0"),
	ITEMTWOID("MKSSI_ISSUE1");
	
	String varName;
	String varValue;

	private EnvVariable(String varName) {
		this.varName = varName;
	}
	
	/**
	 * @return the varName
	 */
	public String getVarName() {
		return varName;
	}
	
	public String getVarValue(){		
		return System.getenv(getVarName());		
	}
}
