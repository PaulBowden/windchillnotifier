package com.nanga.windchillNotifier;

import org.apache.commons.cli.DefaultParser;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

/*
 *  Simple thing to take care of the command line
 *
 *  NOTE this can exit if it doesn't see the parameter we want
 *  NOTE we only take the first argument
 */
public class OptionsManager {

    private final static Logger log = LoggerFactory.getLogger(OptionsManager.class.getName());

    private String[] commandLine;
    private String commandString = "";
    private List<String> args;

    private Options options = new Options();

    // Pass the command line to the manager
    public OptionsManager(String[] commandline) {

        commandLine = commandline;
        options.addOption("h", "help", false, "show help.");
        //options.addOption("c", "cmd", true, "provide command to run");
        Option option = Option.builder("c")
                .longOpt( "command" )
                .desc( "command to run"  )
                .hasArg(true)
                .argName( "Command" )
                .build();
            options.addOption( option );
            
//        option = Option.builder("s")
//                .longOpt( "state" )
//                .desc( "state to update"  )
//                .hasArg()
//                .argName( "Command" )
//                .build();
//        options.addOption( option );    
//        
       log.debug("cmd line len:" + commandline.length);


    }

    public void parse() {
        
        
        CommandLineParser parser = new DefaultParser();
        

        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, commandLine);

            if (cmd.hasOption("h"))
                help();

            if (cmd.hasOption("c")) {
                log.info("Using cli command -c=" + cmd.getOptionValue("c") );
                log.info("args = "+ Arrays.toString(cmd.getArgs()) );
                commandString = cmd.getOptionValue("c");
                args = cmd.getArgList();
                                
            }
           
            
        } catch (ParseException e) {
            log.error("Failed to parse comand line properties", e);
            help();
        }
    }

    private void help() {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();

        formater.printHelp("Notifier [options] ", options);
        System.exit(0);
    }

   
    public String getCommandString() {
        log.debug("Command name from the command line is " + commandString);
        return commandString;
    }


    public boolean cmdIsCreate()
    {
        return commandString.toLowerCase().equals("create");
    }
    
    public boolean cmdIsUpdate()
    {
        return commandString.toLowerCase().equals("update");
    }
    
    public boolean cmdIsRevise()
    {
        return commandString.toLowerCase().equals("revise");
    }

    /**
     * get the first argument this command found on the command line
     * @return
     */
    public String getFirstArgument() {
        assert (args != null);
        
        return args.get(0);
    }
}
