package com.nanga.windchillNotifier;


import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.Properties;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import com.nanga.windchillNotifier.target.Target;
import com.nanga.windchillNotifier.target.UpdateTarget;
import com.nanga.windchillNotifier.ui.DialogUtils;
import com.nanga.windchillNotifier.target.CreateTarget;
import com.nanga.windchillNotifier.target.ReviseTarget;

import org.apache.http.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Notifier {
    
   
    private static PLMResponse plmResponse;
    private final static Logger log = LoggerFactory.getLogger(Notifier.class.getName());
    


    public Notifier() {
 
    }

    
    
    
    
    
    
    public static void main(String[] args) {
        Properties notifierProps = loadProperties();
        log.info("Windchill Notifier");
        
        if(EnvVariable.NUMOFITEM.getVarValue() == null){
            DialogUtils.error("No Documents Selected", "It appears you have not selected any Document Items.\nPlease select a document");
        }else{
            if(EnvVariable.NUMOFITEM.getVarValue().equals("1")){
                // Get some data back from Integrity, based on the environment vars
                Report report = new Report(notifierProps);  
        
                if(Defs.RUNINTESTMODE.getValue().toLowerCase().equals("true"))
                {
                    log.warn("Notifier running in test mode");
                    notifyTestMode(report);
                } else {
                    log.debug("Notifier running in normal mode");
                    notify(report, args);
                }
                
                
            
            } else {
                DialogUtils.error("Too Many Documents Selected", "It appears you have selected more than one document item.\nPlease select just one document");
            }
            
        }
        
        log.debug("notified.");
        System.exit(0);  // made it to the end in one piece!
    }
    
    
    private static void notify(Report report, String[] args) {
        log.debug("notify routine. Got " + args.length + " args from command line");
        
        // create an options manager to handle the command line.
        OptionsManager om = new OptionsManager(args);
        om.parse();
        
        // figure out which target we need according to the command we are sending to windchill
        Target target = null;
        if(Defs.USETARGETRULES.getValue().equals("true")) {
            log.debug("Using Target rules to decide target");
            
            DocRulesProcessor drp = new DocRulesProcessor(report);
            if( drp.cmdIsCreate() )  { target = new CreateTarget(report);  }
            if( drp.cmdIsUpdate() )  { target = new UpdateTarget(report);  }
            if( drp.cmdIsRevise() )  { target = new ReviseTarget(report);  }
            
        
        } else {
            log.debug("Using command line parameters to control target");
        
            if( om.cmdIsCreate() )  { target = new CreateTarget(report);  }
            if( om.cmdIsUpdate() )  { target = new UpdateTarget(report);  target.addParameter(Defs.WINDCHILLTARGETSTATE_KEY.getValue(),om.getFirstArgument());}
       // if( om.cmdIsRevise() )  { target = new ReviseTarget(report);  }
        }
       if( (args.length > 1) && (target==null) ) {
           log.error("We got at least a parameter, but have not computed a valid target to notify.");
           log.debug("args:" + om.getCommandString());
           
           DialogUtils.error("Unknown Parameter", "Unknown command. [" + om.getCommandString() + "]\r\n" +
                             "Please check the properties file and contact your system administrator."  );
           log.error("giving up.");
           System.exit(-6);
       }
        
        
        
        assert(target != null);
        String logfilename = target.getType() + "." + Defs.NOTIFIERLOGFILE.getValue();
        String notificationStatus = "Successfully notified PLM System.";
        
        
        
        log.debug("Checking Target PreRequisite Conditions...");
        
        if(!target.doPreflightChecks()) {
            log.error("Target prerequiste conditions are not met.");
            if(Defs.PFC_CREATE_OVERRIDE.getValue().equals("false")) {
                log.error("Pre flight checks failed for this target type.\n\n** We will not attempt to notify.");
                System.exit(-300);
            } else {
                log.warn("Preflight check for " + target.getType() + " target has been overridden.");
            }
        }
        
        
        
        
        log.debug("check for windchill host:");
        if(isReachable(target.getHostname()))
        {
            log.debug("PLN Notify with target server " + target.getHostname() + ".");
            notifyWindchillTarget(target);
        }
        else
        {
            log.error("Unable to notify PLM system.");
            notificationStatus = "Failed to notify PLM system.";
        }
        
        
        log.info("writing to the test log " + logfilename);
        try{
            PrintWriter writer = new PrintWriter(logfilename, "UTF-8");
            writer.println("Output file for Windchill Notifier Plugin");
            writer.println("Report:");
            writer.println(report.toString());
            writer.println("-----------------------------------------------------------");
            writer.println("Parameters in target object:");
            writer.println(target.getParameters().toString());
            writer.println("-----------------------------------------------------------");
            writer.println("POST REQUEST STRING:");
            writer.println(target.getFullRequest());
            writer.println("-----------------------------------------------------------");
            writer.println("Response:");
            writer.println("some fields may be null, depending on the request:");
            writer.println(plmResponse.toString());
            writer.println("-----------------------------------------------------------");
            writer.println(notificationStatus);
            writer.close();
        } catch (IOException e) {
           log.error("Unable to write notify log to " + logfilename + ". Please contact your administrator");
        }
     
        
    }
  

    /**
     * Test the commands we know about from the properties file
     * @param report
     */
    private static void notifyTestMode(Report report) {
               
        
        // Test the create command
        
        // fake the command line
        String args [] = new String[]{"-c", "create"};
        OptionsManager om = new OptionsManager(args);
        om.parse();
        CreateTarget target = null;
        if(om.cmdIsCreate())
        {
            target = new CreateTarget(report);
            // Notify Windchill
            
        }
        
        
        
        assert(target != null);
        String logfilename = target.getType() + ".TEST." + Defs.NOTIFIERLOGFILE.getValue();
        
        
        DialogUtils.error("TEST " + target.getType(), "Call the windchill URL\r\n" + 
                "Integrity Server: " + report.getHostname() + ":"+ report.getPort()+ "\r\n" +
                "Doc: " + report.getDocId() + " : " + report.getTitle() + ".\r\n" +
                "GET: " + target.getFullRequest());
        log.info("writing to the test log " + logfilename);
        
        log.debug("check for windchill host:");
        if(isReachable(target.getHostname()))
        {
            log.debug("Testing parser using online mode against server " + target.getHostname() + ".");
            notifyWindchillTarget(target);
            target.processReponse(plmResponse, report);
        }
        else
        {
            log.debug("Testing parser using offline file.");
            handleResponseTestOfflineMode();
            target.processReponse(plmResponse,report);
        }
        
        
        try{
            PrintWriter writer = new PrintWriter(logfilename, "UTF-8");
            writer.println("TEST output file for Windchill Notifier Plugin");
            writer.println("Report:");
            writer.println(report.toString());
            writer.println("-----------------------------------------------------------");
            writer.println("POST REQUEST STRING:");
            writer.println(target.getFullRequest());
            
            
            writer.close();
        } catch (IOException e) {
           // do something
        }
    } // NOtifyTestMode
    
    
    
//    private static String getWindchillUserPassword()
//    {
//        // oh man this is not very safe.
//        String password = Defs.WINDCHILLPASSWORD_VALUE.getValue();
//        
//        if(password.length() <1)
//        {
//            password = DialogUtils.getPassword("Windchill password", "Please enter Windchill password: ");
//        }
//        
//        //log.debug("password " + password);
//        return password;
//    }
    
    private static String getWindchillUserPassword()
    {
        return "OrgAsAdn";
    }
    
    
/**
 *  Make the call to the windchill target with the info in the 
 *  target parameter.
 *  
 *  populates our plmresponse object with an interpretation of
 *  whatever we get back from the plm system
 * @param target
 */
    private static void notifyWindchillTarget(Target target)
    {
        //String URL = "https://plm-test.asta.platform.solutions/Windchill/servlet/IE/tasks/ext/inneo/updateDocState.xml?targetState=IN%20REVIEW&docNumber=0000001483&docVersion=A&username=admin";
        
        String URL = target.getFullRequest();
        
        log.debug("target url: " + URL);
        
        int authRetryCount = Defs.WINDCHILLAUTHRETRIES.getValueAsInt();
        boolean connected = false;
        
        while((!connected)&&(authRetryCount>0))
        {
            try {
                int code = doWindchillRequest(URL);
                if (code==HttpStatus.SC_OK)
                {
                   
                    log.info("Connected to windchill ok");
                    connected = true;
                    
                }
                else if(code==HttpStatus.SC_FORBIDDEN)
                {
                    log.error("FORBIDDEN");
                }
                else if(code==HttpStatus.SC_UNAUTHORIZED)
                {
                    
                    --authRetryCount;
                    log.info("failed windchill connection attempt");
                }
                          
            } catch(IOException e)
            {
                log.error("Exception notifying windchill",e);
            }
        }// while
        
        if(connected){
            log.debug("Successfully notified the target with " + authRetryCount + " retries remaining.");
            
            log.debug("notfier got a response of " + plmResponse.getRaw());
            
            // now we just have to figure out the status, and tell the world.
            if(plmResponse.getException() != null)
            {
                String msg = "Exception from the plm system. We did not correctly notify " + target.getType() + " Event.\r\n " + 
                              "Please consult with your system admin for assistance.";
                log.error(msg);
                log.error("target details: " + target.toString() );
                DialogUtils.error("Notify Error",msg);
                log.error("plm response exception: " + plmResponse.getException());
                log.info("Exit before we update the issue.");
                System.exit(-5);
            } else {
                log.info("got a valid response from windchill");
                plmResponse.getException();
            }
            
            
        } else {
            log.error("Definately have the wrong user name and passord for this server.");
            DialogUtils.error("WINDCHILL USER UNAUTHORISED","Please ensure the correct properties are set for username and passord!");
            log.error("Unable to connect to windchill afer " + Defs.WINDCHILLAUTHRETRIES.getValueAsInt() + " attempts. Giving up.");
            System.exit(-3);
        }
            
    }
    
    /**
     *  The main  call to windchill with our constructed URL.
     *  handles basic authentication.
     *  Populates the plmResponse object with whatever the heck we got back.
     * @param URL
     * @return
     * @throws IOException
     */
    private static int doWindchillRequest(String URL) throws IOException {
        URL u = new URL ( URL );
        HttpURLConnection huc =  ( HttpURLConnection ) u.openConnection (); 
        huc.setRequestMethod ("GET");  //OR  huc.setRequestMethod ("HEAD");
        huc.setConnectTimeout( Defs.HTTPTIMEOUT.getValueAsInt() );
        
        //String userPassword = "admin" + ":" + "OrgAsAdn1";
        String userPassword = "admin" + ":" + getWindchillUserPassword();
                        
        byte[] data = userPassword.getBytes("UTF-8");
        String encoding = Base64.encodeBase64String(data);
     
        huc.setRequestProperty("Authorization", "Basic " + encoding);
        
        huc.connect () ; 
        int code = huc.getResponseCode() ;
        
        plmResponse = new PLMResponse(huc.getInputStream());
        log.debug("response " + plmResponse);
        
        plmResponse.parse();
        log.info("got " + code + " from " + URL + ".");
        return code;
    }
    


    private static void handleResponseTestOfflineMode() {
        System.out.println("Reading from test response " + Defs.TEMPLATERESPONSECREATE.getValue());
        
        File file = new File(Defs.TEMPLATERESPONSECREATE.getValue());
        
        
        PLMResponse windchillResponse = null;
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            windchillResponse = new PLMResponse(fis);
            windchillResponse.parse();
            
            
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        if (windchillResponse != null){
            
            log.info("wc reponse: " + windchillResponse.toString());
            plmResponse = windchillResponse;
        }
        
        
    }






    private static Properties loadProperties() {
        Properties notifierProps = new Properties();
        
        FileInputStream input = null;
        try {
           
            String filename = "NotifierProperties.properties";
            
            //input = Notifier.class.getClassLoader().getResourceAsStream(filename);
            File inputFile = FileUtils.getFile(filename);
            //System.out.println("Looking in " + inputFile.getAbsolutePath() );
            input = FileUtils.openInputStream(inputFile);
                  
            if(input==null){
               
                printClassPath();
                System.out.println("Unable to find local properties file " + filename + ".\r\nTrying via http...");
                notifierProps = loadPropertiesFromURL(filename);
                DialogUtils.error( "Abort!", "file fail");
                    
            }
            else
            {
                //DialogUtils.error( filename, inputFile.getAbsolutePath());
                log.debug("loading props from " + inputFile.getAbsolutePath());
                //load a properties file from class path, inside static method
                notifierProps.load(input);
            }

        } catch (IOException ex) {
            DialogUtils.error("Props File Error", "unable to find props file. aborting.");
            log.error("giving up.");
            ex.printStackTrace();
            System.exit(-5);
        } finally{
            if(input!=null){
                try {
                    
                input.close();
            } catch (IOException e) {
                DialogUtils.error("Error", "unable to find props file");
                e.printStackTrace();
            }   
        
        }
        }
        
        return notifierProps;
    }
    
    private static Properties loadPropertiesFromURL(String filename) {
        Properties notifierProps = new Properties();
        URL url;
        try {
            url = new URL("http",EnvVariable.HOSTNAME.getVarValue(), Integer.parseInt(EnvVariable.PORT.getVarValue()), "/windchillNotifier/"+ filename);
            URLConnection  urlConn = url.openConnection();
            notifierProps.load(urlConn.getInputStream());         
/*          for(Object s : notifierProps.keySet()){
                System.out.println(s+"=" +notifierProps.get(s));
            }*/
            
            if( notifierProps.size() < 1 ) {
                System.out.println("No Props found [" + url.toString() + "]");
             // TODO: Should we bail here?
            }
        } catch (MalformedURLException e) {
            throwRemoteError(e);
            System.exit(-1);
        }  catch (IOException e) {
            
            throwError(e);
            System.exit(-1);
        } catch (Exception e)
        {
            DialogUtils.error("Properties Fail", "Please ensure properties file is in same directory as the plugin jar file, and the environment is set correctly");
            throwError(e);
            System.exit(-1); 
        }
        
     
    
        return notifierProps;
    }
    
    
    
    private static void throwError(Exception e) {
        DialogUtils.error("Properties Missing", "The Properties file required is missing. Please send this information to your admin.\n" + e );
        
    }
   private static void throwRemoteError(Exception e) {
        DialogUtils.error("Remote Properties Missing", "The Remote Properties file required is missing. Please send this information to your admin.\n" + e );
        
    }

   private static void printClassPath() {
       ClassLoader cl = ClassLoader.getSystemClassLoader();

       URL[] urls = ((URLClassLoader)cl).getURLs();

       for(URL url: urls){
           System.out.println(url.getFile());
       }
   }

  
  
    
    // test if we can see a given host
    private static boolean isReachable(String host){
        log.info("Can we reach host [" + host + "]?");
        
        if(Defs.PINGTEST.getValue().equals("true") && isPingable(host))
            return true;
        
        String URL = Defs.WINDCHILLPROTOCOL.getValue()+"://"+host;    
        if(isWorkingURL(URL)) {
            return true;
        } else {
            return false;
        }

    }
    
    private static boolean isWorkingURL(String URL)
    {
        log.info("Can see URL " + URL + " in " + Defs.HTTPTIMEOUT.getValueAsInt() +"ms? ...");
        boolean retval = false;
        
        try {
            URL u = new URL ( URL );
            HttpURLConnection huc =  ( HttpURLConnection )  u.openConnection (); 
            huc.setRequestMethod ("GET");  //OR  huc.setRequestMethod ("HEAD"); 
            huc.setConnectTimeout( Defs.HTTPTIMEOUT.getValueAsInt() );
            huc.connect () ; 
            int code = huc.getResponseCode() ;
            log.info("got " + code + " from " + URL + ".");
            if (code==HttpStatus.SC_OK)
            {
               
                log.info("... yes we can.");
                return true;
            }
                
        } catch (IOException e) {
        
        }
        
        
        log.info("... no we cannot");
        return retval;
    }
    
    
    // test if we can see a given host
    private static boolean isPingable(String host){
        log.info("Can we ping host [" + host + "]? ...");
        
        Runtime runtime = Runtime.getRuntime();
        Process proc;
        boolean retval = false;
        try {
            proc = runtime.exec("ping " + host);
            int mPingResult = proc .waitFor();
            if(mPingResult == 0){
                log.info("outstanding. Host is reachable.");
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } //<- Try ping -c 1 www.serverURL.com
        catch (InterruptedException e) {
            e.printStackTrace();
            
        }
        
        log.info(".. no we cannot!");
        return retval;

    }
    

}
