package com.nanga.windchillNotifier;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Helper class to wrap the logic that decides what target the notifier should call
 *  rules:
 *  "1. Windchill Document Number is empty: do "create"
 *   (Windchill feeds back the document number and state into the relevant ILM fields) 
 *   2. Windchill Document Number is not empty (e.g. 123454321) and Windchill Document State is not "Released" : do "update
 *   document 123454321"
 *   3. Windchill Document Number is not empty (e.g. 123454321) and Windchill Document State is "Released" : do "revise
 *   document 123454321"
 * "
 * @author col
 *
 */
public class DocRulesProcessor {
    
    private final static Logger log = LoggerFactory.getLogger(DocRulesProcessor.class.getName());

    private String targetType = "";
    public DocRulesProcessor(Report report) {
        log.debug("Creating Document Rules Processor.");
        String documentNumber = report.getPlmDocNumber();
        String documentState = report.getPlmDocState();
        
        if(documentNumber==null){
            log.debug("No PLM document number, so we are going to aim for the create target.");
            targetType="create";
        } else { // Check for revise or update
            
            
            assert(documentNumber!=null);
            
            
            targetType = "update";
            
            if(documentState!=null) {
                List<String> validStates = Defs.DRP_STATESTOREVISE.getListValue();//Defs.PFC_CREATE_VALIDSTATES.get
                
                log.debug("valid states: " + validStates.toString());
                log.debug("state : " + documentState);
                for(String s : validStates) {
                    if(s.contains(documentState)) {
                        targetType="revise";
                        log.debug("valid state to revise matched");
                    }
                } // for
            } else {
                log.info("PLM State field is empty on the item, will attempt to update");
            }
            log.debug("deduced target to be " + targetType);
        }// else
    }

    public boolean cmdIsCreate() {
        return targetType.equals("create");

        
    }

    public boolean cmdIsUpdate() {
        return targetType.equals("update");

       
    }

    public boolean cmdIsRevise() {
        return targetType.equals("revise");
    }

}
