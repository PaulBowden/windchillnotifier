package com.nanga.windchillNotifier;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;

import com.nanga.windchillNotifier.Report;

/**
 * This defines all of the properties and allows accessing them and their properties in a common way.
 * Using an ENUM allows more flexibility in the way properties are handled than using static final variables and also
 * gives us the opportunity to have a more global access to variables as needed.
 * 
 * @author cwhite
 *
 */
public enum Defs {
    PROPERTIESFILENAME ("props.filename","NotifierProperties.properties"), // define it here, but makes little sense to override!
	ID ("fieldname.id", "ID"),
	TEXT("fieldname.text","Text"),
	STATEFIELD("fieldname.state", "State"),
	HEADERFIELD("fieldname.headervalue","Text"),
	TYPE("fieldname.type","Type"),
	DOCTITLE("fieldname.doctitle","Summary"),
	CATEGORY("fieldname.category","Category"),
	PRODUCT("fieldname.product","Project"),
    PLMTARGETSTATE("fieldname.plmtargetstate","PLM State"),
	PLMDOCNUMBER("fieldname.plmdocnumber","Windchill Item"),
	PLMDOCSTATE("fieldname.plmdocstate","Windchill Document State"),
	DRP_STATESTOREVISE("props.plmstatestorevise","released,ready"),
    
	PLMDOCVERSION("fieldname.plmdocVersion","PLM Document Version"),
	PLMDOCLOCATION("fieldname.plmdocLocation","Windchill Location"),
	PLMNOTIFIERSTATUS("fieldname.plmnotifierStatus","PLM Notifier Status"),
	
	USEPROJECTPRODUCT("props.useprojectitem", "true"),
	ILMPROJECTREL("props.projectrelationship", "Documents"),
	USETARGETRULES("props.usetargetrules","false"),
	PLMPRODUCTFIELD("fieldname.plmProject","PLM Product"),
	
    
	NONMEANINGFULCATEGORIES("categories.nonmeaningful", true),	
	
	DOCTITLEMAXSIZE("props.doctitlemaxsize", "100"),
	NOTIFIERLOGFILE("props.notifier.log","NotifierTest.log"),
	TEMPLATERESPONSECREATE("props.test.response.create","test.create.response"),
	TEMPLATERESPONSEUPDATE("props.test.response.update","test.update.response"),
	TEMPLATERESPONSEREVISE("props.test.response.revise","test.revise.response"),
	
	WINDCHILLHOST("windchill.hostname","icenterv01"),
	WINDCHILLPROTOCOL("windchill.protocol","http"), 
	WINDCHILLUSERNAME_KEY("windchill.username.key","username"),
	WINDCHILLUSERNAME_VALUE("windchill.username.value","wcadmin"),
	
	WINDCHILLPASSWORD_VALUE("windchill.password.value", ""),
	WINDCHILLAUTHRETRIES("windchill.auth.retries", "3"),
	
	WINDCHILLCREATEDOC("windchill.ep.createdoc","create.xml"),
	WINDCHILLREVISEDOC("windchill.ep.revisedoc","reviseDoc.xml"),
	WINDCHILLUPDATEDOC("windchill.ep.updatedoc","updateDocState.xml"),
	RUNINTESTMODE("props.testmode","false"),

	PINGTEST("props.usepingtest","true"),
	HTTPTIMEOUT("props.httptimeout.ms","3000"),
	
	
	PFC_CREATE_VALIDSTATES("preflight.check.create.validstates","open,ready"),
	PFC_CREATE_FAILMESSAGE("preflight.check.message.fail","Please override message in props file!"),
	PFC_CREATE_OVERRIDE("preflight.check.create.override","false"),
	
	
    WINDCHILLPRODUCTNAME_KEY("windchill.productname.key","ProductKey"),
    WINDCHILLPRODUCTNAME_VALUE("windchill.productname.value",""), 
    WINDCHILLFOLDERNAME_KEY("windchill.productfolder.key","folder"),
    WINDCHILLFOLDERNAME_VALUE("windchill.productfolder.value",""),
    WINDCHILLDOCNAME_KEY("windchill.documentname.key","documentName"),
    WINDCHILLDOCNAME_VALUE("windchill.documentname.value",""),
    WINDCHILLDOCNUMBER_KEY("windchill.documentnumber.Key","docNumber"),
    WINDCHILLDOCNUMBER_VALUE("windchill.documentnumber.value",""),
    WINDCHILLDOCVERSION_KEY("windchill.documentversion.Key","docVersion"),
    WINDCHILLDOCVERSION_VALUE("windchill.documentversion.value",""),
    WINDCHILLTARGETSTATE_KEY("windchill.targetstate.key","targetState"),
    WINDCHILLTARGETSTATE_VALUE("windchill.targetstate.value",""),
    
    WINDCHILLDESCRIPTION_KEY("windchill.description.key","windchillDescription"),
    WINDCHILLDESCRIPTION_VALUE("windchill.description.value","");
	
	private String propertyName; // The property name as it appears in the property file
	private ImageIcon icon; // The icon, if necessary. 
	private String defaultValue; // the default value, if there is one.
	private boolean isList = false; //Is this a list property. (ie a comma separated String)
	
	Defs(String value){
		this(value, null);
	}
	Defs(String value, boolean isList){
		this(value, null);
		this.isList = isList;
	}
	Defs(String value, String defaultValue){
		this(value, defaultValue, null);
	}
	Defs(String value, String defaultValue, String iconPath){
		this.propertyName = value;
		this.defaultValue = defaultValue;
		if(iconPath!= null){ // Only set the icon if there is one specified. Right now this is only needed for the Segment Types.
			String iconStr = Report.getProps().getProperty(iconPath);			
			if(iconStr!=null){
				try {					
					this.icon = new ImageIcon(new URL(iconStr));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @return the icon
	 */
	public ImageIcon getIcon() {
		return icon;
	}
	/**
	 * @return the hasIcon
	 */
	public boolean hasIcon() {
		return this.icon != null;
	}
	
	public int getValueAsInt() {
	    return Integer.parseInt( Report.getProps().getProperty(propertyName,defaultValue) );
	}
	/**
	 * Retrieve the property value
	 * @return the the property value, or the default value if specified. <tt>null</tt> otherwise.
	 */
	public String getValue() {			
		return Report.getProps().getProperty(propertyName,defaultValue);
	}
	/**
	 * This returns the values of a property as a List of Strings. Convenience method. If the property isn't a comma delimited list, it just
	 * returns the value of the property as the first element in the List.
	 * @return
	 */
	public List<String> getListValue(){
		List<String> strs = new ArrayList<String>();
		String value = getValue();
		
		if(value == null)
			return strs;
		
		if(!isList){
			strs.add(value);
		}
		else{
				strs = Arrays.asList(value.split(","));
		}
		return strs;
	}
	
	/**
	 * Returns the Defs object that is the reference for the property name given as a parameter.
	 * @param prop
	 * @return
	 */
	public static Defs findDef(String prop) {
		if(prop ==null)
			return null;
		for(Defs d : Defs.values()){			
			if(d.getPropertyName().equals(prop))
				return d;
		}
		return null;
	}
	/**
	 * Return the property name represented by this Enum value.
	 * @return
	 */
	public String getPropertyName() {
		return propertyName;
	}
}
