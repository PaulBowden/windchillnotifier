package com.nanga.windchillNotifier;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;

import com.mks.api.response.Response;
import com.mks.api.response.ValueList;
import com.mks.api.response.WorkItem;
import com.mks.api.response.WorkItemIterator;
import com.mks.services.lib.Connection;
import com.mks.services.lib.IllegalFieldOperationException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.MKSItem;
import com.nanga.windchillNotifier.Defs;
import com.nanga.windchillNotifier.ui.DialogUtils;

public class IntegrityDoc {
    
    private final static Logger log = LoggerFactory.getLogger(IntegrityDoc.class.getName());
	private List<Map.Entry<String,String>> documentMDataFields;
	private String title;
	private int ID;
	private MKSItem item;
	private static final int MAXSIZE = Integer.parseInt(Defs.DOCTITLEMAXSIZE.getValue());

	public IntegrityDoc(String ID, String title, MKSItem item){
		this.title = title;
		this.documentMDataFields = new ArrayList<Map.Entry<String,String>>();	
		this.ID = Integer.parseInt(ID);
		this.item = item;
	}
	public IntegrityDoc(String ID, String title){
		this(ID, title, null);
	}
	
	public void add(Map.Entry<String,String> i){
		documentMDataFields.add(i);
	}
	public List<Map.Entry<String,String>> getCells(){
		return documentMDataFields;
	}	

	public Map.Entry<String,String> getCellAt(int i){
		return documentMDataFields.get(i);
	}
	
	public int getNumberOfItems() {
		return documentMDataFields.size();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	public int getID() {
		return ID;
	}	

	/**
	 * @return the item
	 */
	public MKSItem getItem() {
		return item;
	}
	
	public String getFieldFromItem(String fieldname){
		if(item == null)
			return null;
		String s;
		try {
			s = item.getFieldValueAsString(fieldname);
		} catch (IllegalFieldOperationException e) {
			return null;
		}		
		return s;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(MKSItem item) {
		this.item = item;
	}

		
	public static IntegrityDoc buildDoc(Connection conn, MKSItem docItem) {
		String id = "";
		String title = "";
		
		assert(conn!=null);

		if(docItem==null)
		{
		    log.error("Unable to read a document from " + conn.getCmdRunner().getDefaultHostname() + ":"+ conn.getCmdRunner().getDefaultPort()+". Document cannot be read.");
		    DialogUtils.error("Connection Error", "No document available. Is Integrity connected? [" + conn.getCmdRunner().getDefaultHostname() + ":" + conn.getCmdRunner().getDefaultPort() + "]");
		    log.error("giving up.");
		    System.exit(-2);
		}
		
		
		try {
			id = docItem.getFieldValueAsString(Defs.ID.getValue());
			title = docItem.getFieldValueAsString(Defs.DOCTITLE.getValue());
			
			setMetaDatafields();
		} catch (IllegalFieldOperationException e) {
			e.printStackTrace();
		}
		 
		IntegrityDoc d = new IntegrityDoc(id,( title.length() > MAXSIZE ? (title.substring(0, MAXSIZE-3)+"...") : title),docItem);		
		Response r = null;		
		//r = ApiUtils.viewSegment(conn,id);
		
				
		return d;
	}
	
	
    private static void setMetaDatafields() {
        // TODO Auto-generated method stub
        
    }
    
    
    public List<String> getRelatedIDs(String relationship) {
        
        
        log.debug("navigating relationship " + relationship);
      
        List<String> theIDs = null;
      
        if(item==null)
        {
            log.error("Item is null."); /// what were you thinking?
            log.error("bailing out");
            System.exit(-2);
        }
        
        theIDs = item.getRelatedIDs(relationship);
        log.debug("got " + theIDs.size() + " related item(s)");
        
        return theIDs;
    }
	
	
}
