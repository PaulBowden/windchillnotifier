package com.nanga.windchillNotifier.target;

import com.nanga.windchillNotifier.Report;
import com.nanga.windchillNotifier.target.CreateTarget;

public class TargetFactory {

    
        public static Target getTarget(String targetType, Report report){
           if(targetType == null){
              return null;
           }     
           if(targetType.equalsIgnoreCase("CREATE")){
              return new CreateTarget(report);
              
           } else if(targetType.equalsIgnoreCase("REVISE")){
               assert(true);
               return null;//new ReviseTarget(report);
              
           } else if(targetType.equalsIgnoreCase("UPDATE")){
               assert(true);
              return null;// new UpdateTarget(report);
           }
           
           return null;
        }
}
