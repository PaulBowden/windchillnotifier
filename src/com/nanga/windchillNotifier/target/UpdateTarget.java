package com.nanga.windchillNotifier.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nanga.windchillNotifier.Defs;
import com.nanga.windchillNotifier.EnvVariable;
import com.nanga.windchillNotifier.PLMResponse;
import com.nanga.windchillNotifier.Report;

public class UpdateTarget extends Target {
    private final static Logger log = LoggerFactory.getLogger(UpdateTarget.class.getName());
    
//    https://plm-test.asta.platform.solutions/Windchill/servlet/IE/tasks/ext/inneo/updateDocState.xml?targetState=IN%20REVIEW&docNumber=0000001483&docVersion=A&username=admin
//http://windchill-11-0.windchill.local/Windchill/servlet/IE/tasks/ext/inneo/createDoc.xml?productName=Product%20DEF&username=inneoadmin&docName=Test%20Spec%20Example&folder=/Default/Documents&desc=12345678-test.txt
    
    public UpdateTarget(Report report) {
        super("UPDATE");
        this.setEndpoint(Defs.WINDCHILLUPDATEDOC.getValue());
        
       /*  Example of what could be sent... check the defs for what we needed during the project
        *  this.addParameter(Defs.WINDCHILLPRODUCTNAME_KEY.getValue(), Defs.WINDCHILLPRODUCTNAME_VALUE.getValue());
        * this.addParameter(Defs.WINDCHILLUSERNAME_KEY.getValue(),    Defs.WINDCHILLUSERNAME_VALUE.getValue());
        * this.addParameter(Defs.WINDCHILLDOCNAME_KEY.getValue(),     report.getTitle()); // document Title
        * this.addParameter(Defs.WINDCHILLFOLDERNAME_KEY.getValue(), Defs.WINDCHILLFOLDERNAME_VALUE.getValue());
        * this.addParameter(Defs.WINDCHILLDESCRIPTION_KEY.getValue(), report.getDocId() + "-" + report.getTitle());
        * this.addParameter(Defs.WINDCHILLUSERNAME_KEY.getValue(),    Defs.WINDCHILLUSERNAME_VALUE.getValue());
       */
        this.addParameter(Defs.WINDCHILLUSERNAME_KEY.getValue(),    EnvVariable.USER.getVarValue());
        this.addParameter(Defs.WINDCHILLDOCNAME_KEY.getValue(),         report.getTitle()); // document Title
       // this.addParameter(Defs.WINDCHILLTARGETSTATE_KEY.getValue(),     report.getPlmTargetState()); target state comes fromt he command line...
        this.addParameter(Defs.WINDCHILLDOCNUMBER_KEY.getValue(),       report.getPlmDocNumber());
        this.addParameter(Defs.WINDCHILLDOCVERSION_KEY.getValue(),      report.getPlmDocVersion());
        
        
            
    }

    @Override
    public void processReponse(PLMResponse plmResponse) {
        
        
    }

    @Override
    public void processReponse(PLMResponse plmResponse, Report report) {
        log.debug("processing a response");
    }
    

}
