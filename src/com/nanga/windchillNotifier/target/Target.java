/*
 ***************************************
 * Copyright (c) 2017 Nanga Systems UK *
 ***************************************
 */
package com.nanga.windchillNotifier.target;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nanga.windchillNotifier.Defs;
import com.nanga.windchillNotifier.Notifier;
import com.nanga.windchillNotifier.PLMResponse;
import com.nanga.windchillNotifier.Report;

import java.util.Properties;
import java.util.Set;

/**
 * Represent the thing  / url we have to notify. The Target request is built up of a bunch of things which 
 * can change: the host, the protocol, the end point, and the parameters. This base class provides the 
 * basic mechanisms, then there can be specialised classes for each of the different types of request that
 * the notifier will need to make
 * @author col
 *
 */
public abstract class Target {
    private final static Logger log = LoggerFactory.getLogger(Target.class.getName());
    /**
     * http://windchill-11-0.windchill.local/Windchill/servlet/IE/tasks/ext/inneo/createDoc.xml?productName=Product%20DEF&username=inneoadmin&docName=Test%20Spec%20Example&folder=/Default/Documents&desc=12345678-test.txt
     */
    
    private String hostname; // windchill-11-0.windchill.local
    private String protocol; // http:    
    private String endpoint; // Windchill/servlet/IE/tasks/ext/inneo/createDoc.xml
    private String type;     // just a friendly string to help us remember what type of target this is
    
    private Map<String, String> parameters; // all that other stuff!
    
    private String fullRequest;

    
    
    
    /**
     * Create the representation of what we have to send to the plm system
     * expected to be overridden according to the required functionality.
     * @param type
     */
    public Target(String type) {
        this.type = type;
        this.hostname = Defs.WINDCHILLHOST.getValue();
        this.protocol = Defs.WINDCHILLPROTOCOL.getValue();
        parameters = new HashMap<String,String>();
        
        
        
        if(Defs.RUNINTESTMODE.getValue().equalsIgnoreCase("true")) {  // blast the hostname so we use offline testing.
            this.hostname="OfflineTesting";
        }
        assert(!this.type.isEmpty()); // make sure we at least have a target type
        
    }
    
    
    public String getType() {
        return type;
    }
    

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * @return the protocol
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * @param protocol the protocol to set
     */
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    /**
     * @return the endpoint
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * @param endpoint the endpoint to set
     */
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * @return the parameters
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
    
    /**
     * Add a parameter to the target we are going to request
     * @param key
     * @param value
     */
    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    /**
     * @return the fullRequest
     */
    public String getFullRequest() {
        log.debug("full request. host. Protocol, hostname:" + protocol + "/" + hostname);
        log.debug("endpoint:" + endpoint);
        
        fullRequest = protocol + "://" + hostname + "/" + endpoint + getParametersAsString();
        return fullRequest;
    }


    /**
     * return a utf-8 encoded string of the parameters
     * 
     * @return
     */
    private String getParametersAsString() {
        log.debug("target parameters as string....");
        String p = "";
        
        if(parameters.isEmpty()){
            return p;
        } else {
          p="?";
          Set<Entry<String, String>> set = parameters.entrySet();
          Iterator<Entry<String, String>> iterator = set.iterator();
          
          while(iterator.hasNext()) {
             Map.Entry<String, String> mentry = (Map.Entry<String,String>)iterator.next();
             
             String value; // need to do stuff if the map entry is missing...
             if(mentry.getValue()==null)
             {
              value = "null";
              log.info("NOTE: we have no value in param map for the key " + mentry.getKey() + ".");
              log.error("Check Integity fields are setup and populated right");
              continue;
             } else { value = mentry.getValue(); }
             try {
                
                log.debug("param: "+ mentry.getKey() + "=" + value); 
                
                p = p + mentry.getKey() + "=" + URLEncoder.encode(value, "UTF-8") + "&";
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
          }
          
          
        }
        
        // dont return the last ampersand added by the loop above
        return (p.substring(0, p.length() - 1));
    }
        
    public String toString() {
        String retval = null;
        String p = "";
        Set<Entry<String, String>> set = parameters.entrySet();
        Iterator<Entry<String, String>> iterator = set.iterator();
       
        retval ="\n--------\n";
        retval +="connection:"+this.protocol+"://"+ this.hostname + "" + this.endpoint + "\n";
        
        while(iterator.hasNext()) {
           Map.Entry<String, String> mentry = (Map.Entry<String,String>)iterator.next();
           //System.out.println("param: "+ mentry.getKey() + "=" + mentry.getValue());
                  p = p + mentry.getKey() + "=" + mentry.getValue() + "\r\n";
        }
        
        retval += p;
        retval += "--------\n";
        return retval;
    }


    


    public abstract void  processReponse(PLMResponse plmResponse);


    public abstract void processReponse(PLMResponse plmResponse, Report report);


    /**
     * Check arbitrary business rules which can be set from the properties file
     * @return true if safe to continue
     */
    public boolean doPreflightChecks() {
        
        return true;
    }
    
    
}
