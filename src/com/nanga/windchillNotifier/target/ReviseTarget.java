package com.nanga.windchillNotifier.target;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mks.api.response.APIException;
import com.nanga.windchillNotifier.ApiUtils;
import com.nanga.windchillNotifier.Defs;
import com.nanga.windchillNotifier.EnvVariable;
import com.nanga.windchillNotifier.Notifier;
import com.nanga.windchillNotifier.PLMResponse;
import com.nanga.windchillNotifier.Report;
import com.nanga.windchillNotifier.ui.DialogUtils;

public class ReviseTarget extends Target {

    private final static Logger log = LoggerFactory.getLogger(ReviseTarget.class.getName());
    //http://windchill-11-0.windchill.local/Windchill/servlet/IE/tasks/ext/inneo/createDoc.xml?productName=Product%20DEF&username=inneoadmin&docName=Test%20Spec%20Example&folder=/Default/Documents&desc=12345678-test.txt
    
    
    private Report report = null;
    
    public ReviseTarget(Report report) {
        super("REVISE");
        this.setEndpoint(Defs.WINDCHILLREVISEDOC.getValue());
        
        this.addParameter(Defs.WINDCHILLUSERNAME_KEY.getValue(),    EnvVariable.USER.getVarValue());
        this.addParameter(Defs.WINDCHILLDOCNUMBER_KEY.getValue(),     report.getPlmDocNumber()); 
        
    
    }
    
    
    /**
     *  Override the parent class
     *  callers should set the report so we can get integrity connection details
     *  1. update the integrity document with 
     *   - status
     *   - location
     *   - document number
     *   
     * @param report 
     */
    @Override
    public void processReponse(PLMResponse plmResponse, Report report) {
        log.info("create target has a plm reponse to process");
        
        this.report = report;
        
        if(this.report == null) {
            log.error("You must call setReport, or pass in a valid report on a create target before processing the reponse, since it needs the integrity connection to populate at least the status field");
            log.error("giving up as report is null");
            System.exit(-3);
            
        }
        
        // if it's crap, then just update the status field else go for it
        
        
        // update the integrity fields
        try {
        
            if(plmResponse.getException() == null ) {
                ApiUtils.editIssue(report.getConnection(), report.getDocId(), Defs.PLMDOCLOCATION.getValue(), plmResponse.getLocation());
                ApiUtils.editIssue(report.getConnection(), report.getDocId(), Defs.PLMDOCNUMBER.getValue(),   plmResponse.getNumber());
                ApiUtils.editIssue(report.getConnection(), report.getDocId(), Defs.PLMNOTIFIERSTATUS.getValue(), "Created " + plmResponse.getName()+ "-" + plmResponse.getNumber() +" document at " + plmResponse.getLocation());
            }
            else {
        
                // attempt to Set the integration Status field
        
                ApiUtils.editIssue(report.getConnection(), report.getDocId(), Defs.PLMNOTIFIERSTATUS.getValue(), "Error! - unable to create document in Windchill. Please check plugin settings and logs");
            }
        } catch (APIException e) {
            DialogUtils.error("Update Error", "Unable to set status in Integrity. Please urgently discuss the log with your administrator.");
            log.error("Api Exception. Wonderful. " + e.getMessage());
            e.printStackTrace();
        }
        
        log.info("reponse processed by the create target");
    }


    @Override
    public void processReponse(PLMResponse plmResponse) {
        // TODO Auto-generated method stub
        assert(false);
        log.error("coding fail");
    }
    
}
