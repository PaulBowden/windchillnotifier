package com.nanga.windchillNotifier;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mks.services.lib.Connection;
import com.mks.services.lib.IllegalFieldOperationException;
import com.mks.services.lib.im.MKSItem;
import com.nanga.windchillNotifier.ui.DialogUtils;


/**
 * Report is the information we need from Integrity, that we need to use to interact with windchill
 * @author cwhite
 *
 */
public class Report {
    private final static Logger log = LoggerFactory.getLogger(Notifier.class.getName());
    private IntegrityDoc docOne;	
	private Connection conn;
    private String user;
    private String hostname;
    private String port;
    private String title;
    private String docId;
    private String state;
    private String product;  // the name of the product area where the 
    
    //private String plmTargetState; - we only need the state from the commandline... maybe
    private String plmDocLocation = null; /// The folder where the document lives in the PLM system
    private String plmDocNumber   = null;  /// is it better to be setting these to "" ?
    private String plmDocVersion  = null;
    private String plmDocState    = null;
	
	private static Properties notiferProps = new Properties();
	
	/***
	 *  Report is the stuff we need from Integrity to notify windchill with...
	 *  ... and our properties file(!)
	 *  
	 *  Fields: Document Name
	 *    Username
	 *    Integrity doc UID
	 * @param notiferProps
	 */
	
	public Report(Properties notiferProps){	
	    
	    
		NotifierConnection nc = new NotifierConnection();
		conn = new Connection(nc.getCmdRunner());
		Report.notiferProps = notiferProps;
				
	
        if(null==nc || null==conn)
        {
            DialogUtils.error("Connection Error", "Unable to make an integrity conenction, please check your properties file");
            System.exit(-2);
        }
        
        
		
		
		try {
					
			log.info(EnvVariable.NUMOFITEM.getVarValue() + " items");
			if(EnvVariable.NUMOFITEM.getVarValue().equals("1")) {
			    
			    // Grab the selected item when we were called by sniffing the environment variable
			    MKSItem segmentOneItem = MKSItem.getItem(Integer.parseInt(EnvVariable.ITEMONEID.getVarValue()), conn);
			    
				docOne = IntegrityDoc.buildDoc(conn, segmentOneItem);
				title = docOne.getTitle();
				hostname = EnvVariable.HOSTNAME.getVarValue();
				port     = EnvVariable.PORT.getVarValue();
                user     = EnvVariable.USER.getVarValue();
                docId    = Integer.toString(docOne.getID());
               
                
                // We either get the product name from the Integrity document or from it's corresponding project item
                if(Defs.USEPROJECTPRODUCT.getValue().equals("true"))
                {
                    log.info("Fetching PLM Product details from project item.");
                    List <String> relatedItems = docOne.getRelatedIDs(Defs.ILMPROJECTREL.getValue());
                                        
                    // TODO: Make this more efficient by extending the IntegrityDoc object to return the project item directly
                    // In the mean time we will this slightly inefficient thing
                    assert(relatedItems.size() > 0);
                    
                    log.debug("We take the first item on the other end of the " + Defs.ILMPROJECTREL.getValue() + " relationship ID: " + relatedItems.get(0));
                    MKSItem projectItem  = MKSItem.getItem(Integer.parseInt(relatedItems.get(0)), conn);
                    
                   log.debug("Attempting to get value from Integrity project item field " + Defs.PLMPRODUCTFIELD.getValue() + ".");
                    // find where the doc should be stored in Windchill
                    String productLocation  = projectItem.getFieldValueAsString(Defs.PLMPRODUCTFIELD.getValue());
                    log.info("Expected  PLM location: " + productLocation);
                    
                    product = productLocation;
                    
                } else { // we just use the field directly, the old way
                    product  = docOne.getFieldFromItem(Defs.PRODUCT.getValue()).replaceAll("^/+", ""); /// strip the leading space we see on project name field from integrity
                }
                //plmTargetState = docOne.getFieldFromItem(Defs.PLMTARGETSTATE.getValue()); - think we only get the state from the command line
                plmDocLocation = docOne.getFieldFromItem(Defs.PLMDOCLOCATION.getValue());
                state          = docOne.getFieldFromItem(Defs.STATEFIELD.getValue());
                plmDocNumber   = docOne.getFieldFromItem(Defs.PLMDOCNUMBER.getValue());
                plmDocState    = docOne.getFieldFromItem(Defs.PLMDOCSTATE.getValue());
			
			} else {
				DialogUtils.error("Incorrect Selection", "There is an incorrect selection detected. Please select a document.");
				System.exit(-2);
			}
		
		} catch (IllegalFieldOperationException e) {
			DialogUtils.error("Field Problem", "The Necessary Fields could not be read. Please contact your administrator.");
			System.exit(-2);

		} catch (Exception e) {
			e.printStackTrace();
			DialogUtils.error("Exception", e.getMessage());
			System.exit(-2);
		}
	
	} // constructor	
	
	
	// the fields Inneo specifies we need to tell windchill
	public String getUser(){       return(user); }
	public String getHostname(){   return(hostname); }
	public String getPort(){       return(port); }
	public String getTitle(){      return(title); }
	public String getDocId(){      return(docId); }
	public String getProduct(){    return(product); }
	public String getState(){      return(state); }
	
	
	//public String getPlmTargetState() { return(plmTargetState); }
	public String getPlmDocNumber()   { return(plmDocNumber);   }   /// the plmDocNumber (null if not set)
    public String getPlmDocVersion()  { return(plmDocVersion);  }
    public String getPlmDocState()    { return(plmDocState);    }

	
    public String getPlmDocLocation() { return(plmDocLocation); }
    

    
	/**
	 * access our properties settings
	 * @return
	 */
	public static final Properties getProps(){
		return notiferProps;
	}
	
	/**
	 * @return the conn
	 */
	public Connection getConnection() {
		return conn;
	}
	/**
	 * @return the docOne
	 */
	public IntegrityDoc getDocOne() {
		return docOne;
	}

	/**
	 * @param docOne the docOne to set
	 */
	public void setDocOne(IntegrityDoc docOne) {
		this.docOne = docOne;
	}

	
	public String toString() {
	    return "docId: " + docId + "\r\n" +
	            "state: " + state + "\r\n" +
	            "title: " + title + "\r\n" + 
	            "product:" + product + "\r\n" +
	            "plmDocLocation: " + plmDocLocation + "\r\n" + 
	            "plmDocNumber: "   + plmDocNumber   + "\r\n" + 
	            "plmDocVersion:"   + plmDocVersion  + "\r\n" +   
	            "plmDocState:"   + plmDocState  + "\r\n" +   
                
	           "hostname: " + hostname + "\r\n" +
	           "port: "     + port + "\r\n" +
	           "user: "     + user + "\r\n" ;
	}


    

  




    
	
}
