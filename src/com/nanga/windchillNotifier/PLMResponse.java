package com.nanga.windchillNotifier;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.nanga.windchillNotifier.ui.DialogUtils;


/**
 *  Simple wrapper class for response objects we get from windchill
 *  Attempts to handle the set of attributes we care about, so not all will be populated every time.
 * @author col
 *
 */

public class PLMResponse {

    private final static Logger log = LoggerFactory.getLogger(PLMResponse.class.getName());

    private String name;        /// the name of the document in windchill
    private String desc;        /// description as stored in windchill
    private String number;      /// document number
    private String versionInfo; /// version info from windchill
    private String exception = null;
    private String location;    /// (folder) location in windchill

    private Document document; 

    
    public PLMResponse(InputStream is) {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    
    try {
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        
        document = documentBuilder.parse(is);
        log.debug("created some kind of plmResponse. ");
      
    } catch (SAXException | IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (ParserConfigurationException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (NullPointerException e)
    {
        DialogUtils.error("Error parsing xml stream", "Please check the response config fields and the xml response are aligned");
        //e.printStackTrace();
    }
    }
 
    /**
     *  Process the XML we got back, populating our fields from the elements and attributes we received.
     */
    public void parse()
    {
        
        // TODO: Parameterise this from the properties file
        try { exception = document.getElementsByTagName("exception").item(0).getTextContent();                              }catch(Exception e){log.debug("suppressed parse error for exception");};
        try { versionInfo = document.getElementsByTagName("versionInfo.identifier.versionId").item(0).getTextContent();     }catch(Exception e){log.debug("suppressed parse error for version info");};
        try { name = document.getElementsByTagName("name").item(0).getTextContent();                                        }catch(Exception e){log.debug("suppressed parse error for name");};
        try { desc = document.getElementsByTagName("description").item(0).getTextContent();                                 }catch(Exception e){log.debug("suppressed parse error for description");};
        try { number = document.getElementsByTagName("number").item(0).getTextContent();                                    }catch(Exception e){log.debug("suppressed parse error for number");};
        try { location = document.getElementsByTagName("folder.key").item(0).getTextContent();                              }catch(Exception e){log.debug("suppressed parse error for location");};


        
        log.info("exception:" + exception);
        log.info("version:" + versionInfo);
        log.info("location:" + location);
        log.info("name:" + name);
        log.info("desc:" + desc);
        log.info("number:" + number);
        
    }

    public String toString() {
        return  "Name " + name        + "\n" + 
        "Description " + desc         + "\n" +
        "Document Number " + number   + "\n" +
        "Version Info " +versionInfo  + "\n" +
        "Exception" + exception       + "\n" +
        "Location" + location;    /// (folder) location in windchill

    }
    
    private static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        transformer.transform(new DOMSource(doc), 
             new StreamResult(new OutputStreamWriter(out, "UTF-8")));
    }
    /**
     * get the text content of the response
     * @return
     */
    public String getRaw() {
        assert (document!=null);
        return document.getTextContent();

    }

    /**
     * return the windchill exception text if we have it, otherwise, null
     */
    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public Object getStatus() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the versionInfo
     */
    public String getVersionInfo() {
        return versionInfo;
    }

    /**
     * @param versionInfo the versionInfo to set
     */
    public void setVersionInfo(String versionInfo) {
        this.versionInfo = versionInfo;
    }

    /**
     * @return the document
     */
    public Document getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    
    
}
