/**
 * 
 */
package com.mks.services.lib;

import java.util.List;
import java.util.Properties;

import com.mks.services.lib.im.*;

/**
 * The CLIOptions Class is meant to "optionize" all the CLI options for any CLI command you wish to generate.
 * This extends the Property Class to provide the access to the Individual Options and all the Options easier.<br>
 * The most important method is the getOption(String s) method. It should be used for all calls to get an Option instead of 
 * getProperty();
 *  
 * 
 * @author cstoss
 *
 */
public class CLIOptions extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = -479376033695248978L;
	
	/**
	 * This should be used instead of the Property.getPropery() method to retrieve your
	 * Command Line options from the CLIOptions object. The automatically formats the options
	 * to be in a CLI argument.
	 * 
	 * @param s The option you wish to get
	 * @return A CLI ready formatted option. If the option is not set in the Type, an empty String is returned;
	 */
	public String getOption(String s)
	{		
		
		if(this.getProperty(s) == com.mks.services.lib.GlobalVariable.NULL_VALUE)
			return "";
		
		if(s.startsWith("--"))
			return s + "=\"" + this.getProperty(s) +"\" ";
		else
			return this.getProperty(s) + " ";
	}
	
	
	/**
	 * This method overrides the Property Class and reverts to getOption(String s). This method is preferred
	 * 
	 * @deprecated Use getOption(s)
	 */
	public String getProperty(String s, String v)
	{
		return getOption(s);
	}
		
	public String optionizeMandatoryFields(List<State> states)
	{
		String retStr ="";
		int numStates = states.size();
		for(int i=0;i<numStates;i++)
		{
			State curState = states.get(i);
			List<IField> curFields = curState.getMandatoryFields();
			int numFi = curFields.size();
			if(numFi >0)
			{
				retStr+=curState.getName()+":";
				for(int j=0; j<numFi;j++)
				{				
					IField curFi = curFields.get(j);
					retStr+=curFi.getName();
					if(j<numFi-1)
						retStr+=",";
				}
				if(i<numStates-1)
					retStr+=";";
			}
		}
		return retStr;
	}
	
	public String optionizeFields(List<IField> fields) {
		String retStr= "";
		int numFields = fields.size(); 
		for(int i=0;i< numFields;i++)
		{
			IField curField =fields.get(i); 
			retStr+=curField.getName()+":";
			List<String> curList = curField.getPermittedGroups(); 
			int numPGs = curList.size();
			for(int j=0;j<numPGs;j++)
			{
				retStr += curList.get(j);
				if(j<numPGs - 1)
					retStr+=",";				
			}
			if(i<numFields-1)
				retStr+=";";		
		}
		return retStr;
	}
	
	public String optionizeStateTransitions(List<StateTransition> st)
	{	
		String retStr = "";
		int numSTs = st.size();
		for(int i=0;i<numSTs;i++)
		{
			StateTransition curST = st.get(i);
			retStr += curST.getFromState().getName() + ":" + curST.getToState().getName()+":";;
			List<Group> PGs = curST.getPermittedGroups();
			int numPGs = PGs.size();
			for(int j=0;j<numPGs;j++)
			{
				retStr+= PGs.get(j).getName();
				if(j<numPGs-1)
					retStr+=",";
			}
			if(i<numSTs-1)
				retStr+=";";
		}
		return retStr;
	}
	public String optionizePermittedAdmins(List<IMUserOrGroup> pa)
	{
		String retStr = "";
		String uStr = "";
		String gStr = "";
		
		int numPAs = pa.size();
		for(int i=0;i<numPAs;i++)
		{
			IMUserOrGroup curUG = pa.get(i);
			System.out.println(curUG.getName() + " - " +curUG.getClass().getName());
			if("com.mks.services.lib.im.User".equalsIgnoreCase(curUG.getClass().getName()))
			{
				uStr+=curUG.getName() +",";
			}
			else if("com.mks.services.lib.im.Group".equalsIgnoreCase(curUG.getClass().getName()))
			{
				gStr+=curUG.getName() +",";
			}
		}
		if(uStr.length()>0)
			uStr = "u="+uStr.substring(0, uStr.length()-1);
		if(gStr.length()>0)
			gStr = "g="+gStr.substring(0, gStr.length()-1);
		
		if(uStr.length()>0 && gStr.length() > 0)
			retStr = uStr +";" +gStr;
		else if(uStr.length() == 0 && gStr.length() > 0)
			retStr = gStr;
		else if(uStr.length() > 0 && gStr.length() == 0)
			retStr = uStr;
		else
			retStr = com.mks.services.lib.GlobalVariable.NULL_VALUE;
		
		return retStr;
	}
	public String optionizeTTAllowed(boolean att) {
		if(att)
			return "--enableTimeTracking";
		else
			return "--noenableTimeTracking";
	}

	public String optionizeCPsAllowed(boolean acps) {
		if(acps)
			return "--allowChangePackages";
		else
			return "--noallowChangePackages";
	}

	public String optionizeShowWorkflow(boolean swf) {
		if(swf)
			return "--showWorkflow";
		else
			return "--noshowWorkflow";
	}

	public String optionizePhaseField(PhaseField phaseField) {
		String retStr = "";
		List<Phases> phases = phaseField.getPhases();
		int numPhs = phases.size();
		//retStr = phaseField.getName();
		for(int i=0;i<numPhs;i++)
		{
			Phases curPh = phases.get(i);
			retStr += curPh.getPhaseText() + ":";
			List<State> Ss = curPh.getPhaseStates();
			int numSs = Ss.size();
			for(int j=0;j<numSs;j++)
			{
				retStr+= Ss.get(j).getName();
				if(j<numSs-1)
					retStr+=",";
			}
			if(i<numPhs-1)
				retStr+=";";
		}
		return retStr;		
	}

	public String optionizeNotificationFields(List<IField> notificationFields) {
		String retStr= "";
		int numFields = notificationFields.size(); 
		for(int i=0;i< numFields;i++)
		{
			IField curField =notificationFields.get(i); 
			retStr+=curField.getName();
			if(i<numFields-1)
				retStr+=",";		
		}
		return retStr;	
	}
	
	
}
