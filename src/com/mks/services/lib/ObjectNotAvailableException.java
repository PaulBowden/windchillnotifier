package com.mks.services.lib;

public class ObjectNotAvailableException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7685697761873929815L;

	/**
	 * This is a runtime exception as it is thrown by the Type object, specifically in the getType(String) method
	 * and there is nothing that can be done to recover from it.
	 *  
	 * 
	 * @param string
	 */
	public ObjectNotAvailableException(String string) {
		super(string);
	}
}
