package com.mks.services.lib;

public class IllegalValueDetectedException extends RuntimeException {
	private String value;
	private String fieldtype;
	/**
	 * 
	 */
	private static final long serialVersionUID = 5070332260038127289L;

		public IllegalValueDetectedException(String string) {
			super(string);
		}
		public IllegalValueDetectedException(String value, String fieldtype) {
			super(value + " is not a valid value for " + fieldtype);
			this.value = value;
			this.fieldtype = fieldtype;		
		}
		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		/**
		 * @return the fieldtype
		 */
		public String getFieldtype() {
			return fieldtype;
		}
		
}
