/**
 * 
 */
package com.mks.services.lib;

/**
 * @author cstoss
 *
 */
public class ConflictingAddRequestException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7533943334715787149L;

	/**
	 * Constructor
	 * 
	 * @param string
	 */
	public ConflictingAddRequestException(String string) {
		super(string);
	}
}
