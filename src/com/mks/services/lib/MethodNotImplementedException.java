package com.mks.services.lib;

/**
 * This exception is thrown when you attempt to access a method that cannot be used for the field type
 * you are currently accessing.
 * 
 * 
 * @author cstoss
 *
 */
public class MethodNotImplementedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 192841124401935547L;
	private String fieldType;
	private String method;
	
	public MethodNotImplementedException(String method, String fieldType) {
		super("");
		this.method = method;
		this.fieldType = fieldType;
	}
	
	public String toString(){
		return  super.toString() + "The " + method + "() method cannot be called for a field of type " + this.fieldType + ". ";
	}

	/**
	 * @return the fieldType
	 */
	public String getFieldType() {
		return fieldType;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}
	
}
