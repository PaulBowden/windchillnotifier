/**
 *
 */
package com.mks.services.lib;

/**
 * @author cstoss
 *
 */
public class GlobalVariable {

	public static final int FIELDNAME_MAXLENGTH=255;
	public static final String NULL_VALUE = "MKS_NULL_VALUE";
	public static final String FIRST_POSITION = "first";
	public static final String LAST_POSITION = "last";
	public static final String BEFORE_POSITION = "before";
	public static final String AFTER_POSITION = "after";
	public static final String COMPUTED_NEVER = "never";
	public static final String COMPUTED_DAILY = "daily";
	public static final String COMPUTED_WEEKLY = "weekly";
	public static final String COMPUTED_MONTHLY = "monthly";
	public static final String COMPUTED_DYNAMIC = "dynamic";
	public static final String COMPUTED_STATIC = "static";
	

	

}
