package com.mks.services.lib;

/**
 * This Exception is thrown when an illegal operation is performed when setting a field value.
 * 
 * You can use the throws {@link #getAffectedFieldName()} method to find out which field this was thrown on.
 *  
 * @author cstoss
 *
 */
public class IllegalFieldOperationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1448262676209018388L;
	private String affectedFieldName;
	private boolean isIBPL;
//	private String fieldsCurValue;
	
	
	public IllegalFieldOperationException(String string) {
		super(string);
	}
	public IllegalFieldOperationException(String string, String fieldname, boolean isIBPL) {
		super(string);
		this.setAffectedFieldName(fieldname);
		this.setIBPL(isIBPL);
	}
	
	/**
	 * The field that was being acted upon when the Exception was thrown
	 * @return the affectedFieldName
	 */
	public String getAffectedFieldName() {
		return affectedFieldName;
	}

	/**
	 * @param affectedFieldName the affectedFieldName to set
	 */
	public void setAffectedFieldName(String affectedFieldName) {
		this.affectedFieldName = affectedFieldName;
	}

	/**
	 * Is this field an IBPL. This is a convenience method that can be used to
	 * determine what action to take when the Exception occurs.
	 * @return the isIBPL
	 */
	public boolean isIBPL() {
		return isIBPL;
	}

	/**
	 * @param isIBPL the isIBPL to set
	 */
	public void setIBPL(boolean isIBPL) {
		this.isIBPL = isIBPL;
	}

	/**
	 * @return the fieldsCurValue
	 */
	/*public String getFieldsCurValue() {
		return fieldsCurValue;
	}*/

	/**
	 * @param fieldsCurValue the fieldsCurValue to set
	 */
	/*public void setFieldsCurValue(String fieldsCurValue) {
		this.fieldsCurValue = fieldsCurValue;
	}
	*/
	
	
}
