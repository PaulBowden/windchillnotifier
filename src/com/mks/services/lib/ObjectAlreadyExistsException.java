/**
 * 
 */
package com.mks.services.lib;

/**
 * @author cstoss
 *
 */
public class ObjectAlreadyExistsException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3126495625232707699L;

	/**
	 * Constructor
	 * 
	 * @param string
	 */
	public ObjectAlreadyExistsException(String string) {
		super(string);
	}
}
