package com.mks.services.lib.writers;

public interface IWriter {
	public void print();
	public void setFieldtype(int fieldtype);
	public int getFieldtype();
	public void printInt();
}
