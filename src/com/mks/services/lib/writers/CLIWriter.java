package com.mks.services.lib.writers;

import com.mks.services.lib.im.IField;

public class CLIWriter extends AbstractWriter implements IWriter {

	public CLIWriter(IField field){
		super(field);
	}

	@Override
	public void printInt() {
		System.out.println("Name: " + this.getField().getName());
	}
	
}
