package com.mks.services.lib.writers;

import com.mks.services.lib.im.FieldType;
import com.mks.services.lib.im.IField;

public abstract class AbstractWriter implements IWriter {
	private int fieldtype;
	private IField field;
	
	public AbstractWriter(IField field)
	{
		this.field = field;
	}
	
	/**
	 * @return the field
	 */
	public IField getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(IField field) {
		this.field = field;
	}

	public void print(){
		/*switch(getFieldtype()){
		case FieldType.INTEGER:
				printInt();
				break;
		}*/
	}

	/**
	 * @return the fieldtype
	 */
	public int getFieldtype() {
		return fieldtype;
	}

	/**
	 * @param fieldtype the fieldtype to set
	 */
	public void setFieldtype(int fieldtype) {
		this.fieldtype = fieldtype;
	}
	public abstract void printInt();
}
