/**
 * 
 */
package com.mks.services.lib.processing;

import java.util.HashMap;

import com.mks.services.lib.ConflictingAddRequestException;

/**
 * @author cstoss
 *
 */
public class FieldComparison {
	HashMap<String, Object> newField = new HashMap<String, Object>();
	HashMap<String, Object> existingField = new HashMap<String, Object>();
	
	public FieldComparison(HashMap<String, Object> newField, HashMap<String, Object> existingField) {
		this.newField=newField;
		this.existingField = existingField;
	}
	
	public void doCompare() throws ConflictingAddRequestException
	{
		if(newField.hashCode() == existingField.hashCode())
			return;
		
		if(newField.size() == existingField.size())
		{
			
		}
		else
		{
			findDifferences();
		}
	}
	
	private void findDifferences() throws ConflictingAddRequestException
	{
		throw new ConflictingAddRequestException("You have requested to add a Field '" + newField.get("name") + "' which is already specified with conflicting details:");
	}

}
