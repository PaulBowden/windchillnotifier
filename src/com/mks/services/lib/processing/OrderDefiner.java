/**
 * 
 */
package com.mks.services.lib.processing;

import com.mks.services.lib.im.*;
import com.mks.services.lib.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * @author cstoss
 *
 */
public class OrderDefiner {
	private List<Type> types =  new ArrayList<Type>();
	@SuppressWarnings("unchecked")
	private List<GeneralField> createOrder = new ArrayList<GeneralField>();
	private List<GeneralField> fields = new ArrayList<GeneralField>(); 
	private int numTypes;
	private int numFields;
	
	public OrderDefiner(Type type)
	{
		this.types.add(type);
	}
	public OrderDefiner (List<Type> types)
	{
		this.types = types;
	}
	
	public void doOrder()
	{
		setNumTypes(); //Sets the number of Types to iterate over.
		getAllFields(); //gets all the fields we need to create
		//retrieveFieldsOfType(GeneralField.FIELDTYPE_ATTACHMENT); //Attachment fields have no references and should be created first
		//retrieveFieldsOfType(GeneralField.FIELDTYPE_LONGTEXT);  // Long text fields can only reference Attachments
		
		for(GeneralField f : fields)
		{
			getReferences(f);
		}
	}
	
	private void getReferences(GeneralField f) {
		
		
	}
	private void retrieveFieldsOfType(int t)
	{
		Iterator<GeneralField> fi = fields.iterator();
		while(fi.hasNext())
		{
			GeneralField curF = fi.next();
			/*if(curF.getFieldType()== t)
			{
				createOrder.add(curF);
				fi.remove();
			}*/
		}
		
	}
	private void setNumTypes()
	{
		numTypes  = types.size();
	}
	
	
	
	private void getAllFields()
	{
		for(int i = 0; i<numTypes;i++)
		{
			List<IField> curFields  = types.get(i).getFields();
			int numCurFields = curFields.size();
			for( int j = 0; j<numCurFields;j++)
			{
				IField curField = curFields.get(j);
				int index = -1;
	/*			if((index = isFieldInList(curField))>=0)
					checkConflict(curField, index);
				else
					fields.add(curField);*/
			}
		}
		numFields = fields.size();
	}
	private void checkConflict(GeneralField f, int i)
	{
		//System.out.print("Conflcit Detected");
		/*FieldComparison fc = new FieldComparison(f.getComparitor(), fields.get(i).getComparitor());
		try{
			fc.doCompare();			
		}catch(ConflictingAddRequestException care)
		{
			care.printStackTrace();
			System.exit(-1);
		}*/
	}
	@SuppressWarnings("unchecked")
	private int isInList(Object o, List l)
	{
		if(!l.contains(o))
			return -1;
		else
			return l.indexOf(o);
	}
	private int isFieldInList(GeneralField f)
	{
		return isInList(f.getName(), getAllFieldNames());
	}
	
	private List<String> getAllFieldNames()
	{
		List<String> allFieldList = new ArrayList<String>();
		int numCurFields = fields.size();	
		for (int i=0;i<numCurFields;i++)
			allFieldList.add(fields.get(i).getName());
		return allFieldList;
	}
	
}
