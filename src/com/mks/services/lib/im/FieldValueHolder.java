/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;


/**
 * FieldValueHolder only contains protected methods. This is to ensure that
 * the Field Values are only modified by the public method available in the <tt>MKSItem</tt>.
 * To modify the field values outside of that Class would cause problems when doing updates to 
 * items.
 *  
 * @author cstoss
 *

 */
final class FieldValueHolder {
	private List<String> strListValueHolder;
	private List<IBPLValueHolder> ibplValueHolder;
	private boolean isIBPL;

	protected FieldValueHolder(String value)
	{
		strListValueHolder = new ArrayList<String>(5);
		strListValueHolder.add(value);
		ibplValueHolder = null;
		isIBPL = false;
	}

	protected FieldValueHolder(List<String> values)
	{
		strListValueHolder = values;
		ibplValueHolder = null;
		isIBPL = false;
	}
	protected FieldValueHolder(List<IBPLValueHolder> ibplValueHolder, boolean ibpl)
	{
		this.ibplValueHolder = ibplValueHolder;
		strListValueHolder=null;
		isIBPL = true;
	}
	protected List<String> getFieldValue(){
		return strListValueHolder;
	}
	protected List<IBPLValueHolder> getIBPLValue() {
		return ibplValueHolder;
	}
	
	/**
	 * @return the isIBPL
	 */
	protected boolean isIBPL() {
		return isIBPL;
	}

	protected void setIBPLValues(List<IBPLValueHolder> ibplValueHolder) {
		this.ibplValueHolder = ibplValueHolder;		
	}
	protected void setStringValue(String value) {
		this.strListValueHolder.clear();
		this.strListValueHolder.add(value);
	}
	protected void setStrListValue(List<String> values) {
		this.strListValueHolder = values;	
	}
}
