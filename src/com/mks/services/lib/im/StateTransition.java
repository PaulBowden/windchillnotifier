/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a State Transition for a Type's workflow.
 * 
 * @author cstoss
 *
 */
public class StateTransition implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -984809238824881862L;
	private State fromState;
	private State toState;
	private List<Group> permittedGroups = new ArrayList<Group>();
	private boolean isSelfTransition;
	
	/**
	 * Constructs a transition between 2 States
	 * 
	 * @param fromState The Start State of the transition
	 * @param toState The end state of the transition
	 * @param permittedGroups The groups allowed to make the transition
	 */
	public StateTransition(State fromState, State toState, List<Group> permittedGroups)
	{
		this.fromState=fromState;
		this.toState=toState;
		this.permittedGroups=permittedGroups;
		this.isSelfTransition = fromState.equals(toState) ? true : false;
	}
	
	/**
	 * Constructs a Self transition
	 * 
	 * @param selfState The State that has a self transition
	 * @param permittedGroups  The groups allowed to make the transition
	 */
	public StateTransition(State selfState, List<Group> permittedGroups)
	{
		this.fromState=selfState;
		this.toState=selfState;
		this.permittedGroups=permittedGroups;
		this.isSelfTransition = true;
	}

	/**
	 * @return the fromState
	 */
	public State getFromState() {
		return fromState;
	}

	@SuppressWarnings("unchecked")
	private boolean isObjInArrayList(Object o, List al)
	{
		return al.contains(o);
	}
	
	/**
	 * Adds a permitted group to this State Transition
	 * 
	 * @param g the group to be added
	 */
	public void addPermittedGroup(Group g)
	{
		if(!isObjInArrayList(g, permittedGroups))
			permittedGroups.add(g);	
	}	
	
	/**
	 * @return the toState
	 */
	public State getToState() {
		return toState;
	}

	/**
	 * @return the permittedGroups
	 */
	public List<Group> getPermittedGroups() {
		return permittedGroups;
	}

	/**
	 * @param permittedGroups the permittedGroups to set
	 */
	public void setPermittedGroups(List<Group> permittedGroups) {
		this.permittedGroups = permittedGroups;
	}
	
	
	/**
	 * Compare to States against the start and end States of this StateTransition
	 * 
	 * @param start The Start State to compare
	 * @param end The End State to compare
	 * @return true iff the start and end States are equal to the StateTransition's start and end State, false otherwise
	 * 
	 * @see com.mks.services.lib.im.State#equals(Object)
	 */
	public boolean compare(State start, State end)
	{
		if (start.equals(this.getFromState()) && end.equals(this.getToState()))
			return true;
		else
			return false;
	}

	/**
	 * @return true - if this is a self transition (ie. From and To States are the same, false otherwise
	 */
	public boolean isSelfTransition() {
		return isSelfTransition;
	}
	
	/**
	 * A State transition is equal if the To and From States are the same
	 * and if the permitted groups are the same. Note that the order that the  
	 * Permitted groups were added to the StateTransition is not relevant, just that
	 * they contain the same values and meet the <tt>Group.equal(Object)</tt> requirements.
	 * 
	 * @see com.mks.services.lib.im.Group#equals(Object)
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		StateTransition i = (StateTransition)o;
		
		List<Group> ig = i.getPermittedGroups();
		if(ig.size() != this.getPermittedGroups().size())
			return false;		
		
		for(Group g : ig)
		{
			if(!this.getPermittedGroups().contains(g))
				return false;			
		}
		for(Group g : this.getPermittedGroups())
		{
			if(!ig.contains(g))
				return false;			
		}		
		
		return (i.getFromState().equals(this.getFromState())
				&& i.getToState().equals(this.getToState()));
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash*31 + fromState.hashCode();
		hash = hash * 31 + toState.hashCode();
		for(Group g : permittedGroups)
			hash = hash *31 + g.hashCode();
		return hash;
	}
		
	
}
