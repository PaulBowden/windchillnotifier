/**
 * 
 */
package com.mks.services.lib.im;

import java.util.Date;
import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class DateField extends GeneralField  implements IField {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7567877480323375957L;
	public static final Date EMPTYDATE = null;
	private Date minValue;
	private Date maxValue;
	private Date defaultValue;
	private boolean includeTime;	
	
	/**
	 * Constructs a Date Field explicitly setting whether to include time or not.
	 * 
	 * @param name The name of the field
	 * @param isDateTime Sets whether to include time in the field
	 */
	public DateField(String name, boolean isDateTime)
	{
		super(name, FieldType.DATE);
		includeTime = isDateTime;
	}
	
	/**
	 * Constructs a Date Field with the default of no time included.
	 * @param name
	 */
	public DateField(String name)
	{
		super(name, FieldType.DATE);
		includeTime = false;
	}
	/**
	 * @return the minValue
	 */
	public Date getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Date minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public Date getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Date maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the defaultValue
	 */
	public Date getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(Date defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the includeTime
	 */
	public boolean isIncludeTime() {
		return includeTime;
	}

	/**
	 * @param includeTime the includeTime to set
	 */
	public void setIncludeTime(boolean includeTime) {
		this.includeTime = includeTime;
	}

	protected static DateField getField(WorkItem item)
	{
		DateField f = new DateField(item.getField("name").getValueAsString());
		
		
		f.setDisplayname(item.getField("displayName").getString());
		System.out.println("What is this: " + f.getName());
		try{
			f.setMaxValue(item.getField("max").getDateTime());
		}catch(NullPointerException npe){
			f.setMaxValue(DateField.EMPTYDATE);
		}
		try{
			f.setMinValue(item.getField("min").getDateTime());
		}catch(NullPointerException npe){
			f.setMinValue(DateField.EMPTYDATE);
		}
		try{
			f.setDefaultValue(item.getField("default").getDateTime());
		}catch(NullPointerException npe){
			f.setDefaultValue(DateField.EMPTYDATE);
		}		
		f.setComputation(computationParser(item));		
		f.setDescription(item.getField("description").getString());
			return f;
	}
}
