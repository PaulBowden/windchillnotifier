package com.mks.services.lib.im;

final class IBPLValueHolder {
	private Integer ID;
	private String value;
	
	protected IBPLValueHolder(Integer ID, String value)
	{
		this.ID = ID;
		this.value = value;
	}
	
	protected IBPLValueHolder(Integer ID)
	{
		this.ID = ID;
		this.value = null;
	}
	
	protected IBPLValueHolder(String value)
	{
		this.ID = null;
		this.value = value;
	}

	protected String getIDorValue()
	{
		return getID() == null ? getValue() : getID().toString();
	}

	protected String getValueorID()
	{
		return getValue() == null ? getID().toString() : getValue();
	}
	
	/**
	 * @return the iD
	 */
	protected Integer getID() {
		return ID;
	}

	/**
	 * @return the value
	 */
	protected String getValue() {
		return value;
	}
	
	protected boolean IDEquals(Integer i){
		return i.equals(this.getID());	
	}
	protected boolean valueEquals(String v){
		return v.equals(this.getValue());	
	}
}
