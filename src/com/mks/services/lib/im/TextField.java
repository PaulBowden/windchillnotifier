/**
 * 
 */
package com.mks.services.lib.im;

import com.mks.services.lib.CLIOptions;

/**
 * @author cstoss
 *
 */
public abstract class TextField extends GeneralField implements IField {
	private final int DISPLAYROWSDEFAULT = 5;
	private final boolean ISRICHTEXTDEFAULT = false;
	private final boolean ISLOGGINGDEFAULT = false;
	
	public TextField(String name, FieldType fieldtype)
	{
		super(name, fieldtype);		
	}
	/**
	 * Maximum length of the field
	 */
	private int maxLength;
	
	private String defaultString;

	/**
	 * @return the maxLength
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @return the defaultString
	 */
	public String getDefaultString() {
		return defaultString;
	}

	/**
	 * @param defaultString the defaultString to set
	 */
	public void setDefaultString(String defaultString) {
		this.defaultString = defaultString;
	}

	/**
	 * @return the dISPLAYROWSDEFAULT
	 */
	public int getDISPLAYROWSDEFAULT() {
		return DISPLAYROWSDEFAULT;
	}

	/**
	 * @return the iSRICHTEXT
	 */
	public boolean getISRICHTEXTDEFAULT() {
		return ISRICHTEXTDEFAULT;
	}

	/**
	 * @return the iSLOGGING
	 */
	public boolean getISLOGGINGDEFAULT() {
		return ISLOGGINGDEFAULT;
	}
}
