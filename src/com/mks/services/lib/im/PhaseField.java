/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;

import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;
/**
 * @author cstoss
 *
 */
public class PhaseField extends GeneralField implements IField {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4890135764938942067L;
	private List<Phases> phases = new ArrayList<Phases>();

	/**
	 * @param name The name of the Phase Field
	 */
	public PhaseField(String name) {
		super(name, FieldType.PHASE);
		
	}	
	public Phases createPhase(String p)
	{
		Phases ph = new Phases(p);
		phases.add(ph);
		return ph;
	}
	public void addStateToPhase(State s,Phases ph)
	{
		for(int i=0;i<phases.size();i++)
		{
			Phases curPh = phases.get(i);
			if(curPh.getPhaseText().equals(ph.getPhaseText()))
			{
				curPh.addPhaseState(s);
			}
		}
	}

	/**
	 * @return the phases
	 */
	public List<Phases> getPhases() {
		return phases;
	}
}
