/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * UserField represents a User field in MKS.
 * 
 * @author cstoss
 *
 */
public class UserField extends GeneralField implements IField {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2938894473372761935L;
	/**
	 * 
	 */
	private boolean isMultiValued;
	
	/**
	 * Creates a single valued User field
	 * 
	 * @param name name of the field
	 */
	public UserField(String name)
	{
		super(name, FieldType.USER);
		this.isMultiValued = false;
	}
	
	/**
	 * Creates a User field with an explicit setting of being MultiValued or not.
	 * 
	 * @param name name of the field
	 * @param isMultiValued whether the field allows multiple values
	 */
	public UserField(String name, boolean isMultiValued)
	{
		super(name, FieldType.USER);
		this.isMultiValued = isMultiValued;
	}
	
	protected static UserField getField(WorkItem item)
	{
		UserField f = new UserField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDescription(item.getField("description").getString());
		f.setMultiValued((item.getField("isMultiValued").getBoolean()));
		f.setPosition(item.getField("position").getInteger().intValue());

		System.out.println("What is this: " + f.getName());
		return f;
	}

	/**
	 * @return the isMultiValued
	 */
	public boolean isMultiValued() {
		return isMultiValued;
	}

	/**
	 * @param isMultiValued the isMultiValued to set
	 */
	public void setMultiValued(boolean isMultiValued) {
		this.isMultiValued = isMultiValued;
	}
}
