/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mks.api.Command;
import com.mks.api.response.Field;
import com.mks.api.response.Item;
import com.mks.api.response.ItemList;
import com.mks.api.response.Response;
import com.mks.api.response.WorkItem;
import com.mks.services.lib.Connection;
import com.mks.services.lib.IllegalFieldOperationException;
import com.mks.services.lib.MKSLibConnectionException;
import com.nanga.windchillNotifier.Notifier;

/**
 * @author cstoss
 *
 */
public class MKSItem {
    
    private final static Logger log = LoggerFactory.getLogger(MKSItem.class.getName());
	private int ID;
	private String type;
	private HashMap<String, FieldValueHolder> fields;
	private List<String> changedFields;
	private List<String> cannotEditFields = new ArrayList<String>();
	
	
	
	public MKSItem(String type)
	{
		this.type = type;
		fields = new HashMap<String,FieldValueHolder>();
		changedFields = new ArrayList<String>();
		cannotEditFields.add("ID".toLowerCase());
		cannotEditFields.add("Created Date".toLowerCase());
		cannotEditFields.add("Modified Date".toLowerCase());
		cannotEditFields.add("Modified By".toLowerCase());			
	}
	
	/**
	 * @param id the iD to set
	 */
	private void setID(int id) {
		ID = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the ID
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @return the fields
	 */
	public Set<String> getSetFields() {
		return fields.keySet();
	}	
	
	
	
	
	/**
	 * Retrieve the field value as a List of Strings.
	 * In the case of an IBPL field this will return the Values, not the IDs
	 *  
	 * @param fieldname the name of the field you wish to retrieve
	 * @param ibplsAsIDs if true this will return the IBPL value IDs, if false this will return the values of the display string of the IBPLs. If the ID is not available then the value is substituted automatically, if the value is unavailable the ID is substituted automatically
	 * @return the value(s) in one List. or null if the field is not set.
	 * @throws IllegalFieldOperationException if the field doesn't exist
	 */
	public List<String> getFieldValue(String fieldname, boolean ibplsAsIDs) throws IllegalFieldOperationException
	{
		FieldValueHolder fvh = fields.get(fieldname);
		if(fvh == null)
		{
			IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to access a field that does not exist.");
			ifoe.setAffectedFieldName(fieldname);
			ifoe.setIBPL(false);
			throw ifoe;
		}

		List<String> values = fvh.getFieldValue();		
		if(null == values)
		{
			values = new ArrayList<String>();
			List<IBPLValueHolder> ibplvh = fvh.getIBPLValue();
			if(null == ibplvh)
				return null;
			else
			{
				for(IBPLValueHolder v : ibplvh)
				{
					if(ibplsAsIDs)
						values.add(v.getIDorValue());
					else
						values.add(v.getValueorID());
				}					
			}
		}
		return values;	
	}
		
	
	
	/**
	 * Retrieve the field value as a List of Strings. If the field is an IBPL it will return the available display values of the IBPL
	 * 
	 * @param fieldname The name of the field you wish to retrieve
	 *  
	 * @return the value(s) as one String
	 * @throws IllegalFieldOperationException if the field doesn't exist
	 */
	public List<String> getFieldValue(String fieldname) throws IllegalFieldOperationException
	{
		return getFieldValue(fieldname, false);
	}	
	
	
	/**
	 * Retrieve the field value as List of Items. 
	 * 
	 * @param conn
	 * @param relationship
	 */
    public void getRelatedItems(Connection conn, String fieldname)
    {
        FieldValueHolder fvh = fields.get(fieldname);
        if(fvh == null)
        {
            IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to access a field that does not exist.");
            ifoe.setAffectedFieldName(fieldname);
            ifoe.setIBPL(false);
            try {
                throw ifoe;
            } catch (IllegalFieldOperationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        fvh.getFieldValue();
        
        return;  
    }

	
	
	/**
	 * Retrieve the field value as a String. If there are multiple values it delimits them
	 * into one String 
	 * 
	 * @param fieldname The name of the field you wish to retrieve
	 * @param ibplsAsIDs if true this will return the IBPL value IDs, if false this will return the values of the display string of the IBPLs. If the ID is not available then the value is substituted automatically, if the value is unavailable the ID is substituted automatically
	 * @param delim the delimiter to put into the String if there are multivalues 
	 * @return the value(s) as one String
	 * @throws IllegalFieldOperationException if the field doesn't exist
	 */
	public String getFieldValueAsString(String fieldname, boolean ibplsAsIDs, String delim) throws IllegalFieldOperationException
	{		
		List<String> values = getFieldValue(fieldname, ibplsAsIDs);
		if(values == null)
		{
			IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to access a field that does not exist.");
			ifoe.setAffectedFieldName(fieldname);
			ifoe.setIBPL(false);
			throw ifoe;
		}
			
		if(values.size() == 1)
			return values.get(0);
		else
		{
			String s = values.get(0);
			for (int i=1;i<values.size();i++)
				s += delim + values.get(i);
			return s;			
		}
	}
	/**
	 * Retrieve the field value as a String. If there are multiple values it comma delimits them ("[value], [value2], ...")
	 * into one String 
	 * 
	 * @param fieldname The name of the field you wish to retrieve
	 * @param ibplsAsIDs if true this will return the IBPL value IDs, if false this will return the values of the display string of the IBPLs. If the ID is not available then the value is substituted automatically, if the value is unavailable the ID is substituted automatically
	 *  
	 * @return the value(s) as one String
	 * @throws IllegalFieldOperationException if the field doesn't exist
	 */
	public String getFieldValueAsString(String fieldname, boolean ibplsAsIDs)  throws IllegalFieldOperationException
	{
		return getFieldValueAsString(fieldname, ibplsAsIDs, ", ");
	}

	/**
	 * Retrieve the field value as a String. If there are multiple values it comma delimits them ("[value], [value2], ...")
	 * into one String. If the field is an IBPL it will return the available display values of the IBPL
	 * 
	 * @param fieldname The name of the field you wish to retrieve
	 *  
	 * @return the value(s) as one String
	 * @throws IllegalFieldOperationException if the field doesn't exist 
	 */
	public String getFieldValueAsString(String fieldname)  throws IllegalFieldOperationException
	{
		return getFieldValueAsString(fieldname, false, ", ");
	}
	
	/**
	 * This will set the field to the value of the List of Strings <tt>listValue</tt>. This method will not throw
	 * and error if you attempt to set a single value field to multiple values, however you will receive an 
	 * Exception when the update is attempted to be committed. If the List has only one value, this will set the single
	 * value field correctly and no error will occur. 
	 * 
	 * @param fieldname the field to set
	 * @param listValue the value of the field
	 * @throws IllegalFieldOperationException if this is run against a IBPL field.
	 */
	public void setField(String fieldname, List<String> listValue) throws IllegalFieldOperationException
	{	
		FieldValueHolder fvh = null;
		if(fields.containsKey(fieldname))
		{
			fvh = fields.get(fieldname);
			if(fvh.isIBPL())
			{
				IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Field is an IBPL field and cannot be set using this method.");
				ifoe.setAffectedFieldName(fieldname);
				ifoe.setIBPL(fvh.isIBPL());
				throw ifoe;
			}
			else
			{
				if(!fvh.getFieldValue().equals(listValue))
					changedFields.add(fieldname);					
				fvh.setStrListValue(listValue);
			}
		}
		else
		{
			fvh = new FieldValueHolder(listValue);
		}
		fields.put(fieldname, fvh);	
	}
	
	/**
	 * This is used to see if this is a field that the System provides.
	 * 
	 * @param fieldname
	 * @return if the field is a system field
	 */
	public boolean isSystemField(String fieldname) {
		return cannotEditFields.contains(fieldname.toLowerCase());
	}

	/**
	 * This will set the field to a single String value. This can set any field. If <tt>fieldname</tt> is 
	 * a multivalued field, it will set to a selection of one item.
	 * 
	 * @param fieldname the field to set
	 * @param stringValue the value of the field
	 * @throws IllegalFieldOperationException if this is run against a IBPL field.
	 */
	public void setField(String fieldname, String stringValue) throws IllegalFieldOperationException
	{
		List<String> l =  new ArrayList<String>();
		l.add(stringValue);
		setField(fieldname,l);
	}
	
	/**
	 * Sets a multivalued IBPL field values. This <i>will</i> work against a single valued field, but will 
	 * throw a runtime error if an edit is attempted to be committed and a single valued field has multiple 
	 * values assigned to it.<br><b>NOTE</b>:This can only used on IBPL Fields.
	 * 
	 * @param IBPLFieldName the IBPL field to set
	 * @param listValues the value to set the single values IBPL to
	 * @param isID is the value an ID or a String value
	 * @throws IllegalFieldOperationException if <tt>listValues</tt> contains non-Integers and <tt>isID</tt> is true or if this is run against a non-IBPL field.
	 */
	public void setIBPLField(String IBPLFieldName, List<String> listValues, boolean isID) throws IllegalFieldOperationException
	{
		List<IBPLValueHolder> newValues = new ArrayList<IBPLValueHolder>();
		FieldValueHolder fvh = null;
		if(fields.containsKey(IBPLFieldName))
		{
			fvh = fields.get(IBPLFieldName);
			if(fvh.isIBPL())
			{
				for(String s : listValues)
				{
					boolean foundIt = false;
					if(isID)
					{
						Integer i = null;
						try{
							 i = new Integer(s);
						}catch(NumberFormatException nfe){
							IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to set a non Integer \""+s+"\" to an ID of an IBPL.");
							ifoe.setAffectedFieldName(IBPLFieldName);
							ifoe.setIBPL(fvh.isIBPL());
							throw ifoe;
						}
						List<IBPLValueHolder> vals = fvh.getIBPLValue();
						for(IBPLValueHolder val :  vals)
						{
							if(val.IDEquals(i)){
								newValues.add(val);
								foundIt = true;
								break;
							}
						}
						if(!foundIt)
							newValues.add(new IBPLValueHolder(i));
					}
					else
					{
						List<IBPLValueHolder> vals = fvh.getIBPLValue();
						for(IBPLValueHolder val :  vals)
						{
							if(val.valueEquals(s)){
								newValues.add(val);
								foundIt = true;
								break;
							}
						}
						if(!foundIt)
							newValues.add(new IBPLValueHolder(s));							
					}
				}
				if(!fvh.getFieldValue().equals(newValues))
					changedFields.add(IBPLFieldName);
				fvh.setIBPLValues(newValues);
			}
			else
			{
				IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to set a non IBPL field to IBPL values.");
				ifoe.setAffectedFieldName(IBPLFieldName);
				ifoe.setIBPL(fvh.isIBPL());
				throw ifoe;
			}
		}
		else
		{
			for(String s : listValues)
				newValues.add(new IBPLValueHolder(s));
				
			fvh = new FieldValueHolder(newValues, true);
		}
		fields.put(IBPLFieldName, fvh);			
	}
	
	/**
	 * Sets the value of an IBPL field. If the IBPL field is a mutli-valued field it will set it to a single selection. <br><b>NOTE</b>: This can only be used on IBPL Fields.<br>
	 * 
	 * @param IBPLFieldName The IBPL field to set
	 * @param stringValue the value to set the single values IBPL to
	 * @param isID is the value an ID or a String value
	 * @throws IllegalFieldOperationException if <tt>stringValue</tt> contains non-Integers and <tt>isID</tt> is true or if this is run against a non-IBPL field.
	 */
	public void setIBPLField(String IBPLFieldName, String stringValue, boolean isID) throws IllegalFieldOperationException
	{
		List<String> l = new ArrayList<String>();
		l.add(stringValue);
		setIBPLField(IBPLFieldName, l, isID);
	}
	
	private void setIBPLFieldValueHolder(String fieldname, List<IBPLValueHolder> ibplValueHolder)
	{
		FieldValueHolder fvh = null;
		if(fields.containsKey(fieldname))
		{
			fvh = fields.get(fieldname);
			fvh.setIBPLValues(ibplValueHolder);
		}
		else
		{
			fvh = new FieldValueHolder(ibplValueHolder, true);
		}
		fields.put(fieldname, fvh);		
	}
		
	
	/**
	 * Returns all the fields that have changed values since they were originally set.
	 * Note that if you change a field to a different value, then back to the original value it
	 * is counted as changed.
	 * 
	 * @return the list of changed fields.
	 */
	public List<String> getChangedFields()
	{
		return changedFields;
	}	
	
	private static WorkItem getItemWorkItem(int ID, Connection connection)
	{
		Command viewItem = new Command(Command.IM, "viewissue");
		viewItem.addSelection(""+ID);
		
		Response issueRes = null;
		try {
			issueRes = connection.runCommand(viewItem);
		} catch (MKSLibConnectionException e) {
			issueRes = null;
		}
		if(null != issueRes)
		{
			return issueRes.getWorkItem(""+ID);
		}
		else
			return null;
	}
	
	/**
	 * Retrieve only specific fields for an item. This is more performant in parsing all fields.
	 * 
	 * @param ID ID of the item
	 * @param connection connection to use
	 * @param fields fields to retrieve
	 * @return an MKSItem with only the fields you specified in it. Setting or getting other fields is an undefined action.
	 */
	public static MKSItem getSpecificFields(int ID, Connection connection, List<String> fields) throws IllegalFieldOperationException
	{
		return getItem(ID, connection, fields);
	}
	/**
	 * Retrieve only specific field for an item. This is more performant in parsing all fields.
	 * 
	 * @param ID ID of the item
	 * @param connection connection to use
	 * @param field field to retrieve
	 * @return an MKSItem with only the field you specified in it. Setting or getting other fields is an undefined action.
	 */
	public static MKSItem getSpecificField(int ID, Connection connection, String field) throws IllegalFieldOperationException
	{
		List<String> l = new ArrayList<String>();
		l.add(field);
		return getItem(ID, connection, l);
	}
	
	/**
	 * Retrieves a complete item with all field values populated.
	 *  
	 * @param ID ID of the item
	 * @param connection connection to use
	 * @return an MKSItem with all field values in it.
	 */
	public static MKSItem getItem(int ID, Connection connection) throws IllegalFieldOperationException
	{
		return getItem(ID, connection, null);
	}

	@SuppressWarnings("unchecked")
	private static MKSItem getItem(int ID, Connection connection, List<String> fields) throws IllegalFieldOperationException
	{
		MKSItem item = null;
		WorkItem issueWI = getItemWorkItem(ID, connection);
				
		if(null != issueWI)
		{
			item = new MKSItem(issueWI.getField("Type").getValueAsString());
			item.setID(ID);
			Iterator iterator = issueWI.getFields();
			
			if(null == fields)
			{
				while(iterator.hasNext())
				{
					Field f = (Field)iterator.next();
					//System.out.println(f.getValue());
					item = getFieldData(item, f);
				}				
			}
			else
			{
				for(String s : fields)
				{
					item = getFieldData(item, issueWI.getField(s));
				}
			}		
		}
		return item;		
	}

	@SuppressWarnings("unchecked")
	private static MKSItem getFieldData(MKSItem item, Field f) throws IllegalFieldOperationException
	{
		if(null != f.getDataType())
		{
			if(f.getDataType().equals(Field.VALUE_LIST_TYPE) || f.getDataType().equals(Field.ITEM_LIST_TYPE)) {
				item.setField(f.getName(), f.getList());
			}
			else if(f.getDataType().equals(Field.ITEM_TYPE) && "im.IBPL".equals(f.getItem().getModelType())) {								
				item.setIBPLFieldValueHolder(f.getName(), parseIBPLValuesFromItem(f.getItem()));
			}
			else if(f.getDataType().equals(Field.ITEM_LIST_TYPE) && "im.Relationship".equals(f.getItem().getModelType())) {
			    System.out.println("coffeeeffee");
			    assert(false) ; // work in progress
		    }
			else
				item.setField(f.getName(), f.getValueAsString());
		}
		else
		{
			item.setField(f.getName(), f.getValueAsString());
		}
		return item;
	}
	
	@SuppressWarnings("unchecked")
	private static List<IBPLValueHolder> parseIBPLValuesFromItem(Item valueItem)
	{
		List<IBPLValueHolder> allIBPLValues = new ArrayList<IBPLValueHolder>();

		List<String> iVal = null;
		List<String> iID = null;
				
		// This is to handle the case where there is only one entry.
		// This is expected behaviour, however there is an RFC 97862 that WN is attached to
		try {
			iVal = valueItem.getField("IssueIDList").getList();
		} catch (NoSuchElementException e) {
			iVal = new ArrayList<String>();
			iID = new ArrayList<String>();
			iVal.add(""+valueItem.getId());
			iID.add(valueItem.getField("IssueID").getItem().getId());
		}				
		if(iVal.size() == iID.size())
		{			
			for (int k=0; k< iVal.size();k++)
				allIBPLValues.add(new IBPLValueHolder(Integer.valueOf(iID.get(k)), iVal.get(k)));
		}
		return allIBPLValues;
	}

    @SuppressWarnings("unchecked")
    public List<String> getRelatedIDs(String fieldname) {
        
    
        List<String> relIDs = new ArrayList<String>();
        // values = getFieldValue(fieldname, false);
            FieldValueHolder fvh = fields.get(fieldname);
            
            
            //assert(fvh!=null);
            if(fvh==null)
            {
                IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to access a field that does not exist.");
                ifoe.setAffectedFieldName(fieldname);
                ifoe.setIBPL(false);
                try {
                    throw ifoe;
                } catch (IllegalFieldOperationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            ItemList itemList = (ItemList) fvh.getFieldValue();
            
            Iterator<Item> item  = itemList.iterator();
            
            while(item.hasNext()) {
                Item i = item.next();
               //log.debug("list size: "+ i.getFieldListSize());
               log.debug("id "+ i.getId());
               relIDs.add(i.getId());
           }
            
         
        if(relIDs.isEmpty())
        {
            IllegalFieldOperationException ifoe = new IllegalFieldOperationException("Attempt to access a field that does not exist.");
            ifoe.setAffectedFieldName(fieldname);
            ifoe.setIBPL(false);
            try {
                throw ifoe;
            } catch (IllegalFieldOperationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        
        log.debug("vals " + relIDs);
        
        return relIDs;
    }
   	
}
