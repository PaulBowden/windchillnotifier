/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class PickItem implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5147521005626700042L;
	private int pickValue;
	private String pickLabel;
	private boolean active;
	
	/**
	 * The constructor for setting the Label only. The label will automatically be set to active.
	 * @param pickLabel
	 */
	public PickItem(String pickLabel)
	{
		this.pickLabel = pickLabel;
		active = true;
	}
	/**
	 * Constructor used to set the label and the value. The label will automatically be set to active.
	 * @param pickLabel
	 * @param pickValue
	 */
	public PickItem(String pickLabel, int pickValue)
	{
		this.pickLabel = pickLabel;
		this.pickValue = pickValue;
		active = true;
	}
	/**
	 * 
	 * Constructs a label with an explicit value and whether it is active or not.
	 * @param pickLabel
	 * @param pickValue
	 * @param active
	 */
	public PickItem(String pickLabel,  int pickValue,  boolean active)
	{
		this.pickLabel = pickLabel;
		this.pickValue = pickValue;
		this.active = active;
	}	
	/**
	 * Retrieve the Pick Value
	 * @return the pickValue
	 */
	public int getPickValue() {
		return pickValue;
	}
	/**
	 * Set the Pick Value
	 * @param pickValue the pickValue to set
	 */
	public void setPickValue(int pickValue) {
		this.pickValue = pickValue;
	}
	/**
	 * Retrieve the Pick Label
	 * @return the pickLabel
	 */
	public String getPickLabel() {
		return pickLabel;
	}
	/**
	 * Set the Pick Label
	 * @param pickLabel the pickLabel to set
	 */
	public void setPickLabel(String pickLabel) {
		this.pickLabel = pickLabel;
	}
	/**
	 * Returns if the Pick Item is active
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * Sets the Pick Item to active or not
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	

	/**
	 * Checks all attributes of the PickItem for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		PickItem i = (PickItem)o;
				
		
		return ( i.getPickLabel().equals(this.getPickLabel())
				&& i.getPickValue() == this.getPickValue()
				&& i.isActive() == this.isActive());					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getPickValue();
		hash = hash * 31 + this.getPickLabel().hashCode();
		hash = hash * 31 + (this.isActive() ? 1 : 0);
		return hash;
	}


}
