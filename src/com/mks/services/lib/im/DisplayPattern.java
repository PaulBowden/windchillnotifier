/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class DisplayPattern  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3315121647264288187L;
	public static final String INTWITHCOMMA ="#,###";
	public static final String FLOATWITHCOMMA = INTWITHCOMMA+".00";
	public static final String INTDOLLAR = "$"+INTWITHCOMMA;
	public static final String FLOATDOLLAR = "$"+FLOATWITHCOMMA;	
	
	private String pattern;

	/**
	 * @param pattern
	 */
	public DisplayPattern(String pattern) {
 		this.pattern = pattern;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	public String toString()
	{
		return getPattern();
	}
}