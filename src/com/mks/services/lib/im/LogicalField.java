/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class LogicalField extends GeneralField implements IField {

	public static final Boolean BOOLEAN_TRUE = new Boolean(true);
	public static final Boolean BOOLEAN_FALSE = new Boolean(false);
	public static final Boolean BOOLEAN_NONE = null;
	
	private Boolean defaultValue;

	/**
	 * 
	 */
	private static final long serialVersionUID = -641531507983906141L;
	
	/**
	 * Define a logical field without a default value
	 * 
	 * @param name the logical field name
	 */
	public LogicalField(String name)
	{
		super(name, FieldType.LOGICAL);
		this.defaultValue = BOOLEAN_NONE;
	}
	
	/**
	 * Define a logical field with a default value.
	 * 
	 * @param name The logical field name
	 * @param defaultValue the default value for the logical field
	 */
	public LogicalField(String name, boolean defaultValue)
	{
		super(name, FieldType.LOGICAL);
		this.defaultValue = defaultValue ? BOOLEAN_TRUE : BOOLEAN_FALSE;
	}
	
	public void setDefaultValue(Boolean defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	
	public Boolean getDefaultValue()
	{
		return this.defaultValue;
	}
	
	
	protected static LogicalField getField(WorkItem item)
	{
		LogicalField f = new LogicalField(item.getField("name").getValueAsString());

		f.setDisplayname(item.getField("displayName").getString());
		
		System.out.println("What is this: " + f.getName());
		try{
			f.setDefaultValue(item.getField("default").getBoolean());
		}catch(NullPointerException npe){
			f.setDefaultValue(LogicalField.BOOLEAN_NONE);
		}
		f.setComputation(computationParser(item));
		f.setDescription(item.getField("Description").getString());
			return f;
	}
	
	/**
	 * Checks all attributes of the LogicalField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		LogicalField i = (LogicalField)o;
		
		String thisComp = "";
		String thatComp = "";
		/*if(this.isComputed())
			thisComp = this.getComputation();
		if(i.isComputed())
			thatComp = i.getComputation();
		*/
		
		return ( i.getName().equals(this.getName())
					&& i.getDescription().equals(this.getDescription())
					&& i.getDisplayname().equals(this.getDisplayname())
					//&& i.isComputed() == this.isComputed()
					&& thisComp.equals(thatComp));					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + this.getDisplayname().hashCode();
		/*	hash = hash * 31 + (this.isComputed() ? 1 : 0);
		if(this.isComputed())
			hash = hash *31 + this.getComputation().hashCode();
		else
			hash = hash *31 + "".hashCode();*/
		return hash;
	}
}
