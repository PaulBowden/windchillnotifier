/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class AttachmentField extends GeneralField implements IField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4907765612175382463L;
	public static final String CSVSTYLE = "csv";
	public static final String TABLESTYLE = "table";
	private static final int DEFAULTROWNUMS = 5;
	private static final String DEFAULTSTYLE = TABLESTYLE;
	
	private String displayStyle;
	private int displayRows;
	
	public AttachmentField(String name)
	{
		super(name, FieldType.ATTACHMENT);
		setDisplayRows(DEFAULTROWNUMS);
		setDisplayStyle(DEFAULTSTYLE);
		
	}
	public AttachmentField(String name, int displayRows)
	{
		super(name, FieldType.ATTACHMENT);
		this.displayRows = displayRows;
		setDisplayStyle(DEFAULTSTYLE);
	}
	public AttachmentField(String name, int displayRows, String displayStyle)
	{
		super(name, FieldType.ATTACHMENT);
		this.displayRows = displayRows;
		this.displayStyle = displayStyle;
	}	
	
	/**
	 * @return the displayStyle
	 */
	public String getDisplayStyle() {
		return displayStyle;
	}
	/**
	 * @param displayStyle the displayStyle to set
	 */
	public void setDisplayStyle(String displayStyle) {
		this.displayStyle = displayStyle;
	}
	/**
	 * @return the displayRows
	 */
	public int getDisplayRows() {
		return displayRows;
	}
	/**
	 * @param displayRows the displayRows to set
	 */
	public void setDisplayRows(int displayRows) {
		this.displayRows = displayRows;
	}
	protected static AttachmentField getField(WorkItem item)
	{
		AttachmentField f = new AttachmentField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDescription(item.getField("description").getString());
	//	f.setDescription(item.getField("displayStyle").getString());
		f.setDisplayStyle(DEFAULTSTYLE);
		//f.setDisplayRows(item.getField("displayRows").getInteger().intValue());
		f.setDisplayRows(DEFAULTROWNUMS);
		
		
			System.out.println("What is this: " + f.getName());
		return f;
	}
}
