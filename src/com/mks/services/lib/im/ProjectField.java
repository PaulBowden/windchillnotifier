package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

public class ProjectField extends GeneralField implements IField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3073354753737137418L;
	public ProjectField(String name)
	{
		super(name, FieldType.PROJECT);		
	}

	protected static ProjectField getField(WorkItem item)
	{
		ProjectField f = new ProjectField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		//f.setDefaultValue(item.getField("Default").getValueAsString());
		f.setDescription(item.getField("description").getString());
		f.setPosition(item.getField("position").getInteger().intValue());

		System.out.println("What is this: " + f.getName());
		return f;
	}
}
