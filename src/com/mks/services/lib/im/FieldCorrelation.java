/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class FieldCorrelation {
	private GeneralField sourceField;
	private GeneralField targetField;
	/**
	 * @param sourceField
	 * @param targetField
	 */
	public FieldCorrelation(GeneralField sourceField, GeneralField targetField) {
		this.sourceField = sourceField;
		this.targetField = targetField;
	}
	/**
	 * @return the sourceField
	 */
	public GeneralField getSourceField() {
		return sourceField;
	}
	/**
	 * @param sourceField the sourceField to set
	 */
	public void setSourceField(GeneralField sourceField) {
		this.sourceField = sourceField;
	}
	/**
	 * @return the targetField
	 */
	public GeneralField getTargetField() {
		return targetField;
	}
	/**
	 * @param targetField the targetField to set
	 */
	public void setTargetField(GeneralField targetField) {
		this.targetField = targetField;
	}
	
	

}
