package com.mks.services.lib.im;

import java.io.Serializable;

/**
 * An enum representing the combinations allowed for setting the 
 * frequency and storage parameters of a computed field
 * 
 * @author cstoss
 *
 */
public enum ComputedFrequency implements Serializable{
	NEVER ("never"),
	DAILY ("daily"),
	WEEKLY ("weekly"),
	MONTHLY ("monthly"),
	DELTA ("delta"),
	DYNAMIC ("never", "Dynamic");
	
	private String frequency;
	private String howToRun;
	ComputedFrequency(String frequency){
		this.frequency = frequency;
		this.howToRun = "Static";
	}
	ComputedFrequency(String frequency, String howToRun){
		this.frequency = frequency;
		this.howToRun = howToRun;
	}
	/**
	 * @return the frequency
	 */
	public String getFrequency() {
		return frequency;
	}
	/**
	 * @return the howToRun
	 */
	public String getHowToRun() {
		return howToRun;
	}
	public String toString(){
		return getFrequency()+":"+getHowToRun();	
	}
}
