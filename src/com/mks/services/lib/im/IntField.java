package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.IllegalValueDetectedException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * 
 * @author cstoss
 *
 */
public class IntField extends GeneralField implements IField  {
	public static final Integer EMPTYINT = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2216052737382999932L;
	private Object minValue;
	private Object maxValue;
	private Object defaultValue;
	private boolean displayProgressBar;
	private DisplayPattern displayPattern;
			
	public IntField(String name)
	{
		super(name, FieldType.INTEGER);		
	}
	

	/**
	 * @param minValue
	 * @param maxValue
	 * @param defaultValue
	 * @param displayProgressBar
	 */
	public IntField(String name, Object minValue, Object maxValue, 
			Object defaultValue,
			boolean displayProgressBar) {
		super(name,FieldType.INTEGER);

		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
		this.displayProgressBar = displayProgressBar;
	}




	/**
	 * @return the displayPattern
	 */
	public DisplayPattern getDisplayPattern() {
		return displayPattern;
	}

	/**
	 * @param displayPattern the displayPattern to set
	 */
	public void setDisplayPattern(DisplayPattern displayPattern) {
		this.displayPattern = displayPattern;
	}
	
	private boolean testValue(Object o){
		Integer testValues = null;
		try{
			testValues = (Integer)o;
		}catch(ClassCastException cce){
			try{
				testValues = Integer.valueOf(o.toString());
			}catch(NumberFormatException nfe){
				return false;
			}
		}
		return true;
	}
	/**
	 * @return the minValue
	 */
	public Object getMinValue() {		
		return minValue;
	}

	/**
	 * This will set the minimum value to the Integer Object specified or the Integer value of a String as defined by @link {@link java.lang.Integer#parseInt(String)}
	 * @param minValue the minValue to set
	 * @throws IllegalValueDetectedException if the value is illegal for the type of field
	 */
	public void setMinValue(Object minValue) {
		if(testValue(minValue))
			this.minValue = minValue;
		else
			throw new IllegalValueDetectedException(minValue.toString(), this.getFieldTypeAsStr());

	}

	/**
	 * @return the maxValue
	 */
	public Object getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Object maxValue) {
		if(testValue(maxValue))
			this.maxValue = maxValue;
		else
			throw new IllegalValueDetectedException(maxValue.toString(), this.getFieldTypeAsStr());
	}

	/**
	 * @return the defaultValue
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(Object defaultValue) {
		if(testValue(defaultValue))
			this.defaultValue = defaultValue;
		else
			throw new IllegalValueDetectedException(defaultValue.toString(), this.getFieldTypeAsStr());
	}

	/**
	 * @return the displayProgressBar
	 */
	public boolean isDisplayProgressBar() {
		return displayProgressBar;
	}

	/**
	 * @param displayProgressBar the displayProgressBar to set
	 */
	public void setDisplayProgressBar(boolean displayProgressBar) {
		this.displayProgressBar = displayProgressBar;
	}
	
	protected static IntField getField(WorkItem item)
	{
		IntField f = new IntField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		try{
			f.setDefaultValue(item.getField("default").getInteger().intValue());
		}catch(NullPointerException npe){
			f.setDefaultValue(IntField.EMPTYINT);
		}
		
		f.setDescription(item.getField("Description").getString());
		f.setDisplayProgressBar(item.getField("displayAsProgress").getBoolean().booleanValue());
		
		try{
			f.setMaxValue(item.getField("max").getInteger().intValue());
		}catch(NullPointerException npe){
			f.setMaxValue(IntField.EMPTYINT);
		}
		try{
			f.setMinValue(item.getField("min").getInteger().intValue());
		}catch(NullPointerException npe){
			f.setMinValue(IntField.EMPTYINT);
		}
		//f.setMaxLength(item.getField("MaxLength").getInteger().intValue());
		f.setPosition(item.getField("Position").getInteger().intValue());
		f.setComputation(computationParser(item));
		
		System.out.println("What is this: " + f.getName());
		return f;
	}

	/**
	 * Checks all attributes of the IntField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		IntField i = (IntField)o;
		
		Computation thisComp = null;
		Computation thatComp = null;
		if(this.isComputed())
			thisComp = this.getComputation();
		if(i.isComputed())
			thatComp = i.getComputation();

		
		return ( i.getName().equals(this.getName())
					&& i.getDescription().equals(this.getDescription())
					&& i.getDisplayname().equals(this.getDisplayname())
					&& i.getMinValue() == this.minValue
					&& i.getMaxValue() == this.maxValue 
					&& i.isComputed() == this.isComputed()
					&& i.isDisplayProgressBar() == this.isDisplayProgressBar()
					&& thisComp.equals(thatComp));					
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + (null == maxValue ? 0 : ((Integer)maxValue).intValue());
		hash = hash * 31 + (null == minValue ? 0 : ((Integer)minValue).intValue());
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + (this.isComputed() ? 1 : 0);
		hash = hash * 31 + (this.isDisplayProgressBar() ? 1 : 0);
		if(this.isComputed())
			hash = hash *31 + this.getComputation().hashCode();
		else
			hash = hash *31 + "".hashCode();
		return hash;
	}	
}
