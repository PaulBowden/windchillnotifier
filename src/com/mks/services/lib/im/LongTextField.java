/**
 * 
 */
package com.mks.services.lib.im;

import java.util.Enumeration;
import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * 
 * This Class is the representation of a Long Text Field.
 * @author cstoss
 *
 */
public class LongTextField extends TextField implements IField {


	private static final long serialVersionUID = -198849056764582223L;
	
	/**
	 * Used when this field is to have no logging
	 */
	public static final String LOGGING_NONE= "none";
	
	/**
	 * Used to log the field most recent entries first
	 */
	public static final String LOGGING_MOSTRECENTFIRST = "mostRecentFirst";
	
	/**
	 * Used to log the field most recent last 
	 */
	public static final String LOGGING_MOSTRECENTLAST = "mostRecentLast";
	
	
	private String loggingText = LongTextField.LOGGING_NONE;
	private boolean richText = getISRICHTEXTDEFAULT();
	private int displayRows = getDISPLAYROWSDEFAULT();
	private String attachmentField;

	public LongTextField(String name)
	{
		super(name, FieldType.LONGTEXT);
	}
	
	/**
	 * Is this a logging field?
	 * 
	 * @return Returns the loggingText.
	 * @see #LOGGING_NONE
	 * @see #LOGGING_MOSTRECENTLAST
	 * @see #LOGGING_MOSTRECENTFIRST
	 */
	public String getLoggingText() {
		return loggingText;
	}
	
	/**
	 * Set the logging text value. If the String is invalid the default will be used.
	 * If the logging text is set to <tt>LOGGING_MOSTRECENTFIRST</tt> or <tt>LOGGING_MOSTRECENTLAST</tt>
	 * the field cannot be richtext, this will automatically be enforced.
	 * 
	 * @param loggingText The direction of the logging text.
	 * 
	 * @see #LOGGING_NONE
	 * @see #LOGGING_MOSTRECENTLAST
	 * @see #LOGGING_MOSTRECENTFIRST
	 */
	public void setLoggingText(String loggingText) {
		if(loggingText.equals(LOGGING_MOSTRECENTLAST)
				|| loggingText.equals(LOGGING_MOSTRECENTFIRST))
		{
			this.setRichText(false);
			this.loggingText = loggingText;
		}
		else
			this.loggingText = LOGGING_NONE;
	}
		
	/**
	 * Is the field richtext?
	 * 
	 * @return the richText
	 */
	public boolean isRichText() {
		return richText;
	}
	/**
	 * The sets if the field is a rich text field. If this is set to true, then logging text cannot
	 * be used. This will be automatically enforced.
	 * 
	 * @param richText the richText to set
	 */
	public void setRichText(boolean richText) {
		this.richText = richText;
		if(richText)
			this.setLoggingText(LOGGING_NONE);
	}
	/**
	 * Get the number of rows to display.
	 * 
	 * @return the displayRows
	 */
	public int getDisplayRows() {
		return displayRows;
	}
	/**
	 * Set the number of display rows.
	 * 
	 * @param displayRows the displayRows to set
	 */
	public void setDisplayRows(int displayRows) {
		this.displayRows = displayRows;
	}
	/**
	 * 
	 * @return the attachmentField
	 */
	public String getAttachmentField() {
		return attachmentField;
	}
	/**
	 * @param attachmentField the attachmentField to set
	 */
	public void setAttachmentField(String attachmentField) {
		this.attachmentField = attachmentField;
	}

	/**
	 * This method will generate a Command Line for the Type
	 * 
	 * @param create - true if you want it to be a 'im createtype', false if you want 'im edittype'
	 * 
	 * @return The IM CLI String for the command.
	 */
	@SuppressWarnings("unchecked")
	public String getCLICommand(boolean create)
	{
		String retStr = "im ";
		if(create)
			retStr += "createfield ";
		else
		{
			retStr += "editfield ";
			return "Edit field not supported at this time";
		}
		
		CLIOptions args = getOptions();
		
		for (Enumeration<String> opts = (Enumeration<String>)args.propertyNames() ; opts.hasMoreElements() ;) {
		         retStr += args.getOption(opts.nextElement());
		}		
		if(!create)
			retStr += this.getName();
		
		return retStr;
	}
	
	protected CLIOptions getOptions() {
		//checkForConflicts();
		CLIOptions cli = new CLIOptions();
		cli.setProperty("--type", this.getFieldTypeAsStr());
		cli.setProperty("--name", getName());
		cli.setProperty("--description", this.getDescription());
		cli.setProperty("--position", this.getPosition());
		cli.setProperty("--loggingText", ""+getLoggingText());
		cli.setProperty("--isRichtext", ""+isRichText());
		if(isRichText())
			cli.setProperty("--defaultAttachmentField", this.getAttachmentField());
		cli.setProperty("--displayRows", ""+this.getDisplayRows());
		cli.setProperty("--maxLength", ""+this.getMaxLength());
		return cli;
	}
	/*
	private void checkForConflicts()
	{
		if(isLoggingText() && isRichText())
			return;
		else if(isRichText() && null == getAttachmentField())
			return;
		
	}*/
	protected static LongTextField getField(WorkItem item)
	{
		LongTextField f = new LongTextField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDescription(item.getField("description").getString());
		f.setMaxLength(item.getField("maxLength").getInteger().intValue());
		f.setPosition(item.getField("position").getInteger().intValue());		
		f.setDisplayRows(item.getField("displayRows").getInteger().intValue());
		
		if(item.getField("loggingText").getBoolean().booleanValue())
			f.setLoggingText(LOGGING_MOSTRECENTFIRST);
		else
			f.setLoggingText(LOGGING_NONE);

		boolean rt = item.getField("richContent").getBoolean().booleanValue();
		f.setRichText(rt);
		
		if(rt)
			f.setAttachmentField(item.getField("defaultAttachmentField").getString());		

		System.out.println("What is this: " + f.getName());
		return f;
	}
	

	/**
	 * Checks all attributes of the LongTextField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		LongTextField i = (LongTextField)o;
				
		return ( i.getName().equals(this.getName())
					&& i.getDescription().equals(this.getDescription())
					&& i.getDisplayname().equals(this.getDisplayname())
					&& i.isRichText() == this.isRichText()
					&& i.getDisplayRows() == this.getDisplayRows()
					&& i.getMaxLength() == this.getMaxLength()
					&& i.getLoggingText() == this.getLoggingText());					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getMaxLength();
		hash = hash * 31 + this.getDisplayRows();
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getLoggingText().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + this.getDisplayname().hashCode();
		hash = hash * 31 + (this.isRichText() ? 1 : 0);
		
		return hash;
	}
}
