/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.IllegalValueDetectedException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 *
 */
public class ShortTextField extends TextField  implements java.io.Serializable,IField {
	private Object defaultValue;
	/**
	 * 
	 */
	private static final long serialVersionUID = 730392082604368168L;

	public ShortTextField(String name)
	{
		super(name, FieldType.SHORTTEXT);		
	}
	protected static ShortTextField getField(WorkItem item)
	{
		ShortTextField f = new ShortTextField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDefaultValue(item.getField("default").getValueAsString());
		f.setDescription(item.getField("description").getString());
		f.setMaxLength(item.getField("maxLength").getInteger().intValue());
		f.setPosition(item.getField("position").getInteger().intValue());
		f.setComputation(computationParser(item));
		System.out.println("What is this: " + f.getName());
		return f;
	}
	/**
	 * Checks all attributes of the ShortTextField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		ShortTextField i = (ShortTextField)o;
		
		Computation thisComp = null;
		Computation thatComp = null;
		if(this.isComputed())
			thisComp = this.getComputation();
		if(i.isComputed())
			thatComp = i.getComputation();
		
		
		return ( i.getName().equals(this.getName())
					&& i.getDescription().equals(this.getDescription())
					&& i.getDisplayname().equals(this.getDisplayname())
					&& i.getMaxLength() == this.getMaxLength()
					&& i.isComputed() == this.isComputed()
					&& thisComp.equals(thatComp));					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getMaxLength();
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + this.getDisplayname().hashCode();
		hash = hash * 31 + (this.isComputed() ? 1 : 0);
		if(this.isComputed())
			hash = hash *31 + this.getComputation().hashCode();
		else
			hash = hash *31 + "".hashCode();
		return hash;
	}
	
	private boolean testValue(Object o){
		String testValues = null;
		try{
			testValues = (String)o;
		}catch(ClassCastException cce){
			testValues = o.toString();
		}
		return true;
	}
	public void setDefaultValue(Object defaultValue) {
		if(testValue(defaultValue))
			this.defaultValue = defaultValue;
		else
			throw new IllegalValueDetectedException(defaultValue.toString(), this.getFieldTypeAsStr());

	}
	public Object getDefaultValue(){
		return defaultValue;
	}
}
