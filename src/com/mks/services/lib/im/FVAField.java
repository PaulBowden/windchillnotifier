/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class FVAField extends GeneralField implements IField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6281001981461584976L;
	private String relationship;
	private String fieldname;
	private String backedby;
	
	public FVAField(String name)
	{
		super(name, FieldType.FVA);
	}
	
	public FVAField(String name, String relationship, String fieldname)
	{
		super(name, FieldType.FVA);
		this.relationship = relationship;
		this.fieldname = fieldname;
		createBackedBy(relationship,fieldname);
	}
	
	private void createBackedBy(String r, String f)
	{
		backedby = r+ "." + f;
	}

	/**
	 * @return the backedby
	 */
	private String getBackedby() {
		return backedby;
	}

	/**
	 * @return the relationship
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * @param relationship the relationship to set
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	/**
	 * @return the fieldname
	 */
	public String getFieldname() {
		return fieldname;
	}

	/**
	 * @param fieldname the fieldname to set
	 */
	public void setFieldname(String fieldname) {
		this.fieldname = fieldname;
	}

	private boolean splitBackedBy(String s)
	{
		String[] sa = s.split(".");
		if(sa.length==2)
		{
			relationship = sa[0];
			fieldname = sa[1];
			return true;
		}
		else
			return false;
	}
	
	protected static FVAField getField(WorkItem item)
	{
		FVAField f = new FVAField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDescription(item.getField("description").getString());
		f.setBackedby(item.getField("backedBy").getString());
		
		
			System.out.println("What is this: " + f.getName());
		return f;
	}

	/**
	 * @param backedby the backedby to set
	 */
	public void setBackedby(String backedby) {
		splitBackedBy(backedby);
		this.backedby = backedby;
		
	}
}
