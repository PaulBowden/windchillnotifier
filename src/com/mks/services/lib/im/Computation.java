package com.mks.services.lib.im;

import java.io.Serializable;

/**
 * Used to organize the dataa for a Computed field.
 * The idea is to eventually put in an AI to verify computations before allowing them to be set.
 * Having an object let's this happen quite easily while providing the functionality needed before that
 * is completed.
 * 
 * @author cstoss
 *
 */
public class Computation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3581095185017121230L;
	private String computationString;
	private ComputedFrequency frequency;
	
	/**
	 * Create a dynamic computation
	 * 	
	 * @param computation the computation string
	 */
	public Computation (String computation){
		setComputationString(computation.trim());	
		this.frequency = ComputedFrequency.DYNAMIC;
	}	
	
	/**
	 * Create a computation
	 * 
	 * @param computation the computation string
	 * @param frequency the frequency and storage type of the computation
	 */
	public Computation (String computation, ComputedFrequency frequency){
		setComputationString(computation.trim());
		this.frequency = frequency;
		
	}
	/**
	 * This is a convenience method to find out if the computation is empty.
	 * If it is, then the field is deemed "non-computed".
	 * @return if the computation is empty
	 */
	public boolean isEmpty(){
		return (computationString == null);
	}
	/**
	 * @return the computation
	 */
	public String getComputationString() {
		return computationString;
	}

	/**
	 * @param computation the computation to set
	 */
	public void setComputationString(String computation) {
		if(computation.equals(""))
			this.computationString = null;
		else
			this.computationString = computation;
	}
	
	/**
	 * @return the frequency
	 */
	public ComputedFrequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(ComputedFrequency frequency) {
		this.frequency = frequency;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((computationString == null) ? 0 : computationString
						.hashCode());
		result = prime * result
				+ ((frequency == null) ? 0 : frequency.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computation other = (Computation) obj;
		if (computationString == null) {
			if (other.computationString != null)
				return false;
		} else if (!computationString.equals(other.computationString))
			return false;
		if (frequency == null) {
			if (other.frequency != null)
				return false;
		} else if (!frequency.equals(other.frequency))
			return false;
		return true;
	}
}
