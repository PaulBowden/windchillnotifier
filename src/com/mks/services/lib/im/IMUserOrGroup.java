/**
 * 
 */
package com.mks.services.lib.im;

/**
 * This Class has all the common elements of a User or a Group in IM. It allow you to use common calls to access all
 * Objects that deal with permissions or Access in a common way. This Class should not be instantiated on its own. It is 
 * preferred to instantiate a Group or a User Object instead.
 * 
 * @author cstoss
 *
 */
public abstract class IMUserOrGroup implements java.io.Serializable {
	/**
	 * Represents the user principal type
	 */
	public final String PRINCIPALTYPE_USER = "user";
	
	/**
	 * Represents the group principal type
	 */
	public final String PRINCIPALTYPE_GROUP = "group";
	/**
	 * Login name of the user
	 */
	private String name;

	/**
	 * Description of the user
	 */
	private String description;
	
	/**
	 * Email address of the user
	 */
	private String email;

	/**
	 * Is this user active?
	 */
	private boolean isActive;

	private String principalType;
	
	/**
	 * Is this user active?
	 * 
	 * @return Returns the isActive.
	 */
	public boolean isActive() {
		return isActive;
	}
	
	/**
	 * Activate/deactivate the user
	 * 
	 * @param isActive The isActive to set.
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * Get the name of the user
	 * 
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of the user
	 * 
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the description of the user
	 * 
	 * @return Returns the descr.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set the description of the user
	 * 
	 * @param descr The descr to set.
	 */
	public void setDescription(String descr) {
		this.description = descr;
	}
	
	/**
	 * Get email address of the user
	 * 
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Set email address of the user
	 * 
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the principalType
	 */
	public String getPrincipalType() {
		return principalType;
	}

	/**
	 * @param principaltype the principalType to set
	 */
	protected void setPrincipalType(String principalType) {
		this.principalType = principalType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (isActive ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((principalType == null) ? 0 : principalType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IMUserOrGroup other = (IMUserOrGroup) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (isActive != other.isActive)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (principalType == null) {
			if (other.principalType != null)
				return false;
		} else if (!principalType.equals(other.principalType))
			return false;
		return true;
	}
	
	/**
	 * A IMUserOrGroup is equal to another IMUserOrGroup when all <i>common</i > attributes are the same.
	 * This excludes the "Full Name" attribute which is specific to a User Object, but includes the principalType
	 * attribute.
	 * 
	 *  This is added for cases where you may not have access to the Object which extends this Class directly.
	 */
	
	
	/*public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		IMUserOrGroup i = (IMUserOrGroup)o;
		
		return (this.isActive() == i.isActive()
				 && this.getEmail().equals(i.getEmail())
				 && this.getDescription().equals(i.getDescription())
				 && this.getName().equals(i.getName())
				 && this.getPrincipalType().equals(i.getPrincipalType())
				 );
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + getEmail().hashCode();
		hash = hash * 31 + getDescription().hashCode();
		hash = hash * 31 + getName().hashCode();
		hash = hash*31 + (isActive() ? 1 : 0);
		hash = hash * 31 + getPrincipalType().hashCode();
		return hash;
	}*/
}
