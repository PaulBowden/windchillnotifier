package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.mks.api.Command;
import com.mks.api.response.Field;
import com.mks.api.response.Item;
import com.mks.api.response.Response;
import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.Connection;
import com.mks.services.lib.MKSLibConnectionException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

public abstract class GeneralField implements java.io.Serializable, IField{		
	
	/**
	 * Name of the field
	 */
	private String name;
		/**
	 * Displayname of the field
	 */
	private String displayname;
	
	/**
	 * Description of the field
	 */
	private String description;
		
	/**
	 * Position of the field
	 */
	private String position;
		
	private FieldType fieldtype;
	/**
	 * Is this field mandatory?
	 */
	private boolean mandatory;
	protected Computation computation;
	private List<String> permittedGroups = new ArrayList<String>();
	
	
	public GeneralField(String name, FieldType fieldtype)
	{
		this.name = name;
		this.fieldtype = fieldtype;
	}
	
	public List<String> getPermittedGroups() {
		return permittedGroups;
	}

	public void setPermittedGroups(List<String> permittedGroups) {		
		this.permittedGroups = permittedGroups;
	}
	
	public void removePermittedGroup(String s)
	{
		permittedGroups.remove(s);
	}

	public void addPermittedGroup(String s)
	{
		if(!isStrInArrayList(s, permittedGroups))
			permittedGroups.add(s);	
	}
	private boolean isStrInArrayList(String s, List<String> al)
	{
		return al.contains(s);
	}
	/**
	 * Is this field mandatory?
	 * Used when a field is on a particular Type
	 * 
	 * @return returns true iff this field is mandatory.
	 */
	public boolean isMandatory() {
		return mandatory;
	}
	/**
	 * Set if this field is mandatory
	 * 
	 * @param mandatory
	 */
	public void setMandatory(boolean mandatory) {
			this.mandatory = mandatory;
	}
	
	/**
	 * Get the field description
	 * 
	 * @return Returns the description.
	 */
	public String getDescription() {		
		return description;
	}
	/**
	 * Set the field description
	 * 
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
		
	/**
	 * Get name of this field
	 * 
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set name of this field
	 * 
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the position
	 * 
	 * @return the position
	 */
	public String getPosition() {
		if(null == position)
			return com.mks.services.lib.GlobalVariable.NULL_VALUE;
		else if(position.length()>0 && (position.equals("first") || position.equals("last") || position.startsWith("before:") || position.startsWith("after:")))
			return position;
		else
		{
			try{
				Integer.parseInt(position);			
			}catch(NumberFormatException e){
				return com.mks.services.lib.GlobalVariable.NULL_VALUE;
			}
			return position;
		}		
	}	
	/**
	 * Set the position of this field
	 * 
	 * @param position The position to set.
	 */
	public void setPosition(String position) {
		this.position = position;
	}
	public Computation getComputation() {		
		return computation;
	}
	public void setComputation(Computation computation) {
		if(allowComputedValue()){
			this.computation = computation;
		}
		else{
			throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());
		}
	}
	public boolean allowFieldCorrelations(){
		return (fieldtype.isRelationship() && fieldtype.isNeedsQuery());
	}
	/**
	 * True if the field type is  a number field 
	 * @return true if the field is a Number Field 
	 */
	public boolean isNumberField(){
		return fieldtype.isNumberField();
	}
	/**
	 * If the field has a valid minimum value it is returned, otherwise <tt>null</tt> is.
	 * <br>The Objects are returned as follows:<br>
	 * {@link com.mks.services.lib.im.FieldType#INTEGER} returns {@link java.lang.Integer#TYPE}<br>
	 * {@link com.mks.services.lib.im.FieldType#FLOAT} returns  {@link java.lang.Float#TYPE}
	 * 
	 */
	public Object getMinValue(){return null;}
	
	/**
	 * If the Field allows a Minimum Field Value and the Object is the correct Object type for that
	 * Field type then the Minimum Value will be set.
	 * <br>Objects should be set as follows:<br>
	 * {@link com.mks.services.lib.im.FieldType#INTEGER} returns {@link java.lang.Integer#TYPE}<br>
	 * {@link com.mks.services.lib.im.FieldType#FLOAT} returns  {@link java.lang.Float#TYPE}
	 * 
	 * <br>NOTE: In both cases above the respective {@link java.lang.String#TYPE} representations of those Objects will be accepted.
	 * @throws MethodNotImplementedException if the field does not allow for a minimum Value to be set
	 * @throws IllegalValueDetectedException if the value is illegal for the type of field
	 */
	public void setMinValue(Object minValue){throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public Object getMaxValue(){return null;}
	public void setMaxValue(Object maxValue){}
	public Object getDefaultValue(){return null;}
	public void setDefaultValue(Object defaultValue){}
	public void setDisplayRows(int displayRows){}
	public int getDisplayRows(){
		return 0;}
	public boolean isDisplayProgressBar(){return false;}
	public void setDisplayProgressBar(boolean displayProgressBar){}
	public List<IField> getColumns() throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setColumns(List<IField> columns) throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public Query getQuery() throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public String getDisplayType() throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}	
	public String getRelationship() throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public boolean isVariableHeightRows() throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setBackedby(String backedby ) throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setDisplayType(String displayType) throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setQuery(Query query)throws MethodNotImplementedException { throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setRelationship(String relationship) throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public void setVariableHeightRows(boolean variableHeightRows) throws MethodNotImplementedException{ throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());}
	public boolean isMultiValued() {
		return false;
	}
	public void setMultiValued(boolean isMultiValued) {
	}	
	public List<PickItem> getPickItems() {
		return null;
	}

	/**
	 * Sets the list of Pick Items
	 * @param pickItems the pickItems to set
	 */
	public void setPickItems(List<PickItem> pickItems) {
	}	

	/**
	 * Takes no actions for all fields except the QBR field.
	 */
	public void setFieldCorrelations(List<FieldCorrelation> fieldCorrelations){		
	}
	/**
	 * Always returns null
	 */
	public List<FieldCorrelation> getFieldCorrelations(){
		return null;
	}
	
	
	/**
	 * Set the position of this field
	 * 
	 * @param position The position to set.
	 */
	public void setPosition(int position) {
		setPosition(""+position);
	} 
	
	/**
     * Get the displayname of this field
     * 
     * @return Returns the displayname.
     */
    public String getDisplayname() {
        return displayname;
    }
    
    /**
     * Set the displayname of this field
     * 
     * @param displayname The displayname to set.
     */
    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }
    


    public String toString()
    {
    	String retStr = "Field Definition for Field: (" + getName() + ") "+ getFieldTypeAsStr() +"\n";
    	return retStr;
    }



	/**

	/**
	 * Retrieve a Field by name.
	 * 
	 * A few notes:
	 *  - LongText fields with logging turned on will always be retrieved as mostRecentFirst. This is due to RFC 145321. If you you would like this to change, please contact support@mks.com 
	 * 
	 * @param name The field name to retrieve
	 * @param connection The connection to use
	 * @return The field definition as an Object
	 * @throws MKSLibConnectionException Thrown when there is a problem running the command to get the field specified by <tt>name</tt>
	 */
	public static GeneralField getField(String name, Connection connection) throws MKSLibConnectionException
	{
		Command viewField = new Command(Command.IM, "viewfield");
		viewField.addSelection(name);		
		return GeneralField.parseFieldResponse(connection.runCommand(viewField), name);
	}
	
	private static GeneralField parseFieldResponse(Response r, String id)
	{
		WorkItem fieldWI = null;
		
		try{
			fieldWI = r.getWorkItem(id);
		}catch(NoSuchElementException  nsee){			
			return null;
		}
		
		GeneralField newField = null;
		String typename = fieldWI.getField("type").getValueAsString();
	
		if(typename.equals("shorttext")) {
			newField = ShortTextField.getField(fieldWI);
		}
		else if(typename.equals("integer")) {
			newField = IntField.getField(fieldWI);
		}
		else if(typename.equals("longtext")) {
			newField = LongTextField.getField(fieldWI);
		}
		else if(typename.equals("logical")) {
			newField = LogicalField.getField(fieldWI);
		}
		else if(typename.equals("float")) {
			newField = FloatField.getField(fieldWI);
		}
		else if(typename.equals("date")) {
			newField = DateField.getField(fieldWI);
		}
		else if(typename.equals("pick")) {
			newField = PickField.getField(fieldWI);
		}
		else if(typename.equals("attachment")) {
			newField = AttachmentField.getField(fieldWI);
		}		
		else if(typename.equals("fva")) {
			newField = FVAField.getField(fieldWI);
		}
		/*else if(typename.equals("phase")) {
			newField = PhaseField.getField(fieldWI);
		}		*/
		/*else if(typename.equals("qbr")) {
		newField = PhaseField.getField(fieldWI);
		}		*/
		/*else if(typename.equals("group")) {
		newField = PhaseField.getField(fieldWI);
		}		*/

		else if(typename.equals("user")) {
			newField = UserField.getField(fieldWI);
		}
		else if(typename.equals("ibpl")) {
			newField = IBPLField.getField(fieldWI);
		}	
		else if(typename.equals("project")) {
			newField = ProjectField.getField(fieldWI);
		}
		else if(typename.equals("relationship")) {
			newField = ProjectField.getField(fieldWI);
		}
		newField = addGroupsToField(newField, fieldWI);
		
		
		return newField;
	}
	
	protected static Computation computationParser(Item item){
		if(item ==null)
			return null;
		Field computation = item.getField("computation");
		
		if(computation.getString() ==null)
			return null;
		
		Computation c = new Computation(computation.getString());
		Field storeToHistoryFrequency = item.getField("storeToHistoryFrequency");
		Field staticComputation = item.getField("staticComputation");
		
		if(!staticComputation.getBoolean())
			c.setFrequency(ComputedFrequency.DYNAMIC);
		else
			c.setFrequency(ComputedFrequency.valueOf(
					storeToHistoryFrequency.getString()
										.toUpperCase()
										.trim()));
		return c;
	}
	
	private static GeneralField addGroupsToField(GeneralField f, WorkItem wi)
	{		
		return f;
	}

	/**
	 * This can be used for name verification of fields. It will allow you to compare the names
	 * of any Class that extends GeneralField.
	 * 
	 * @param gf The field to compare.
	 * @return true if the names are the same and not null
	 */
	public boolean hasSameName(GeneralField gf)
	{
		if(null == gf)
			return false;
		if(null == gf.getName())
			return false;
		return ( this.getName().equals(gf.getName()));
	}
	/**
	 * This checks if the fields are the same. 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		GeneralField i = (GeneralField)o;
		
		return ( i.getName().equals(this.getName()));
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getName().hashCode();
		return hash;
	}
	/**
	 * Returns an Integer constant value for the field type
	 * 
	 *  @return the int value of the Field Type
	 */
	public FieldType getFieldType()
	{
		return fieldtype;
	}
	
	/**
	 * Returns the String value of the Field type
	 * 
	 * @return the String value of the Field type
	 */
	public String getFieldTypeAsStr()
	{
		return fieldtype.getFieldtype();
	}
	public boolean allowComputedValue(){
		return fieldtype.isAllowComputed();
	}
	/**
	 * Returns if the field is computed.
	 * Will always return false for fields where computations are not allowed to be set.
	 * @return true iff the field can be computed and has a computation set.
	 * @see #setComputation(Computation)
	 */
	public boolean isComputed(){
		if(!fieldtype.isAllowComputed())
			return false;
		
		if(computation != null)
			return !computation.isEmpty();
		else
			return false;
	}
	
	protected String trace(StackTraceElement e[]){
		boolean doNext = false;
		for(StackTraceElement s : e){
			if(doNext){
				return s.getMethodName();
			}
			doNext =  s.getMethodName().equals("getStackTrace");
		}
		return "";
	}
}
