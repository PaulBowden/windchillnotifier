/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;

import com.mks.api.response.Item;
import com.mks.api.response.ItemList;
import com.mks.api.response.ValueList;
import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class QBRField extends GeneralField implements IField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8534784546119716075L;
	public static final String DISPLAYTYPE_TABLE = "table";
	public static final String DISPLAYTYPE_CSV = "csv";
	
	private Query query;
	private List<FieldCorrelation> fieldCorrelations;
	private String displayType;
	private int displayRows;
	private boolean variableHeightRows;
	private List<IField> columns;
	
	
	public QBRField(String name)
	{
		super(name, FieldType.QBR);
		this.query = null;
	}
	
	public QBRField(String name, Query query)
	{
		super(name, FieldType.QBR);
		this.query = query;
	}
	/**
	 * @return the query
	 */
	public Query getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(Query query) {
		this.query = query;
	}

	/**
	 * Retrieve a list of the Field Correlations
	 * @return the fieldCorrelations
	 */
	public List<FieldCorrelation> getFieldCorrelations() {
		return fieldCorrelations;
	}

	/**
	 * Sets the field correlations for this QBR
	 * 
	 * @param fieldCorrelations the fieldCorrelations to set
	 */
	public void setFieldCorrelations(List<FieldCorrelation> fieldCorrelations) {
		if(allowFieldCorrelations())
			this.fieldCorrelations = fieldCorrelations;
		else
			throw new MethodNotImplementedException(trace(Thread.currentThread().getStackTrace()), this.getFieldTypeAsStr());
	}

	/**
	 * @return the displayType
	 */
	public String getDisplayType() {
		return displayType;
	}

	/**
	 * Sets the Display Type. If the display type entered isn't correct the default DISPLAYTYPE_TABLE is used.
	 *
	 * @param displayType the displayType to set
	 */
	public void setDisplayType(String displayType) {
		displayType = displayType.toLowerCase();
		if(displayType.equals(DISPLAYTYPE_CSV)
				|| displayType.equals(DISPLAYTYPE_TABLE))
			this.displayType = displayType;
		else
			this.displayType = DISPLAYTYPE_TABLE;
	}

	/**
	 * @return the displayRows
	 */
	public int getDisplayRows() {
		return displayRows;
	}

	/**
	 * @param displayRows the displayRows to set
	 */
	public void setDisplayRows(int displayRows) {
		this.displayRows = displayRows;
	}

	/**
	 * @return the variableHeightRows
	 */
	public boolean isVariableHeightRows() {
		return variableHeightRows;
	}

	/**
	 * @param variableHeightRows the variableHeightRows to set
	 */
	public void setVariableHeightRows(boolean variableHeightRows) {
		this.variableHeightRows = variableHeightRows;
	}

	/**
	 * Returns the columns as a list of 
	 * @return the columns
	 */
	public List<IField> getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(List<IField> columns) {
		this.columns = columns;
	}
	protected static QBRField getField(WorkItem item)
	{
		QBRField f = new QBRField(item.getField("name").getValueAsString());
		ValueList colsVL = (ValueList)item.getField("defaultList").getList();
		
		List<String> cols = new ArrayList<String>();
		for(int i=0;i<colsVL.size();i++)
		{
			cols.add(colsVL.get(0).toString());
		}
		f.setDisplayname(item.getField("displayName").getString());
		
		f.setQuery(new Query(item.getField("query").getString()));
		String correlation =  item.getField("correlation").getString();
		//FieldCorrelation fc = new FieldCorrelation() 
		f.setDescription(item.getField("Description").getString());

		//f.setMaxLength(item.getField("MaxLength").getInteger().intValue());
		f.setPosition(item.getField("Position").getInteger().intValue());

		System.out.println("What is this: " + f.getName());
		return f;
	}

}
