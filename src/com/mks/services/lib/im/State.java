package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class State implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7441299342387469732L;

	/**
	 * Mandatory fields in this state
	 */
	private List<IField> mandatoryFields = new ArrayList<IField>();
	
	/**
	 * name of this state
	 */
	private String name;
	/**
	 * description of this state
	 */
		
	private String description;
		
	/**
	 * Constructor
	 * 
	 * @param s
	 */
	public State(String s) {
		name = s;
	}

	
	/**
	 * Add mandatory field
	 * 
	 * @param f The field to make mandatory in this State
	 */
	public void addMandatoryField(IField f) {
		if(!mandatoryFields.contains(f)) {
			mandatoryFields.add(f);
		}
	}
	
	/**
	 * Get mandatory fields as  a List
	 * 
	 * @return the List of all Mandatory fields on this State.
	 */
	public List<IField> getMandatoryFields() {
		return mandatoryFields;
	}
	
	/**
	 * Checks by field name if it is already set to mandatory on this State
	 * 
	 * @param fieldname
	 * @return true iff the Field is mandatory on the State, false otherwise
	 */
	public boolean isMandatory(String fieldname) {		
		for(Iterator<IField> list = this.getMandatoryFields().iterator();list.hasNext();)
		{
			if(((list.next()).getName()).equals(fieldname))
				return true;					
		}
		return false;
	}
	
	/**
	 * Get name of the state
	 * 
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set name of the state
	 * 
	 * @param n
	 */
	public void setName(String n) {
	    name = n;
	}

	/**
	 * Get name of the state
	 * 
	 * @return Returns the name.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set name of the state
	 * 
	 * @param d The description of the State
	 */
	public void setDescripton(String d) {
	    description = d;
	}

	
	/**
	 * A State is defined as "equal" solely if the name and the description are the same. This is because
	 * a state can have different mandatory fields depending on the Type(s) it is visible in. 
	 * 
	 * @return true if and only if the name and the description of the two objects are the same.
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if((o == null) || (o.getClass() != this.getClass()))
		{
			return false;
		}
		State s = (State)o;
		return (s.getName().equals(name)
				&& s.getDescription().equals(description));
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash*31 + name.hashCode();
		hash = hash * 31 + description.hashCode();
		return hash;
	}
}
