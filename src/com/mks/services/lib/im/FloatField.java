package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.IllegalValueDetectedException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * 
 * @author cstoss
 *
 */
public class FloatField extends GeneralField implements IField {
	
	public static final Float EMPTYFLOAT = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = -84965276497908242L;
	private Object minValue;
	private Object maxValue;
	private Object defaultValue;
	private DisplayPattern displayPattern;

	public FloatField(String name)
	{
		super(name, FieldType.FLOAT);	
		minValue = EMPTYFLOAT;
		maxValue = EMPTYFLOAT;
		defaultValue = EMPTYFLOAT;
	}
	
	
	
	/**
	 * @param minValue
	 * @param maxValue
	 * @param defaultValue
	 */
	public FloatField(String name, Object minValue, Object maxValue, 
			Object defaultValue) {
		super(name, FieldType.FLOAT);
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
	}



	/**
	 * @return the minValue
	 */
	public Object getMinValue() {
		return minValue;
	}
	private boolean testValue(Object o){
		Float testValues = null;
		try{
			testValues = (Float)o;
		}catch(ClassCastException cce){
			try{
				testValues = Float.valueOf(o.toString());
			}catch(NumberFormatException nfe){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Object minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public Object getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Object maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the defaultValue
	 */

	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	
	protected static FloatField getField(WorkItem item)
	{
		FloatField f = new FloatField(item.getField("name").getValueAsString());
		
		
		f.setDisplayname(item.getField("displayName").getString());
		
		System.out.println("What is this: " + f.getName());
		try{
			f.setDefaultValue(new Float(item.getField("default").getDouble().floatValue()));
		}catch(NullPointerException npe){
			f.setDefaultValue(FloatField.EMPTYFLOAT);
		}
		
		f.setDescription(item.getField("Description").getString());
		try{
			f.setMaxValue(new Float(item.getField("max").getDouble().floatValue()));
		}catch(NullPointerException npe){
			f.setMaxValue(FloatField.EMPTYFLOAT);
		}
		try{
			f.setMinValue(new Float(item.getField("min").getDouble().floatValue()));
		}catch(NullPointerException npe){
			f.setMinValue(FloatField.EMPTYFLOAT);
		}
		f.setComputation(computationParser(item));
		f.setPosition(item.getField("Position").getInteger().intValue());

		
		return f;
	}


	/**
	 * @return the displayPattern
	 */
	public DisplayPattern getDisplayPattern() {
		return displayPattern;
	}

	/**
	 * @param displayPattern the displayPattern to set
	 */
	public void setDisplayPattern(DisplayPattern displayPattern) {
		this.displayPattern = displayPattern;
	}

	/**
	 * Checks all attributes of the FloatField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		FloatField i = (FloatField)o;
		
		Computation thisComp = null;
		Computation thatComp = null;
		if(this.isComputed())
			thisComp = this.getComputation();
		if(i.isComputed())
			thatComp = i.getComputation();
		
		
		return ( i.getName().equals(this.getName())
					&& i.getDescription().equals(this.getDescription())
					&& i.getDisplayname().equals(this.getDisplayname())
					&& i.getMinValue() == this.minValue
					&& i.getMaxValue() == this.maxValue
					&& i.getDefaultValue() == this.defaultValue 
					&& i.isComputed() == this.isComputed()
					&& thisComp.equals(thatComp));					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + (null == maxValue ? 0 : ((Float)maxValue).intValue());
		hash = hash * 31 + (null == minValue ? 0 : ((Float)minValue).intValue());
		hash = hash * 31 + (null == defaultValue ? 0 : ((Float)defaultValue).intValue());
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + (this.isComputed() ? 1 : 0);
		
		if(this.isComputed())
			hash = hash *31 + getComputation().hashCode();
		else
			hash = hash *31 + "".hashCode();
		return hash;
	}
}
