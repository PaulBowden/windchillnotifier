/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import com.mks.api.Command;
import com.mks.api.Option;
import com.mks.api.response.Field;
import com.mks.api.response.Item;
import com.mks.api.response.ItemList;
import com.mks.api.response.Response;
import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.Connection;
import com.mks.services.lib.MKSLibConnectionException;
import com.mks.services.lib.ObjectNotAvailableException;
import com.mks.services.lib.ObjectAlreadyExistsException;

/**
 * The Type Class. It gives you the ability to put all aspects of a Type into an Object.
 * 
 * Upon creation of a new Type Object a State is also created called "Unspecified". This State can be used to start the
 * Workflow creation by using the getUnspecifiedState method.
 * 
 * 
 * @author cstoss
 *
 */
public class Type implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7277331836898142248L;
	/**
	 * Indicates that no CP Creation Policy is set
	 */
	public static final int CPCREATIONPOLICY_NOTSET = -1;
	/**
	 * Indicates that the CP Creation Policy is set to "anyone"
	 */
	public static final int CPCREATIONPOLICY_ANYONE = 0;
	/**
	 * Indicates that the CP Creation Policy is set to a user field
	 */
	public static final int CPCREATIONPOLICY_USERFIELD = 1;
	/**
	 * Indicates that the CP Creation Policy is set to a group field
	 */
	public static final int CPCREATIONPOLICY_GROUPFIELD = 2;
	/**
	 * Indicates that the CP Creation Policy is set to a set of groups
	 */
	public static final int CPCREATIONPOLICY_GROUPS = 3;
	
	/**
	 * Name of the type
	 */
	private String name;

	/**
	 * Description of the type
	 */
	private String description;

	/**
	 * Fields contained in the type
	 */
	private List<IField> fields = new ArrayList<IField>();

	/**
	 * Notification Fields contained in the type
	 */
	private List<IField> notificationFields = new ArrayList<IField>();

	/**
	 * That phase field associated with this Type
	 */
	private PhaseField phaseField;
	
	/**
	 * States of the type
	 */
	private List<State> states = new ArrayList<State>();

	/**
	 * Field Relationships of the type
	 */
	private List<GeneralFieldRelationship> fieldrelationships = new ArrayList<GeneralFieldRelationship>();	
	
	/**
	 * Are CPs allowed for this issue type?
	 */
	private boolean allowCPs;
	
	private int CPCreationPolicy = Type.CPCREATIONPOLICY_NOTSET; // This indicate that
	private List<Group> groupCPCreatePolicyList;
	
	/**
	 * Is Time Tracking allowed for this issue type?
	 */
	private boolean allowTimeTracking;

	/**
	 * Should the WorkFlow be displayed for this type?
	 */
	private boolean showWorkflow;

	/**
	 * This position in the adminGUI
	 */
	private String position;
	
	/**
	 * A list of all Permitted Groups for this type
	 */
	private List<Group> permittedGroups = new ArrayList<Group>();
	/**
	 * A list of Users or Groups of Permitted Administrators.
	 */
	private List<IMUserOrGroup> permittedAdmins = new ArrayList<IMUserOrGroup>();
	/**
	 * State Transitions for the Workflow
	 */
	private List<StateTransition> stateTransitions = new ArrayList<StateTransition>();
	
	private HashMap<String, String> typeProperties = new HashMap<String, String>();
	
	
	/**
	 *  The Constructor used when you don't know the Type name. The Type name will be set to <tt>null</tt> and must
	 *  be set prior to using an method to alter the type on an Integrity Server
	 */
	public Type()
	{
		this.name = null;		
		try {
			this.addState(new State("Unspecified"));
		} catch (ObjectNotAvailableException e) {
		}
	}
	
	/**
	 *  The Constructor used when you know the Type name.
	 *  
	 * @param name the name of the Type in Integrity
	 */
	public Type(String name)
	{
		this.name = name;
		try {
			this.addState(new State("Unspecified"));
		} catch (ObjectNotAvailableException e) {
		}
	}
	
	/**
	 * Add a Field to this Type
	 * 
	 * @param f The Field you wish to add
	 * @return true/false - based on Collection.add(Object o)
	 */
	public boolean addField(IField f)
	{
		if(!isFieldInType(f.getName()))
			return fields.add(f);
		else
			return false;				
	}
	/**
	 * Add a Permitted Administrator
	 * @param userOrGroup Permitted Administrator User or Group to add.
	 */
	public void addPermittedAdmin(IMUserOrGroup userOrGroup)
	{
		if(!isObjInArrayList(userOrGroup, permittedAdmins))
			permittedAdmins.add(userOrGroup);	
	}
	
	/**
	 * Add a Permitted Group for this Type
	 * 
	 * @param g The group to add
	 */
	public void addPermittedGroup(Group g)
	{
		if(!isObjInArrayList(g, permittedGroups))
			permittedGroups.add(g);	
	}
	

	/**
	 * Add a State to this Type
	 * 
	 * @param s The State you wish to add
	 * @return true - as specified by Collections.add(Object)
	 * 
	 * @throws ObjectNotAvailableException Only thrown if makeVisible is false. 
	 */
	public boolean addState(State s) throws ObjectNotAvailableException
	{
		boolean success;
		if(getStateIndex(s.getName())<0)
			success = states.add(s);
		else
			success = false;				

		if(success)
		{
			for(Iterator<IField> fi = s.getMandatoryFields().iterator(); fi.hasNext();)
			{
				IField curF = fi.next();
				if(!isFieldInType(curF.getName()))
					throw new ObjectNotAvailableException("Field " + curF.getName() + " is mandatory in State " + s.getName() + ", but is not visible in Type " + this.getName()+ ". Either remove this Mandatory field or add it to the visible fields.");
			}		
			
		}		
		return success;
	}
	
	private void addOrOverrideState(State s)
	{
		int idx = getStateIndex(s.getName());
		if( idx >=0)
			states.remove(idx);		
		states.add(s);
	}
	
	
	/**
	 * This is how to add a new State Transition to a Type. This will throw StateNotVisibleException if one of
	 * the states you use is not associated with the Type.<br>If the StateTransition with a matching Start and Target State exists
	 * the permitted group will be the <b>Union</b> of the two permittedGroups Lists added.
	 * 
	 * @param startState The initial state or "From State"
	 * @param targetState The state to transition to
	 * @param permittedGroups The groups permitted to make this transition
	 * 
	 * @throws StateNotVisibleException
	 */
	public void addStateTransition(State startState, State targetState, List<Group> permittedGroups) throws ObjectNotAvailableException
	{
		if(getStateIndex(startState.getName()) <0)
		{
			try {
				addState(startState);
			} catch (ObjectNotAvailableException e) {
				e.printStackTrace();
			}
		}
		
		if(getStateIndex(targetState.getName()) <0)
		{
			try {
				addState(targetState);
			} catch (ObjectNotAvailableException e) {
				e.printStackTrace();
			}
		}		
		if(getStateIndex(startState.getName())>=0)
			if(getStateIndex(targetState.getName())>=0)
			{
				StateTransition st1 = new StateTransition(startState, targetState, permittedGroups);
				StateTransition mST = matchingST(st1);
				if(mST == null)
					stateTransitions.add(st1);
				else
				{
					for(int i=0;i<permittedGroups.size();i++)
						st1.addPermittedGroup(permittedGroups.get(i));
					for(int j=0;j<mST.getPermittedGroups().size();j++)
						st1.addPermittedGroup(mST.getPermittedGroups().get(j));
					stateTransitions.remove(mST); // removes the old copy of the Object
					stateTransitions.add(st1); //adds the new copy of the Object
				}
			}				
			else
				throw new ObjectNotAvailableException("The Target State: " + targetState.getName() + " is not in Type " + this.getName());
		else
			throw new ObjectNotAvailableException("The Initial State: " + startState.getName() + " is not in Type " + this.getName());
	}
	
	/**
	 * This is how to add a new State Transition to a Type if the State Transition is already created. This will throw StateNotVisibleException if one of
	 * the states you use is not associated with the Type.<br>If the StateTransition with a matching Start and Target State exists
	 * the permitted group will be the <b>Union</b> of the two permittedGroups Lists added.
	 * 
	 * @param st The State Transition to add.
	 * @throws StateNotVisibleException
	 */
	public void addStateTransition(StateTransition st) throws ObjectNotAvailableException
	{
		addStateTransition(st.getFromState(),st.getToState(),st.getPermittedGroups());
	}

	/**
	 * This method will generate a Command Line for the Type
	 * 
	 * @param create - true if you want it to be a 'im createtype', false if you want 'im edittype'
	 * 
	 * @return The IM CLI String for the command.
	 */
	@SuppressWarnings("unchecked")
	public String generateCommandLine(boolean create)
	{
		String retStr = "im ";
		if(create)
			retStr += "createtype ";
		else
		{
			retStr += "edittype ";
			return "Edit Type not supported at this time";
		}
		
		CLIOptions args = getOptions();
		
		for (Enumeration<String> opts = (Enumeration<String>)args.propertyNames() ; opts.hasMoreElements() ;) {
		         retStr += args.getOption(opts.nextElement());
		}		
		if(!create)
			retStr += this.getName();
		
		return retStr;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the fields
	 */
	public List<IField> getFields() {
		return fields;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the permittedAdmins
	 */
	public List<IMUserOrGroup> getPermittedAdmins() {
		return permittedAdmins;
	}
	
	/**
	 * @return the permittedGroups
	 */
	public List<Group> getPermittedGroups() {
		return permittedGroups;
	}

	/**
	 * Returns the position
	 * 
	 * @return the position
	 */
	public String getPosition() {
		
		if(null == position)
			return com.mks.services.lib.GlobalVariable.NULL_VALUE;
		else if(position.length()>0 && (position.equals("first") || position.equals("last") || position.startsWith("before:") || position.startsWith("after:")))
			return position;
		else
		{
			try{
				Integer.parseInt(position);			
			}catch(NumberFormatException e){
				return com.mks.services.lib.GlobalVariable.NULL_VALUE;
			}
			return position;
		}		
	}

	/**
	 * @return the states
	 */
	public List<State> getStates() {
		return states;
	}

	/**
	 * @return the stateTransitions
	 */
	public List<StateTransition> getStateTransitions() {
		return stateTransitions;
	}

	/**
	 * This allows you to get the Unspecified State. Should be used to generate the initial transition(s)
	 * 
	 * @return The Unspecified State for the Type. 
	 */
	public State getUnspecifiedState()
	{
		for(int i=0;i<states.size();i++) {
			if(states.get(i).getName().equals("Unspecified"))
				return states.get(i);			
		}
		/* This recursion is used in case someone removes the Unspecified State */
		try {
			this.addState(new State("Unspecified"));
		} catch (ObjectNotAvailableException e) {
		}
		return getUnspecifiedState();
	}

	/**
	 * @return the allowCPs
	 */
	public boolean isAllowCPs() {
		return allowCPs;
	}

	/**
	 * @return the allowTimeTracking
	 */
	public boolean isAllowTimeTracking() {
		return allowTimeTracking;
	}

	/**
	 * Checks to see if a field is in the field list for this type, based on its name
	 * 
	 * @param fieldName
	 * @return true - if the field is in the fields list, false otherwise
	 */
	public boolean isFieldInType(String fieldName)
	{
		if(fieldName==null) 
			return false;
		for(int i=0;i<fields.size();i++) {
			if(fields.get(i).getName().equals(fieldName))
				return true;			
		}
		return false;
	}

	/**
	 * @return true the workflow will be shown in the IPT, false otherwise
	 */
	public boolean isShowWorkflow() {
		return showWorkflow;
	}

	/**
	 * Checks to see if a state is in the state list for this type, based on its name
	 * 
	 * @param stateName The State name to check
	 * @return the index of the state or -1 if it does not exist on the Type yet
	 */
	public int getStateIndex(String stateName)
	{
		if(stateName==null) 
			return -1;
		for(int i=0;i<states.size();i++) {
			if(states.get(i).getName().equals(stateName))
				return i;
		}
		return -1;
	}
	
	/**
	 * Remove a Field from the Type
	 * 
	 * @param field the GeneralField to remove
	 * @return true - if the Field was present, false otherwise
	 */
	public boolean removeField(GeneralField field)
	{
		return fields.remove(field);		
	}
	/**
	 * Remove a Field from the Type by its name
	 * 
	 * @param fieldname the name of the Field to remove
	 * @return true - if the Field was present, false otherwise
	 */
	public boolean removeField(String fieldname)
	{
		for(IField f : fields)
		{
			if(f.getName().equals(fieldname));
				return fields.remove(f);
		}
		return false;
				
	}
	
	/**
	 * Remove a permitted Administrator group or user.
	 * 
	 * @param userOrGroup The group or user you want to remove.
	 */
	public void removePermittedAdmin(IMUserOrGroup userOrGroup)
	{
		removePermittedAdmin(userOrGroup.getName());
	}
	/**
	 * Remove a permitted group or user by its name.
	 * 
	 * @param userOrGroupName The group or user name you want to remove.
	 */
	public void removePermittedAdmin(String userOrGroupName)
	{
		for(Iterator<IMUserOrGroup> list = permittedAdmins.iterator();list.hasNext();)
		{	
			IMUserOrGroup curGroup = list.next();
			if((curGroup.getName()).equals(userOrGroupName))
			{
				permittedAdmins.remove(curGroup);
				break;
			}
		}		
	}
	/**
	 * Remove a permitted group.
	 * 
	 * @param g The group you want to remove.
	 */
	public void removePermittedGroup(Group g)
	{
		removePermittedGroup(g.getName());
	}
	/**
	 * Remove a permitted group by its name.
	 * 
	 * @param groupName The group name you want to remove.
	 */
	public void removePermittedGroup(String groupName)
	{
		for(Iterator<Group> list = permittedGroups.iterator();list.hasNext();)
		{	
			Group curGroup = list.next();
			if((curGroup.getName()).equals(groupName))
			{
				permittedGroups.remove(curGroup);
				break;
			}
		}		
	}
	
	/**
	 * Remove a State from the Type
	 * 
	 * @param state the State you want to remove.
	 * @return true - if the State was present, false otherwise
	 */
	public boolean removeState(State state)
	{
		return states.remove(state);		
	}
	
	/**
	 * This sets the allowCPs flag true and enforces that you set a policyType.
	 * If the policyType is not set or is invalid  the default is used (<tt>Type.CPCREATIONPOLICY_ANYONE</tt>)
	 * To unset this flag use Type.unsetAllowCPs()
	 * 
	 * @param policyType the allowCPs to set
	 * 
	 * @see #unsetAllowCPs()
	 * @see #CPCREATIONPOLICY_ANYONE
	 * @see #CPCREATIONPOLICY_GROUPFIELD
	 * @see #CPCREATIONPOLICY_GROUPS
	 * @see #CPCREATIONPOLICY_USERFIELD
	 */
	public void setAllowCPs(int policyType) {
		
		this.allowCPs = true;
		if(policyType == Type.CPCREATIONPOLICY_ANYONE 
				||policyType == Type.CPCREATIONPOLICY_GROUPFIELD
				|| policyType == Type.CPCREATIONPOLICY_GROUPS
				|| policyType == Type.CPCREATIONPOLICY_USERFIELD)
			CPCreationPolicy = policyType;
		else
			CPCreationPolicy = Type.CPCREATIONPOLICY_ANYONE;
	}
	
	/**
	 * This unsets the ability for the Type to contain CPs
	 * 
	 * To reset use Type.unsetAllowCPs()
	 * @see #setAllowCPs(int)
	 */
	public void unsetAllowCPs() {
		this.allowCPs = false;
		CPCreationPolicy = Type.CPCREATIONPOLICY_NOTSET;
	}
		

	/**
	 * @param allowTimeTracking the allowTimeTracking to set
	 */
	public void setAllowTimeTracking(boolean allowTimeTracking) {
		this.allowTimeTracking = allowTimeTracking;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param fields the fields to set
	 */
	public void setFields(List<IField> fields) {
		this.fields = fields;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * To set the entire list of admins.
	 * 
	 * @param permittedAdmins the permittedAdmins to set
	 */
	public void setPermittedAdmins(List<IMUserOrGroup> permittedAdmins) {
		this.permittedAdmins = permittedAdmins;
	}

	/**
	 * To set the entire list of permitted groups
	 * 
	 * @param permittedGroups the permittedGroups to set
	 */
	public void setPermittedGroups(List<Group> permittedGroups) {
		this.permittedGroups = permittedGroups;
	}
	
	/**
	 * Set the position to an Integer
	 * 
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = ""+position;
	}
	
	/**
	 * Set one of the String Values for Position. Options can be com.mks.services.lib.GlobalVariable.FIRST_POSITION, com.mks.services.lib.GlobalVariable.LAST_POSITION
	 * 
	 * @param position the position to set. 
	 */
	public void setPosition(String position) {
		if(position.equals(com.mks.services.lib.GlobalVariable.FIRST_POSITION) || position.equals(com.mks.services.lib.GlobalVariable.LAST_POSITION))
			this.position = position;
	}
	
	/**
	 * Set one of the String Values for Position. Options can be com.mks.services.lib.GlobalVariable.BEFORE_POSITION, com.mks.services.lib.GlobalVariable.AFTER_POSITION
	 * 
	 * @param position The position to set.
	 * @param typeName The name of the Type to position in respect to
	 * 
	 */
	public void setPosition(String position, String typeName) {
		if(position.equals(com.mks.services.lib.GlobalVariable.BEFORE_POSITION) || position.equals(com.mks.services.lib.GlobalVariable.AFTER_POSITION))
			this.position = position + ":"+typeName;
	}

	/**
	 * Do you want to show the workflow tab?
	 * 
	 * @param showWorkflow the showWorkflow to set
	 */
	public void setShowWorkflow(boolean showWorkflow) {
		this.showWorkflow = showWorkflow;
	}

	/**
	 * Set the entire list of States
	 * 
	 * @param states the states to set
	 */
	public void setStates(List<State> states) {
		this.states = states;
	}

	/**
	 * Set the entire list of State Transitions
	 * 
	 * @param stateTransitions the stateTransitions to set
	 */
	public void setStateTransitions(List<StateTransition> stateTransitions) {
		this.stateTransitions = stateTransitions;
	}

	/**
	 * "optionizes" the arguments for the CLI
	 * 
	 * @return All the options in a CLIOPtions object
	 */
	private CLIOptions getOptions() {
		CLIOptions allOpts =  new CLIOptions();
		allOpts.setProperty("--name", this.getName());
		allOpts.setProperty("--description", this.getDescription());
		allOpts.setProperty("--position", this.getPosition());
		allOpts.setProperty("showWorkflow", allOpts.optionizeShowWorkflow(this.showWorkflow));
		allOpts.setProperty("allowChangePackages", allOpts.optionizeCPsAllowed(this.allowCPs));
		allOpts.setProperty("enableTimeTracking", allOpts.optionizeTTAllowed(this.isAllowTimeTracking()));
		allOpts.setProperty("--visibleFields", allOpts.optionizeFields(this.fields));
		allOpts.setProperty("--stateTransitions", allOpts.optionizeStateTransitions(this.stateTransitions));
		allOpts.setProperty("--permittedAdministrators", allOpts.optionizePermittedAdmins(this.permittedAdmins));
		allOpts.setProperty("--mandatoryFields", allOpts.optionizeMandatoryFields(this.states));
		allOpts.setProperty("--phaseField", getPhaseName(this.phaseField));
		allOpts.setProperty("--notificationFields", allOpts.optionizeNotificationFields(this.notificationFields));
		return allOpts;
	}
	private String getPhaseName(PhaseField pf) {
		if(null==pf)
			return com.mks.services.lib.GlobalVariable.NULL_VALUE;
		else
			return pf.getName();
	}
	/**
	 * Used to determine if Object o is in List al.
	 * 
	 * @param o The needle
	 * @param al The haystack
	 * @return true - if it is found, false - if it is not found
	 */
	@SuppressWarnings("unchecked")
	private boolean isObjInArrayList(Object o, List al)
	{
		return al.contains(o);
	}	
	
	/**
	 *  This is used to make sure that there are no duplicates.
	 *  
	 * @param st1 The transition you want to compare to the entire list.
	 * @return The current State Transition if it matches, or null if it does not.
	 */
	private StateTransition matchingST(StateTransition st1) {
		  for (Iterator<StateTransition> STs = stateTransitions.iterator() ; STs.hasNext() ;) {
		         StateTransition curST = STs.next();
		         if(curST.compare(st1.getFromState(), st1.getToState()))
		        	 return curST;
		    }
		return null;
	}
	/**
	 * @return the notificationFields
	 */
	public List<IField> getNotificationFields() {
		return notificationFields;
	}
	/**
	 * @param notificationFields the notificationFields to set		
	 */
	public void setNotificationFields(List<IField> notificationFields) {
		this.notificationFields = notificationFields;
	}
	/**
	 * @return the phaseField
	 */
	public PhaseField getPhaseField() {
		return phaseField;
	}
	/**
	 * Add the Phase Field. Note if it isn't already Visible in the type, it will be added, as this is mandatory.
	 * 
	 * @param phaseField the phaseField to set
	 */
	public void setPhaseField(PhaseField phaseField) {
		this.phaseField = phaseField;
		this.addField(phaseField);
	}	

	/**
	 * Will add a field to the Notification Field list. Will throw a FieldNotVisibleException if the 
	 * Field is not Visible on the Type.
	 * 
	 * <b>NOTE</b>: This will not add the Summary Field, as it is on by default. It is <i>not</i> a bug that it will not appear.
	 * 
	 * @param f - The field you want to add
	 * @throws ObjectNotAvailableException
	 */
	public void addNotificationField(IField f) throws ObjectNotAvailableException
	{
		if( !"Summary".equals(f.getName()))
		{
			if(isFieldInType(f.getName()))
				notificationFields.add(f);
			else
				throw new ObjectNotAvailableException("Field " + f.getName() + " is not visible in Type " + this.getName() + " and cannot be added as a Notification Field.");
		}
	}
	
	/**
	 * Is the Field Relationship already added
	 * 
	 * @param fr The Field Relationship that you want to check.
	 * @return true - if it does exist already, false - if not
	 * @throws ObjectAlreadyExistsException
	 */
	private boolean isFieldRelationshipAdded(GeneralFieldRelationship fr) //throws FieldRelationshipAlreadyExistsException
	{
		int numFR = fieldrelationships.size();
		
		for(int i=0;i<numFR;i++)
		{
			GeneralFieldRelationship curFr = fieldrelationships.get(i);
			if(curFr.isRuleBased())
			{
				if(curFr.getTargetField() == fr.getTargetField())
				{
					return true;
				}
			}		
			else
			{
				NormalFieldRelationship nfr = (NormalFieldRelationship)curFr;
				NormalFieldRelationship nfr2 = (NormalFieldRelationship)fr;
				if(curFr.getTargetField() == fr.getTargetField() 
						&& nfr.getSourceField() == nfr2.getSourceField())
					return true;
			}
			
				
			
			//		if(curFr.getSourceField().equals(fr.getSourceField()) && curFr.getTargetField().equals(fr.getTargetField()))
				return true; //throw new FieldRelationshipAlreadyExistsException("You cannot add two Field Relationships with the same Source (" + fr.getSourceField().getName() + ") and Target (" + fr.getTargetField().getName()+") Fields.");
		}
		return false;
	}
	
	/**
	 * Add the Field Relationship to the Type
	 * 
	 * @param fr The Field Relationship to add
	 * 
	 * @throws ObjectAlreadyExistsException
	 */
	public void addFieldRelationship(GeneralFieldRelationship fr) throws ObjectAlreadyExistsException
	{
		if(!isFieldRelationshipAdded(fr))
			fieldrelationships.add(fr);
		else
			throw new ObjectAlreadyExistsException("A Field Relationship with the same Source/Target inputs already exists, please modify this Field Relationship");
	}
	
	
/*
 * Everything below here is used to set the data retrieved from a system
 * 
 */	
	
	@SuppressWarnings({ "unchecked"})
	/**
	 * Retrieve the Type definition as a Type Object
	 * 
	 * @param name The name of the Type to retrieve
	 * @param connection The connection to use
	 * 
	 * @return Type The Type object representing the Type requested
	 * @throws MKSLibConnectionException when a connection problem occurs
	 */
	public static Type getType(String name, Connection connection)	throws MKSLibConnectionException
	{
		Type returnType = new Type(name);		
		Command viewType = new Command(Command.IM, "viewtype");
		viewType.addOption(new Option("showProperties"));
		viewType.addSelection(name);		
		Response typeDefRes = null;
		try {
			typeDefRes = connection.runCommand(viewType);
		} catch (MKSLibConnectionException e) {
			typeDefRes = null;
		}

		if(null != typeDefRes)
		{
			WorkItem typeDefWI= typeDefRes.getWorkItem(name);
			Iterator typeDefFields = typeDefWI.getFields();

			returnType = parseItemList(typeDefWI.getField("permittedAdministrators"), returnType, connection);
			returnType = parseItemList(typeDefWI.getField("permittedGroups"), returnType, connection);				
			returnType = parseItemList(typeDefWI.getField("stateTransitions"), returnType, connection);
			returnType = parseItemList(typeDefWI.getField("visibleFields"), returnType, connection);			
			returnType = parseItemList(typeDefWI.getField("mandatoryFields"), returnType, connection);		
			returnType = parseItemList(typeDefWI.getField("notificationFields"), returnType, connection);
							
			while(typeDefFields.hasNext())
			{
				Field field = (Field)typeDefFields.next();
				String dataType =  field.getDataType(); 

				if(null != dataType
						&& !Field.ITEM_LIST_TYPE.equals(dataType))
				{
					if(dataType.equals(Field.STRING_TYPE))
						returnType = setStringAttribute(field, returnType); 					
					else if(dataType.equals(Field.BOOLEAN_TYPE))
						returnType = setBooleanAttribute(field, returnType);
					else if(dataType.equals(Field.INTEGER_TYPE))
						returnType = setIntAttribute(field, returnType);
				}			
			}
		}
		else
		{
			throw new MKSLibConnectionException("Response returned was null. Ensure that Connection.makeConnection(String, int) is called and successful.");
		}
		return returnType;		
	}
	
	@SuppressWarnings("unchecked")
	private static Type parseItemList(Field f, Type t, Connection connection) throws MKSLibConnectionException, ObjectNotAvailableException
	{
		ItemList il = (ItemList)f.getList();
		String fName = f.getName();
		
		if(null == il || null == fName)
			return t;
			
		if(fName.equalsIgnoreCase("permittedAdministrators"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				String mt = item.getModelType();
				if("im.Group".equals(mt))
					t.addPermittedAdmin(new Group(item.getId()));
				else if("im.User".equals(mt))
					t.addPermittedAdmin(new User(item.getId()));
			}
		}
		else if(fName.equalsIgnoreCase("permittedGroups"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				String mt = item.getModelType();
				if("im.Group".equals(mt))
					t.addPermittedGroup(new Group(item.getId()));
			}
		}
		else if(fName.equalsIgnoreCase("notificationFields"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				String mt = item.getModelType();
				if("im.Field".equals(mt))
					t.addNotificationField(getVisibleField(t,(item.getId())));
			}
		}
		else if(fName.equalsIgnoreCase("mandatoryFields"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				String mt = item.getModelType();
				if("im.State".equals(mt))
				{
					State s = null;					
					try{
						s = t.getStates().get(t.getStateIndex(item.getId()));
					}catch(ArrayIndexOutOfBoundsException e){
						s = new State(item.getId());
					}
					
					ItemList mfil = (ItemList)item.getField("fields").getList();
					for(int j=0;j<mfil.size();j++)
					{						
						try{
							IField manField = getVisibleField(t,((Item)mfil.get(j)).getId()  );
							if(null != manField)
								s.addMandatoryField(manField);
							else
								throw new NullPointerException();							
						}catch(NullPointerException npe){
							throw new ObjectNotAvailableException(((Item)mfil.get(j)).getId() + " cannot be a mandatory field on " + t.getName() + " and on  State " + s.getName() + ". The field is not visible.");
						}						
					}
					t.addOrOverrideState(s);
				}
			}
		}
		else if(fName.equalsIgnoreCase("stateTransitions"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				State sourceState = new State(item.getId());
				ItemList targetStates = (ItemList)item.getField("targetStates").getList();
				for(int j=0;j<targetStates.size();j++)
				{
					Item targetStateItem = (Item)targetStates.get(j);
					Field pgf = targetStateItem.getField("permittedGroups");
					ItemList pgil = (ItemList)pgf.getList();
					List<Group> groupList = new ArrayList<Group>();
					for(int k=0; k< pgil.size();k++)
						groupList.add(new Group(((Item)pgil.get(k)).getId()));
					StateTransition st = null;				
					if(sourceState.getName().equals(targetStateItem.getId()))
					{
						 st = new StateTransition(sourceState, groupList);
					}
					else
					{
						State targetState = new State(targetStateItem.getId());					
						st = new StateTransition(sourceState, targetState, groupList);
					}					
					t.addStateTransition(st);
				}
			}
		}
		else if(fName.equalsIgnoreCase("visibleFields"))
		{
			for(int i=0; i <il.size();i++)
			{
				Item item = (Item)il.get(i);
				String mt = item.getModelType();
				if("im.Field".equals(mt))
				{
					GeneralField gf = GeneralField.getField(item.getId(), connection);
					if(null != gf)
					{
						ItemList groupIL = (ItemList)item.getField("groups").getList();
						Iterator git = groupIL.getItems();
						while(git.hasNext())
						{
							Item gi = (Item)git.next();
							gf.addPermittedGroup(gi.getId());
						}
						t.addField(gf);
					}
				}
			}
		}
		return t;
	}

	private static IField getVisibleField(Type t, String id) {
		List<IField> fl = t.getFields();
		int size = fl.size();
		for(int i = 0; i< size;i++)
		{
			//System.out.println("Comparing: " + )
			if(fl.get(i).getName().equals(id))
			{
				return fl.get(i);
			}
		}
		return null;
	}
	private static Type setStringAttribute(Field f, Type t)
	{
		String fName = f.getName();
		String fValue = f.getValueAsString();
		
		if(fName.equalsIgnoreCase("description"))
			t.setDescription(fValue);	
		return t;
			
	}
	private static Type setBooleanAttribute(Field f, Type t)
	{
		String fName = f.getName();
		boolean fValue = f.getBoolean().booleanValue();
		
		if(fName.equalsIgnoreCase("allowChangePackages"))
		{
			if(fValue)
				t.setAllowCPs(Type.CPCREATIONPOLICY_NOTSET);
			else
				t.unsetAllowCPs();
		}
		else if(fName.equalsIgnoreCase("showWorkflow"))
			t.setShowWorkflow(fValue);
		else if(fName.equalsIgnoreCase("timeTrackingEnabled"))
			t.setAllowTimeTracking(fValue);
		return t;
			
	}
	
	private static Type setIntAttribute(Field f, Type t)
	{
		String fName = f.getName();
		int fValue = f.getInteger().intValue();
		
		if(fName.equalsIgnoreCase("position"))
			t.setPosition(fValue);
		return t;
	}
	
	/**
	 * Retrieve the Type definition as a String.
	 *
	 *@return The entire Type definition.
	 */
	public String toString()
	{
		String retStr = "Type: " + this.getName();
		retStr += "\nDescription:\n\t"+ this.getDescription();
		retStr += "\nDisplay Workflow in Issue: " + (this.isShowWorkflow() ? "yes" : "no");
		
		try{
			retStr += "\nWorkflow Phase Field: " + this.phaseField.getName();
		}catch(NullPointerException npe){
			retStr += "\nWorkflow Phase Field: none";			
		}
		
		retStr += "\nTime Tracking Enabled: " + (this.isAllowTimeTracking() ? "yes" : "no");
		retStr += "\nAllow Change Packages: " + (this.isAllowCPs() ? "yes" : "no");
		if(this.isAllowCPs())
			retStr += "\nMKS Source Change Package creation policy: " + this.CPCreationPolicy; 
		retStr += "\nVisible Fields:";
		for(int i=0;i<this.getFields().size();i++)
		{
			IField gf = this.getFields().get(i);
			retStr+= "\n\t" + gf.getName() + " (";
			for(int j=0;j<gf.getPermittedGroups().size();j++)
			{
				retStr += gf.getPermittedGroups().get(j);
				if(j+1<gf.getPermittedGroups().size())
					retStr += ", ";				
			}
			retStr += ")";
		}
		retStr += "\nState Transitions:";
		for(int i=0;i<this.getStateTransitions().size();i++)
		{
			StateTransition st = this.getStateTransitions().get(i);
			retStr+= "\n\t" + st.getFromState().getName() + " -> " + st.getToState().getName() + " (";
			for(int j=0;j<st.getPermittedGroups().size();j++)
			{
				retStr += st.getPermittedGroups().get(j).getName();
				if(j+1<st.getPermittedGroups().size())
					retStr += ", ";				
			}
			retStr += ")";
		}	
		retStr += "\nMandatoryFields:";
		for(int i=0; i<this.getStates().size();i++)
		{			
			State s = this.getStates().get(i);
			if(s.getMandatoryFields().size() >0)
			{
				List<IField> fs = s.getMandatoryFields();
				retStr +="\n\t" + s.getName() + "(" + fs.get(0).getName();
				
				for(int j=1; j<fs.size();j++)
					retStr+="," + fs.get(j).getName();
				retStr +=")";
			}
		}
		
		retStr += "\nPosition: " + this.getPosition();
		retStr += "\nNotification Fields:";
		for(int i=0; i<this.getNotificationFields().size();i++)
		{			
				retStr +="\n\t" + this.getNotificationFields().get(i).getName();
		}

		retStr += "\nPermitted Groups:";		
		for(int i=0;i<this.getPermittedGroups().size();i++)
			retStr += "\n\t" + this.getPermittedGroups().get(i).getName();
	
		retStr += "\nAdministrators:";		
		for(int i=0;i<this.getPermittedAdmins().size();i++)
		{
			IMUserOrGroup p = this.getPermittedAdmins().get(i);
			retStr += "\n\t" + p.getName() + ":";
			retStr += p.getPrincipalType();
		}

		return retStr;
	}
}
