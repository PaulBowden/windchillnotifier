/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class GeneralFieldRelationship {
	private GeneralField targetField;
	private boolean ruleBased;
	
	
	/**
	 * @return the setIsRuleBased
	 */
	public boolean isRuleBased() {
		return ruleBased;
	}

	/**
	 * @param setIsRuleBased the setIsRuleBased to set
	 */
	protected void setRuleBased(boolean ruleBased) {
		this.ruleBased = ruleBased;
	}

	/**
	 * @return the targetField
	 */
	public GeneralField getTargetField() {
		return targetField;
	}

	/**
	 * @param targetField the targetField to set
	 */
	public void setTargetField(GeneralField targetField) {
		this.targetField = targetField;
	}

}
