/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;
import com.mks.api.response.Item;
import com.mks.api.response.ItemList;
import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.IllegalValueDetectedException;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.ObjectAlreadyExistsException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class PickField extends GeneralField implements IField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5790350248060793105L;
	public static final PickItem EMPTYPICK = null;
	private PickItem defaultValue;
	private List<PickItem> pickItems = new ArrayList<PickItem>();
	private boolean isMultiValued;

	public PickField(String name)
	{
		super(name, FieldType.PICKLIST);
	}
	
	public boolean addPickItem(PickItem pi) throws ObjectAlreadyExistsException
	{
		for(PickItem n : pickItems)
		{
			if(n.getPickLabel().equals(pi.getPickLabel())
					&& !n.equals(pi))
				throw new ObjectAlreadyExistsException("The Pick Item '" + pi.getPickLabel() + "' already exists with a different set of attributes on it.");
		}
		return this.pickItems.add(pi);
	}
	
	protected static PickField getField(WorkItem item)
	{
		PickField f = new PickField(item.getField("name").getValueAsString());
		ItemList picks = (ItemList)item.getField("picks").getList();
		
		for(int i=0;i<picks.size();i++)
		{
			Item pickItem = (Item)picks.get(i);
			f.addPickItem(new PickItem( pickItem.getField("label").getValueAsString(),pickItem.getField("value").getInteger().intValue(),  pickItem.getField("active").getBoolean()));
		}
		
		f.setDisplayname(item.getField("displayName").getString());
		PickItem defaultPick = getPick(item.getField("default").getValueAsString(),f);
		if(PickField.EMPTYPICK != defaultPick)
			f.setDefaultValue(defaultPick);
		else
			f.setDefaultValue(PickField.EMPTYPICK);
		
		f.setMultiValued(item.getField("isMultiValued").getBoolean());
		f.setDescription(item.getField("Description").getString());

		//f.setMaxLength(item.getField("MaxLength").getInteger().intValue());
		f.setPosition(item.getField("Position").getInteger().intValue());

		System.out.println("What is this: " + f.getName());
		return f;
	}

	private static PickItem getPick(String valueAsString, PickField f) {
		
		for(PickItem n : f.getPickItems() )
		{
			if(n.getPickLabel().equals(valueAsString))
				return n;
		}
		return PickField.EMPTYPICK;
	}

	/**
	 * Get's the default Pick Item for this Pick Field
	 * @return the defaultValue
	 */
	public PickItem getDefaultValue() {
		return defaultValue;
	}
	
	
	private boolean testValue(Object o){
		PickItem testValues = null;
		try{
			testValues = (PickItem)o;
		}catch(ClassCastException cce){
			return false;
		}
		return true;
	}

	/**
	 * Sets the Default Pick Item for this Pick Field.
	 * 
	 * @param defaultValue the defaultValue to set
	 * @throws IllegalValueDetectedException if the <tt>defaultValue</tt> is not of Type PickItem.
	 */
	public void setDefaultValue(Object defaultValue) {		
		if(testValue(defaultValue))
			this.defaultValue = (PickItem)defaultValue;
		else
			throw new IllegalValueDetectedException(defaultValue.toString(), this.getFieldTypeAsStr());
	}

	/**
	 * Returns all Pick Items in this Pick field
	 * @return the pickItems
	 */
	public List<PickItem> getPickItems() {
		return pickItems;
	}

	/**
	 * Sets the list of Pick Items
	 * @param pickItems the pickItems to set
	 */
	public void setPickItems(List<PickItem> pickItems) {
		this.pickItems = pickItems;
	}	

	/**
	 * @return the isMultiValued
	 */
	public boolean isMultiValued() {
		return isMultiValued;
	}

	/**
	 * @param isMultiValued the isMultiValued to set
	 */
	public void setMultiValued(boolean isMultiValued) {
		this.isMultiValued = isMultiValued;
	}	
	/**
	 * Checks all attributes, including the PickItem values of the PickField to check for equality.
	 * 
	 * 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		PickField i = (PickField)o;
				
		if(i.getPickItems().size() != this.getPickItems().size())
			return false;
		
		// this checks the pick items
		for(int j = 0; j< i.getPickItems().size();j++)
		{
			if(!this.getPickItems().contains(i.getPickItems().get(j)))
				return false;
			if(!i.getPickItems().contains(this.getPickItems().get(j)))
				return false;		
		}
		
		return ( i.getName().equals(this.getName())
				&& i.getDescription().equals(this.getDescription())
				&& i.getDisplayname().equals(this.getDisplayname())
				);					
	}
	
	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + this.getDisplayname().hashCode();
		hash = hash * 31 + this.getDescription().hashCode();
		hash = hash * 31 + this.getName().hashCode();
		hash = hash * 31 + this.getPickItems().size();
		for(int j = 0; j< this.getPickItems().size();j++)
		{
			hash = hash * 31 + this.getPickItems().get(j).hashCode();
		}	
		
		return hash;
	}
}
