/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cstoss
 *
 */
public class NormalFieldRelationship extends GeneralFieldRelationship {
	/**
	 * Source field of the relationship
	 */
	private GeneralField sourceField;
	
	/**
	 * Target field of the relationship
	 */
	
	
	private List<String> sourceValues = new ArrayList<String>();
	private List<String> targetValues = new ArrayList<String>();
	
	/**
	 * Constructor
	 * 
	 * @param sourceField
	 * @param targetField
	 */
	public NormalFieldRelationship(GeneralField sourceField, GeneralField targetField) {
		this.sourceField = sourceField;
		setTargetField(targetField);
		setRuleBased(false);
	}
	


	public void addSourceValue(String s)
	{
		if(!sourceValues.contains(s))
			sourceValues.add(s);
	}
	public void addTargetValue(String s)
	{
		if(!targetValues.contains(s))
			targetValues.add(s);
	}

	/**
	 * @return the sourceField
	 */
	public GeneralField getSourceField() {
		return sourceField;
	}

	/**
	 * @param sourceField the sourceField to set
	 */
	public void setSourceField(GeneralField sourceField) {
		this.sourceField = sourceField;
	}


	/**
	 * @return the sourceValues
	 */
	public List<String> getSourceValues() {
		return sourceValues;
	}

	/**
	 * @return the targetValues
	 */
	public List<String> getTargetValues() {
		return targetValues;
	}
}
