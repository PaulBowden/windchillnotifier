/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class Group extends IMUserOrGroup  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7086629797481853232L;

	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public Group(String name) {
	    setName(name);
	    this.setPrincipalType(this.PRINCIPALTYPE_GROUP);
	}

	/**
	 * A Group is equal to another Object when all attributes are the same. 
	 */
	/*public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		Group i = (Group)o;
		
		return (this.isActive() == i.isActive()
				 && this.getEmail().equals(i.getEmail())
				 && this.getDescription().equals(i.getDescription())
				 && this.getName().equals(i.getName())
				 );
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + getEmail().hashCode();
		hash = hash * 31 + getDescription().hashCode();
		hash = hash * 31 + getName().hashCode();
		hash = hash*31 + (isActive() ? 1 : 0);
		return hash;
	}*/
}

