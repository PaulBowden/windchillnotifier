package com.mks.services.lib.im;

import java.util.List;

import com.mks.services.lib.im.adminobjects.Query;

public interface IField {

	/**
	 * Get the permitted groups of a Field
	 * Used when adding fields to a Type.
	 * @return a list of the permitted Groups as Strings
	 */
	public List<String> getPermittedGroups();

	/**
	 * Sets the Permitted Groups of a field
	 * Used when adding fields to a Type.
	 * @param permittedGroups
	 */
	public void setPermittedGroups(List<String> permittedGroups);
	
	
	public void removePermittedGroup(String s);
	public void addPermittedGroup(String s);
	public boolean isMandatory();
	public void setMandatory(boolean mandatory);
	public String getDescription();
	public void setDescription(String description);
	public void setName(String name);
	public String getPosition();
	public void setPosition(String position);
	public void setPosition(int position);
    public String getDisplayname();
    public void setDisplayname(String displayname);
    public abstract String getFieldTypeAsStr();
	public boolean hasSameName(GeneralField gf);
	public boolean allowComputedValue();
	public boolean isComputed();
	public Computation getComputation();
	public void setBackedby(String backedby);
	public void setColumns(List<IField> columns);
	public List<IField> getColumns();
	public boolean isVariableHeightRows();
	public void setVariableHeightRows(boolean variableHeightRows);
	public Query getQuery();
	public void setQuery(Query query);
	public List<FieldCorrelation> getFieldCorrelations();
	public void setFieldCorrelations(List<FieldCorrelation> fieldCorrelations);
	public String getDisplayType();
	public void setDisplayType(String displayType);
	public int getDisplayRows();
	public void setDisplayRows(int displayRows);
	public Object getMinValue();
	/**
	 * Sets the minimum object value of the Field
	 * @param minValue the minimum object value of the Field
	 */
	public void setMinValue(Object minValue);
	public Object getMaxValue();
	public void setMaxValue(Object maxValue);
	public Object getDefaultValue();
	public void setDefaultValue(Object defaultValue);
	public boolean isDisplayProgressBar();
	public void setDisplayProgressBar(boolean displayProgressBar);
	public String getRelationship();
	public void setRelationship(String relationship);
	public FieldType getFieldType();
	public String getName();
	public boolean isMultiValued();
	public void setMultiValued(boolean isMultiValued);
	public List<PickItem> getPickItems();
	public void setPickItems(List<PickItem> pickItems);
}

