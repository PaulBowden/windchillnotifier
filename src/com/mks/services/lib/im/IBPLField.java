/**
 * 
 */
package com.mks.services.lib.im;

import java.util.ArrayList;
import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class IBPLField extends GeneralField implements IField {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5447053493196756435L;
	private boolean displayAsLink;
	private String backingType;
	private String itemIdentifier;
	private List<String> activeStates;
	private String filter;
	private String backingTextFormat;
	private boolean isMultiValued;

	public IBPLField(String name)
	{
		super(name, FieldType.IBPL);
	}

	/**
	 * @return the backingTextFormat
	 */
	public String getBackingTextFormat() {
		return backingTextFormat;
	}

	/**
	 * @param backingTextFormat the backingTextFormat to set
	 */
	public void setBackingTextFormat(String backingTextFormat) {
		this.backingTextFormat = backingTextFormat;
	}

	/**
	 * @return the isMultiValued
	 */
	public boolean isMultiValued() {
		return isMultiValued;
	}

	/**
	 * @param isMultiValued the isMultiValued to set
	 */
	public void setMultiValued(boolean isMultiValued) {
		this.isMultiValued = isMultiValued;
	}
	/**
	 * @return the displayAsLink
	 */
	public boolean isDisplayAsLink() {
		return displayAsLink;
	}

	/**
	 * @param displayAsLink the displayAsLink to set
	 */
	public void setDisplayAsLink(boolean displayAsLink) {
		this.displayAsLink = displayAsLink;
	}

	/**
	 * @return the backingType
	 */
	public String getBackingType() {
		return backingType;
	}

	/**
	 * @param backingType the backingType to set
	 */
	public void setBackingType(String backingType) {
		this.backingType = backingType;
	}

	/**
	 * @return the itemIdentifier
	 */
	public String getItemIdentifier() {
		return itemIdentifier;
	}

	/**
	 * @param itemIdentifier the itemIdentifier to set
	 */
	public void setItemIdentifier(String itemIdentifier) {
		this.itemIdentifier = itemIdentifier;
	}

	/**
	 * @return the activeStates
	 */
	public List<String> getActiveStates() {
		return activeStates;
	}

	/**
	 * @param activeStates the activeStates to set
	 */
	public void setActiveStates(List<String> activeStates) {
		this.activeStates = activeStates;
	}

	/**
	 * @return the filter
	 */
	public String getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}
	protected static IBPLField getField(WorkItem item)
	{
		IBPLField f = new IBPLField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		//f.setDefaultValue(item.getField("Default").getValueAsString());
		f.setDescription(item.getField("description").getString());		
		f.setPosition(item.getField("position").getInteger().intValue());
		
		f.setBackingType(item.getField("backingType").getString());
		f.setItemIdentifier(item.getField("backingTextFormat").getString());
		f.setMultiValued((item.getField("isMultiValued").getBoolean()));
		f.setDisplayAsLink((item.getField("displayAsLink").getBoolean()));
		String[] states = item.getField("backingStates").getString().split(",");
		List<String> stateList =  new ArrayList<String>();
		for(String s : states)
			stateList.add(s);
		f.setActiveStates(stateList);
		
		f.setFilter(item.getField("backingFilter").getString());
		
		System.out.println("What is this: " + f.getName());
		return f;
	}
}
