package com.mks.services.lib.im;

public enum FieldType {
	ATTACHMENT("attachment", false),
	DATE("date", true),
	FLOAT("float", true, true),
	FVA("fva"),
	IBPL("ibpl"),
	INTEGER("integer", true,true),
	LOGICAL("logical", true),
	LONGTEXT("longtext"),
	PHASE("phase"),
	PICKLIST("pick"),
	PROJECT("project"),
	RELATIONSHIP("relationship", false, true, false),
	SHORTTEXT("shorttext", true),
	QBR("qbr", false, true, true),
	USER("user");
			
	private String fieldtype;
	private boolean allowComputed = false;
	private boolean isRelationship = false;
	private boolean needsQuery = false;
	private boolean isNumberField = false;

	FieldType(String fieldtype, boolean allowComputed, boolean isRelationship, boolean needsQuery){
		this.fieldtype = fieldtype;
		this.allowComputed = allowComputed;
		this.isRelationship = isRelationship;
		this.needsQuery = needsQuery;
	}
	FieldType(String fieldtype, boolean allowComputed, boolean isNumberField){
		this.fieldtype = fieldtype;
		this.allowComputed = allowComputed;
		this.isNumberField = isNumberField;
	}
	FieldType(String fieldtype, boolean allowComputed){
		this.fieldtype = fieldtype;
		this.allowComputed = allowComputed;
	}
	FieldType(String fieldtype){
		this.fieldtype = fieldtype;
	}
	/**
	 * @return the fieldtype
	 */
	public String getFieldtype() {
		return fieldtype;
	}
	/**
	 * @return the allowComputed
	 */
	public boolean isAllowComputed() {
		return allowComputed;
	}
	/**
	 * @return the isRelationship
	 */
	public boolean isRelationship() {
		return isRelationship;
	}
	/**
	 * @return the needsQuery
	 */
	public boolean isNeedsQuery() {
		return needsQuery;
	}
	/**
	 * @return the isNumberField
	 */
	public boolean isNumberField() {
		return isNumberField;
	}
	
}
