/**
 * 
 */
package com.mks.services.lib.im;

/**
 * @author cstoss
 *
 */
public class User extends IMUserOrGroup {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6360271391677977203L;
	/**
	 * Full name of the user
	 */
	private String fullName;
		
	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public User(String name) {
	    setName(name);
	    this.setPrincipalType(this.PRINCIPALTYPE_USER);
	}
	
	
	/**
	 * Get full name of the user
	 * 
	 * @return Returns the fullName.
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Set full name of the user
	 * 
	 * @param fullName The fullName to set.
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * A User is equal to another User when all attributes are the same. 
	 */
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		User i = (User)o;
		
		return (this.isActive() == i.isActive()
				 && this.fullName.equals(i.getFullName())
				 && this.getEmail().equals(i.getEmail())
				 && this.getDescription().equals(i.getDescription())
				 && this.getName().equals(i.getName())
				 );
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash*31 + fullName.hashCode();
		hash = hash * 31 + getEmail().hashCode();
		hash = hash * 31 + getDescription().hashCode();
		hash = hash * 31 + getName().hashCode();
		hash = hash*31 + (isActive() ? 1 : 0);
		return hash;
	}
}
