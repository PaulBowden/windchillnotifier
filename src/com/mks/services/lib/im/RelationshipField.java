	/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;

import com.mks.api.response.WorkItem;
import com.mks.services.lib.CLIOptions;
import com.mks.services.lib.MethodNotImplementedException;
import com.mks.services.lib.im.adminobjects.Query;

/**
 * @author cstoss
 *
 */
public class RelationshipField extends GeneralField implements IField {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1269734183916744064L;
	private RelationshipField backwardsRelationship;
	
	public RelationshipField(String forwardName, String backwardName)
	{
		super(forwardName, FieldType.RELATIONSHIP);
		backwardsRelationship =  new RelationshipField(this, backwardName);
	}
	public RelationshipField(String forwardName)
	{
		super(forwardName, FieldType.RELATIONSHIP);
		//backwardsRelationship =  new RelationshipField(this, backwardName);
	}
	
	public RelationshipField(RelationshipField forwardRelationship, String backwardName)
	{
		super(backwardName, FieldType.RELATIONSHIP);
		backwardsRelationship = forwardRelationship;
	}
	

	/**
	 * @return the backwardsRelationship
	 */
	public RelationshipField getBackwardsRelationship() {
		return backwardsRelationship;
	}

	/**
	 * @param backwardsRelationship the backwardsRelationship to set
	 */
	public void setBackwardsRelationship(RelationshipField backwardsRelationship) {
		this.backwardsRelationship = backwardsRelationship;
	}
	
	protected static RelationshipField getField(WorkItem item)
	{
		RelationshipField f = new RelationshipField(item.getField("name").getValueAsString());
		
		f.setDisplayname(item.getField("displayName").getString());
		f.setDescription(item.getField("description").getString());
		
		
		
			System.out.println("What is this: " + f.getName());
		return f;
	}
}
