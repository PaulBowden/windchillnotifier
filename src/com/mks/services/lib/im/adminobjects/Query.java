/**
 * 
 */
package com.mks.services.lib.im.adminobjects;

/**
 * The Query Object is a name and a Query Definition only at this point.
 * There is no concept of sharing, column sets or error checking the queryDefinition
 * 
 * @author cstoss
 *
 */
public class Query {

	private String name;
	private String queryDefinition;
	
	public Query(String name)
	{
		this.name = name;
	}
	public Query(String name, String rule)
	{
		this.name = name;
		this.queryDefinition = rule;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the rule
	 */
	public String getRule() {
		return queryDefinition;
	}
	/**
	 * @param rule the rule to set
	 */
	public void setRule(String rule) {
		this.queryDefinition = rule;
	}
	
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(null == o ||  (o.getClass() != this.getClass()))
			return false;
		Query i = (Query)o;
		
		return (this.getRule().equals(i.getRule())
				 && this.getName().equals(i.getName())
				 );
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash * 31 + getRule().hashCode();
		hash = hash * 31 + getName().hashCode();
		return hash;
	}
}
