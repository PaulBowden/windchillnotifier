/**
 * 
 */
package com.mks.services.lib.im;

import java.util.List;
import java.util.ArrayList;

/**
 * This is for specification of individual phases within a PhaseField
 * 
 * @author cstoss
 *
 */
public class Phases {
	private String phaseText;
	private List<State> phaseStates = new ArrayList<State>();
	/**
	 * @param phaseText
	 */
	public Phases(String phaseText) {
		this.phaseText = phaseText;
	}
	/**
	 * @return the phaseText
	 */
	public String getPhaseText() {
		return phaseText;
	}
	/**
	 * @param phaseText the phaseText to set
	 */
	public void setPhaseText(String phaseText) {
		this.phaseText = phaseText;
	}
	/**
	 * @return the phaseStates
	 */
	public List<State> getPhaseStates() {
		return phaseStates;
	}
	
	public void addPhaseState(State s)
	{
		if (!phaseStates.contains(s))
		{
			phaseStates.add(s);
		}
	}
	
	
}
