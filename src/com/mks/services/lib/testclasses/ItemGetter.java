package com.mks.services.lib.testclasses;

import com.mks.services.lib.Connection;
import com.mks.services.lib.IllegalFieldOperationException;
import com.mks.services.lib.MKSLibConnectionException;
import com.mks.services.lib.im.MKSItem;

public class ItemGetter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection connection = new Connection("administrator", "password");
		
		try {			
			connection.makeConnection("ukltcstoss", 7001);
		} catch (MKSLibConnectionException e) {
			e.printStackTrace();
		}
		

		try{
			
		
		MKSItem mi = MKSItem.getItem(1215, connection);
		
		System.out.println(mi.getType());
		System.out.println(mi.getFieldValueAsString("test fva IBPL"));
		System.out.println(mi.getFieldValueAsString("test fva IBPL", true));
				/*mi.setField("Shared Text", "asf");
		System.out.println(mi.getFieldValue("Shared Text").get(0));
				mi.setIBPLField("Shared Text", "The timer must have a user interface that allows for simple gestures to set it.", false);
		System.out.println(mi.getFieldValueAsString("Shared Text"));
		*/
		System.out.println(mi.getChangedFields());
		mi.setField("project","/myProj");
		System.out.println(mi.getFieldValueAsString("Modified By"));
		System.out.println(mi.getID());
		System.out.println(mi.getChangedFields());
		System.out.println(mi.getFieldValueAsString("project"));
		mi.setField("ID","123");
		}catch(IllegalFieldOperationException ifoe)
		{
			ifoe.printStackTrace();
			System.out.println("This affected " + ifoe.getAffectedFieldName() + "("+ ifoe.isIBPL()+")");
		}
		try {
			connection.disconnect();
		} catch (MKSLibConnectionException e) {
			e.printStackTrace();
		}
	}

}
