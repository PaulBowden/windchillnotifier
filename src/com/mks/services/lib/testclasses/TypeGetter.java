package com.mks.services.lib.testclasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.mks.services.lib.Connection;
import com.mks.services.lib.MKSLibConnectionException;
import com.mks.services.lib.ObjectNotAvailableException;
import com.mks.services.lib.im.Computation;
import com.mks.services.lib.im.GeneralField;
import com.mks.services.lib.im.IField;
import com.mks.services.lib.im.Type;

public class TypeGetter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Connection c = new Connection("administrator", "password");
		
		try {			
			c.makeConnection("ukltcstoss2", 7001);
		} catch (MKSLibConnectionException e) {
			e.printStackTrace();
		}
		
		Type t = null;
		try {
			
			t = Type.getType("Project",c);
		} catch (MKSLibConnectionException e1) {
 
			e1.printStackTrace();
		}
		
		
		System.out.println("Desc: " + t.getDescription());
		System.out.println("Position: " + t.getPosition());
		System.out.println("ALL CPs: " + t.isAllowCPs());
		System.out.println("ALL TT: " + t.isAllowTimeTracking());
		
		System.out.println("ALL PA: " + t.getPermittedAdmins().size());
		System.out.println("ALL PG: " + t.getPermittedGroups().size());
		System.out.println("ALL f: " + t.getFields().size());

		try {
			c.disconnect();
		} catch (MKSLibConnectionException e) {
			e.printStackTrace();
		}
		
		//System.out.println("BEFORE WRITE");
		writeOut(t, "c:/type.bin");
		//System.out.println("AFTE WRITE");
		Type newType = (readIn("c:/type.bin")).get(0);
		
	//	System.out.println("READ IN");
		System.out.println(newType.toString());
		//System.out.println("All fs: " + newType.getFields().size());
		List<IField> fl = newType.getFields();
		for(IField gf : fl)
		{
			System.out.println(gf);
			if(gf.isComputed()){
				Computation c2 = gf.getComputation();
				System.out.println(c2.getComputationString() + " " + c2.getFrequency());
			}	
		}
	}

	public static List<Type> readIn(String filename)
	{
		List<Type> allTypes = new ArrayList<Type>();
		
		File dataFile = new File(filename);
		
		if(dataFile.canRead())
		{				
			ObjectInputStream oin = null;
			try {
				oin = new ObjectInputStream(new FileInputStream(dataFile));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			
			boolean moreObjs = true;		
			while(moreObjs)
			{	
				try{
					allTypes.add((Type)oin.readObject());
				}catch(Exception e)
				{				
					moreObjs = false;
				}
			}
			
			try {
				oin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return allTypes;
		}
		else
		{
			return null;
		}
	}
	public static void writeOut(Type t, String dataFile)
	{
		ObjectOutputStream oout = null;	
		

		try {
			oout = new ObjectOutputStream(new FileOutputStream(dataFile));
			oout.writeObject(t);
			 oout.flush();
			 oout.close();	
		}catch (java.io.FileNotFoundException fnfexc){
			System.exit(-1);
		}catch (java.io.StreamCorruptedException e2){
				System.exit(-1);
		}catch (Exception exc){
			exc.printStackTrace();
		}
		

	}
}

