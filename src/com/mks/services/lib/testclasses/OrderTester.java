/**
 * 
 */
package com.mks.services.lib.testclasses;

import java.util.ArrayList;
import java.util.List;

import com.mks.services.lib.ObjectNotAvailableException;
import com.mks.services.lib.im.*;
import com.mks.services.lib.processing.OrderDefiner;

/**
 * @author cstoss
 *
 */
public class OrderTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Type t = new Type("myTestType");
		
		Group newG = new Group("Developers");
		User newU =  new User("MyUSer");
		Group g1= new Group("everyone");
		Group g3= new Group("QA");
		t.addPermittedGroup(newG);
		t.addPermittedGroup(g1);
		//t.addPermittedAdmin(newG);
		//t.addPermittedAdmin(g1);
		//t.addPermittedAdmin(newU);				
		//t.setAllowCPs(true);
		t.setAllowTimeTracking(false);
		t.setDescription("This is a test type");
		t.setShowWorkflow(true);
		GeneralField f1 = new ShortTextField("Summary");
		f1.setDescription("This is my description");
		
		f1.addPermittedGroup("everyone");
		t.addField(f1);
		GeneralField f2 = new PickField("State");
		f2.addPermittedGroup("everyone");
		//t.addField(f2);
		GeneralField f3 = new PickField("Project");
		f3.addPermittedGroup("Administrators");
		f3.addPermittedGroup("Developers");
		t.addField(f3);
		State s1 = new State("ALM_Submit");
		t.setPosition(5);
		//t.setMakeMandatoryFieldsVisibleInType(false);
		State s2 = new State("ALM_Complete");
		State s3 = new State("ALM_Design");
		State s4 = new State("ALM_Inactive");
		List <Group> gl1 = new ArrayList<Group>();
		gl1.add(g3);
		t.setPosition("before:<ALMSOLUTION>");
		List <Group> gl2 = new ArrayList<Group>();
		gl2.add(g1);
		gl2.add(newG);
		State s5 = new State("ALM_On Hold");
		s1.addMandatoryField(f3);
		s1.addMandatoryField(f2);
		s2.addMandatoryField(f2);
		s2.addMandatoryField(f1);
		StateTransition st3 = null;
		//PhaseField pf = new PhaseField("My Phase");
		//Phases p1 = pf.createPhase("Critical");
		//Phases p2 = pf.createPhase("Medium");
		//Phases p3 = pf.createPhase("Low");
		
	//	p1.addPhaseState(s1);
		//p1.addPhaseState(s2);
		//p2.addPhaseState(s3);
		//p3.addPhaseState(s4);
		//p3.addPhaseState(s5);

		//t.setPhaseField(pf);
		
		System.out.println(f1);
		System.out.println(f2);
		System.out.println(f3);
		try{
			t.addState(s1);
			t.addState(s2);
			st3 = new StateTransition(t.getUnspecifiedState(),s1, gl1);
		}catch(ObjectNotAvailableException e)
		{
			e.printStackTrace();
		}
		StateTransition st1 = new StateTransition(s1,s2, gl2);
		StateTransition st2 = new StateTransition(s2,s1, gl1);

		t.addStateTransition(s1,s2, gl1);
		t.addStateTransition(st2);
		
		t.addStateTransition(st1);
		t.addStateTransition(st3);

		
		try{
			t.addNotificationField(f1);
			t.addNotificationField(f2);
		}catch(ObjectNotAvailableException e)
		{
			e.printStackTrace();
		}
					
		OrderDefiner od = new OrderDefiner(t);
		od.doOrder();
		
		
		System.out.println(t.generateCommandLine(true)); 

	}

}
