package com.mks.services.lib;

public class BadDataException extends RuntimeException {

	public BadDataException(String string) {
		super(string);
	}
}
