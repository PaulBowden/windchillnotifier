/**
 * 
 */
package com.mks.services.lib;

import com.mks.api.CmdRunner;
import com.mks.api.Command;
import com.mks.api.IntegrationPointFactory;
import com.mks.api.response.APIException;
import com.mks.api.response.Response;

/**
 * A Connection Object is the base object which is required for all commands to be run. It needs to be 
 * instantiated with a username and password and the method <tt>makeConnection(String, int)</tt> must be called
 * once and only once for each application.
 * 
 * It would be a recommended practice to close the connection using the <tt>disconnect()</tt> method.
 * 
 * @author cstoss
 *
 */
public class Connection {
	private String username;
	private String password;
	private CmdRunner cmdRunner = null;
	
	
	/**
	 * Construct a Connection with the username and password to use or the Connection.
	 * 
	 * @param user username
	 * @param pass password
	 */
	public Connection(String user, String pass)
	{
		username=user;
		password=pass;		
	}

	public Connection(CmdRunner cmdRunner)
	{
		this.cmdRunner = cmdRunner;
	}
	
	/**
	 * Build a connection to the specified server:port
	 * At the moment this only works with a 2009 server
	 * 
	 * @param server Servername
	 * @param port Port
	 * @return true - if the connection was successful or a connection was already established, false otherwise
	 * @throws MKSLibConnectionException Throws an Exception if this method has been called more than once in an application.
	 */
	public boolean makeConnection(String server, int port) throws MKSLibConnectionException
	{
		if(null != cmdRunner)
			throw new MKSLibConnectionException("Connection has already been established. Cannot instantianiate two connections.");
		
		IntegrationPointFactory ipf = IntegrationPointFactory.getInstance();
		try {
			cmdRunner = ipf.createIntegrationPoint(server,port,4,10)
			.createSession(username,password)
			.createCmdRunner();				
			//Setup connection
			cmdRunner.setDefaultHostname(server);
			cmdRunner.setDefaultPort(port);
			cmdRunner.setDefaultUsername(username);
			cmdRunner.setDefaultPassword(password);
		} catch (APIException apiEx) {
        	System.out.println("MAKE CONNECT APIException: "  + apiEx.getClass().getName() +
					 "\n Initmessage:     " + apiEx.getMessage() +
				     "\n InitexceptionId: " + apiEx.getExceptionId()
				     );			
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * This runs a command specified by the Command cmd and returns the Response object form the MKS Java API
	 * 
	 * @param cmd The Command to be run
	 * @return The Response object from the executed command.
	 * @throws MKSLibConnectionException Thrown if the Connection has not properly been established. Either <tt>makeConnection(String, int)</tt> hasn't been called or was unsuccessful. 
	 */
	public Response runCommand(Command cmd) throws MKSLibConnectionException
	{
		if(null == cmdRunner)
			throw new MKSLibConnectionException("Connection has not been established.");
		//CmdRunner cmdRunner = connection.getCmdRunner();
		Response r = null;
		String cmdstr;
		try {
			r = cmdRunner.execute(cmd);
			
		} catch (APIException e) {
			e.printStackTrace();
			String msg = "The Command: " + cmd.getApp() + " " + cmd.getCommandName() + " failed to run.";
			throw new MKSLibConnectionException(msg);			
		} 	
		return r;	
	}
	
	/**
	 * This allows you to get the CmdRunner Object if you wish to run your own commands.
	 * If <tt>makeConnection(String, int)</tt> hasn't been called yet, the CmdRunner is <tt>null</tt>
	 * 
	 * @return the Connection's CmdRunner.
	 * @see #makeConnection(String, int)
	 */
	public CmdRunner getCmdRunner()
	{
		return cmdRunner;
	}

	/**
	 * Disconnects the connection. Should be run once for every <tt>makeConnection(String, int)</tt> call. 
	 * 
	 * @throws MKSLibConnectionException Thrown if trying to disconnect a non-open Connection.
	 */
	public void disconnect() throws MKSLibConnectionException{
		if(null == cmdRunner)
			throw new MKSLibConnectionException("No Connection found.");
		
		try{
			cmdRunner.execute(new String[] {"integrity"	, "disconnect"});			
		}catch (APIException apiEx) {
				System.out.println("APIException: "  + apiEx.getClass().getName() +
					 "\n Discmessage:     " + apiEx.getMessage() +
				     "\n DiscexceptionId: " + apiEx.getExceptionId());
	    }
	}	
}
