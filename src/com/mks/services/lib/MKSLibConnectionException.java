/**
 * 
 */
package com.mks.services.lib;

/**
 * @author cstoss
 *
 */
public class MKSLibConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7120490541198406141L;
	
	/**
	 * Constructor
	 * 
	 * @param string
	 */
	public MKSLibConnectionException(String string) {
		super(string);
	}

}
